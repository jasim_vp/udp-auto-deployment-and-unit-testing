﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataPlatform.Repository
{
    public class MenuListrepository: IMenuListrepository
    {
        private readonly DbWebApiContext _context;
        public MenuListrepository(DbWebApiContext context)
        {
            _context = context;
        }
        #region Select
        public IEnumerable<MenuListDTO> GetMenuList()
        {
            List<MenuListDTO> result = null;
            try
            {
                //result = (from menumaster in _context.menumaster
                //          join submenumaster in _context.submenumaster on menumaster.menuid equals submenumaster.menuid
                //          select new MenuListDTO
                //          {
                //              MenuId = menumaster.menuid,
                //              Menuname = menumaster.menuname,
                //              SubMenuName = submenumaster.submenuname,
                //              InsertedBy = menumaster.insertedby,                              
                //              InsertedTime = menumaster.insertedtime,
                //              Active = menumaster.active
                //          }).ToList();

                result = (from menumaster in _context.menumaster
                          join usermaster in _context.usermaster on menumaster.insertedby equals usermaster.userid
                          orderby menumaster.menuid descending
                          select new MenuListDTO
                          {
                              MenuId = menumaster.menuid,
                              Menuname = menumaster.menuname,
                              InsertedBy = menumaster.insertedby,
                              CreatedBy = usermaster.firstname + ' ' + usermaster.lastname,
                              InsertedTime = Convert.ToDateTime(menumaster.insertedtime).ToString("yyyy-MM-dd HH:mm:ss.fffff"),
                              InsertedDateTime = menumaster.insertedtime,
                              Active = Convert.ToBoolean(menumaster.active)
                          }).ToList();
            }
            catch (Exception ex)
            {
                //return ex.ToString();
            }
            return result;
        }
        #endregion

        #region Insert
        public int InsertMenuList(MenuList menuList)
        {

            try
            {
                var existsRecord = _context.menumaster.Any(menu => menu.menuname == menuList.menuname);

                if (!existsRecord)
                {
                    _context.menumaster.Add(menuList);
                    _context.SaveChanges();

                    foreach (var entry in _context.ChangeTracker.Entries())
                        entry.State = EntityState.Detached;
                }
                else
                {
                    return 0;
                }

               
            }
            catch(Exception ex)
            {
                ex.ToString();
            }
            return menuList.menuid;
        }
        #endregion

        #region GetBy
        public IEnumerable<MenuListDTO> GetMenuListById( int menuId)
        {
            List<MenuListDTO> result = null;
            try
            {
                result = (from menumaster in _context.menumaster where menumaster.menuid == menuId
                          select new MenuListDTO
                          {
                              MenuId = menumaster.menuid,
                              Menuname = menumaster.menuname,
                              InsertedBy = menumaster.insertedby,
                              InsertedTime = Convert.ToDateTime(menumaster.insertedtime).ToString("yyyy-MM-dd HH:mm:ss.fffff"),
                              Active = Convert.ToBoolean(menumaster.active)
                          }).ToList();
            }
            catch(Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        #endregion

        #region Update
        public string UpdateMenuList(MenuList menuList)
        {
            try
            {

                var existsRecord = _context.menumaster.Any(menu => (menu.menuname == menuList.menuname) && (menu.menuid == menuList.menuid));

                if (existsRecord)
                {
                    _context.menumaster.Update(menuList);
                    _context.SaveChanges();

                    foreach (var entry in _context.ChangeTracker.Entries())
                        entry.State = EntityState.Detached;
                }
                else
                {
                    return "Fail";
                }                

            }
            catch (Exception ex)
            {
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;
                return ex.ToString();
            }
            return "Success";
        }
    }
    #endregion
}
