﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Repository
{
    public class StageRepository : IStageRepository
    {
        private readonly DbWebApiContext _context;

        public StageRepository(DbWebApiContext context)
        {
            _context = context;
        }


        #region Get
        public IEnumerable<StageDTO> GetStageList()
        {
            List<StageDTO> result = null;
            try
            {
                result = (from stage_master in _context.stage_master
                          orderby stage_master.stageid descending
                          where stage_master.active.Equals(true)
                          select new StageDTO
                          {
                              StageId = stage_master.stageid,
                              StageName = stage_master.stagename,

                          }).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;

        }
        #endregion
    }
}
