﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DataPlatform.Repository
{
    public class UserGroupRepository : IUserGroupRepository
    {

        private readonly DbWebApiContext _context;
        public UserGroupRepository(DbWebApiContext context)
        {
            _context = context;
        }


        

        #region Get
        public IEnumerable<UserGroupList> GetUserGroupList()
        {
            List<UserGroupList> result = null;
            List<UserGroupList> resultUserGroup = new List<UserGroupList>();
            List<UserGroupList> resultUserGroupRoles = new List<UserGroupList>();
            ArrayList results = new ArrayList();
            try
            {

                resultUserGroup = (from usergroupmaster in _context.usergroupmaster
                                   orderby usergroupmaster.usergroupid descending
                                   join usergroupmapping in _context.usergroupmapping on usergroupmaster.usergroupid equals usergroupmapping.usergroupid
                                   join usermasters in _context.usermaster on usergroupmapping.userid equals usermasters.userid
                                   orderby usergroupmaster.usergroupname ascending

                                   select new UserGroupList
                                   {
                                       UserGroupMapId = usergroupmapping.id,
                                       UserGroupId = usergroupmaster.usergroupid,
                                       UserId = usergroupmapping.userid,
                                       RoleId = 0,
                                       UserGroupName = usergroupmaster.usergroupname,
                                       UserName = usermasters.firstname + ' ' + usermasters.lastname,
                                       Project = usergroupmaster.projectname,
                                       UserGroupMail = usergroupmaster.group_mailid,
                                       NoOfUsers = usergroupmaster.no_of_user,
                                       RoleName = "",
                                       Active = usergroupmapping.active,
                                       CreatedBy = usermasters.firstname + ' ' + usermasters.lastname,
                                       CreatedOn = Convert.ToDateTime(usergroupmapping.insertedtime).ToString("yyyy-MM-dd HH:mm:ss.fffff"),
                                   }).ToList();


                resultUserGroupRoles = (from usergroupmaster in _context.usergroupmaster
                                        orderby usergroupmaster.usergroupid descending
                                        join usergroupmapping in _context.usergroupmapping on usergroupmaster.usergroupid equals usergroupmapping.usergroupid
                                        join usermasters in _context.usermaster on usergroupmapping.userid equals usermasters.userid
                                        join projecttype_mapping in _context.projecttype_mapping on usergroupmapping.userid equals projecttype_mapping.userid
                                        join role_mapping in _context.role_mapping on projecttype_mapping.id equals role_mapping.projecttype_mapping_id
                                        join rolemaster in _context.rolemaster on role_mapping.roleid equals rolemaster.roleid
                                        orderby usergroupmaster.usergroupname ascending

                                        select new UserGroupList
                                        {
                                            UserGroupMapId = usergroupmapping.id,
                                            UserGroupId = usergroupmaster.usergroupid,
                                            UserId = usergroupmapping.userid,
                                            RoleId = rolemaster.roleid,
                                            UserGroupName = usergroupmaster.usergroupname,
                                            UserName = usermasters.firstname + ' ' + usermasters.lastname,
                                            Project = usergroupmaster.projectname,
                                            UserGroupMail = usergroupmaster.group_mailid,
                                            NoOfUsers = usergroupmaster.no_of_user,
                                            RoleName = rolemaster.role,
                                            Active = usergroupmapping.active,
                                            CreatedBy = usermasters.firstname + ' ' + usermasters.lastname,
                                            CreatedOn = Convert.ToDateTime(usergroupmapping.insertedtime).ToString("yyyy-MM-dd HH:mm:ss.fffff"),
                                        }).ToList();


                resultUserGroupRoles.Select(m => new { m.UserGroupMapId, m.UserId, m.UserGroupId, m.UserGroupName, m.UserName, m.Project, m.UserGroupMail, m.NoOfUsers, m.RoleName, m.Active, m.CreatedBy, m.CreatedOn }).Distinct().ToList();

                for (int i = 0; i <= resultUserGroup.Count - 1; i++)
                {

                    var filteredUsers = resultUserGroupRoles.Where(user => user.UserId == resultUserGroup[i].UserId).ToList();
                    if (filteredUsers.Count > 0)
                    {
                        var iii = resultUserGroup[i];

                        for (int j = 0; j <= filteredUsers.Count - 1; j++)
                        {
                            results.Add(filteredUsers[j]);
                        }

                    }
                    else
                    {
                        results.Add(resultUserGroup[i]);
                    }
                }

                result = results.Cast<UserGroupList>().ToList();
                result.Select(m => new { m.UserGroupMapId, m.UserId, m.UserGroupId, m.UserGroupName, m.UserName, m.Project, m.UserGroupMail, m.NoOfUsers, m.RoleName, m.Active, m.CreatedBy, m.CreatedOn }).Distinct().ToList();

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }
        #endregion


        #region Get Edit By

        public IEnumerable<UserGroupDTO> GetEditUserList(int userGroupId)
        {
            List<UserGroupDTO> result = null;
            try
            {

                result = (from usergroupmaster in _context.usergroupmaster
                          join usergroupmapping in _context.usergroupmapping on usergroupmaster.usergroupid equals usergroupmapping.usergroupid
                          join usermaster in _context.usermaster on usergroupmaster.insertedby equals usermaster.userid
                          join projectmaster in _context.projectmaster on usergroupmapping.projectid equals projectmaster.id
                          where usergroupmaster.usergroupid == userGroupId
                          select new UserGroupDTO
                          {
                              UserGroupId = usergroupmaster.usergroupid,
                              UserGroupName = usergroupmaster.usergroupname,
                              UserId = usergroupmapping.userid,
                              Active = usergroupmaster.active,
                              ProjectId = usergroupmapping.projectid,
                              ProjectName = usergroupmaster.projectname,
                              GroupMailId = usergroupmaster.group_mailid,

                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        #endregion


        #region Get Project By
        public IEnumerable<UserRoleList> GetProjectUserRoleList(int projectid)
        {
            List<UserRoleList> result = null;
            try
            {

                result = (from project_user_role_mapping in _context.project_user_role_mapping
                          join usermaster in _context.usermaster on project_user_role_mapping.userid equals usermaster.userid
                          join rolemaster in _context.rolemaster on project_user_role_mapping.roleid equals rolemaster.roleid
                          where project_user_role_mapping.projectid.Equals(projectid)
                          select new UserRoleList
                          {
                              UserId = usermaster.userid,
                              UserName = usermaster.firstname + ' ' + usermaster.lastname,
                              RoleId = rolemaster.roleid,
                              RoleName = rolemaster.role
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        #endregion

        #region Insert
        public string InsertUserGroup(UserGroup objUserGroup, List<UserGroupMapping> userGroupMappings)
        {
            try
            {
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;


                if (_context.usergroupmaster.Where(u => u.usergroupname.ToLower() == objUserGroup.usergroupname.ToLower()).Any())
                {
                    return "Failed_UsergroupName";
                }
                else if (_context.usergroupmaster.Where(u => u.group_mailid == objUserGroup.group_mailid).Any())
                {
                    return "Failed_GroupMailId";
                }
                else
                {
                    _context.usergroupmaster.Add(objUserGroup);
                    _context.SaveChanges();

                    for (int i = 0; i <= userGroupMappings.Count - 1; i++)
                    {
                        userGroupMappings[i].usergroupid = objUserGroup.usergroupid;
                        _context.usergroupmapping.Add(userGroupMappings[i]);
                        _context.SaveChanges();
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return "Success";
        }
        #endregion




        #region Update

        public string UpdateUserGroup(UserGroup objUserGroup, UserGroup objUpdateUserGroup, List<UserGroupMapping> userGroupMappings)
        {
            try
            {
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;

                

                if (_context.usergroupmaster.Where(u => u.usergroupname.ToLower() == objUpdateUserGroup.usergroupname.ToLower() && u.usergroupid != objUserGroup.usergroupid).Any())
                {
                    return "Failed_UsergroupName";
                }
                else if (_context.usergroupmaster.Where(u => u.group_mailid == objUpdateUserGroup.group_mailid && u.usergroupid != objUserGroup.usergroupid).Any())
                {
                    return "Failed_GroupMailId";
                }
                else
                {

                    var userGroupMappingPar = _context.usergroupmapping.Where(m => m.usergroupid == objUserGroup.usergroupid).ToList();
                    foreach (var userGroupMapping in userGroupMappingPar)
                    {
                        _context.usergroupmapping.Remove(userGroupMapping);
                    }

                    _context.usergroupmaster.Remove(objUserGroup);
                    _context.SaveChanges();

                    _context.usergroupmaster.Add(objUpdateUserGroup);
                    _context.SaveChanges();

                    for (int i = 0; i <= userGroupMappings.Count - 1; i++)
                    {
                        userGroupMappings[i].usergroupid = objUpdateUserGroup.usergroupid;
                        _context.usergroupmapping.Add(userGroupMappings[i]);
                        _context.SaveChanges();
                    }
                }


            }
            catch (Exception)
            {
                throw;
            }

            return "Success";
        }
        #endregion

        #region Delete
        public string DeleteUserGroup(UserGroup userGroup)
        {
            try
            {
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;

                var userGroupMappings = _context.usergroupmapping.Where(m => m.usergroupid == userGroup.usergroupid).ToList();
                foreach (var userGroupMapping in userGroupMappings)
                {
                    _context.usergroupmapping.Remove(userGroupMapping);
                }
                
                _context.usergroupmaster.Remove(userGroup);
                _context.SaveChanges();

                
            }
            catch (Exception)
            {
                throw;
            }

            return "Success";
        }
        #endregion


    }
}
