﻿using Dapper;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using Nancy.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Text.Json;

namespace DataPlatform.Repository
{
    public class ServicesCatalogRepository : IServicesCatalogRepository
    {
        private readonly DbWebApiContext _context;
        // private readonly DapperContext _dapperContext;
        public ServicesCatalogRepository(DbWebApiContext context)
        {
            _context = context;
            //_dapperContext = dapperContext;
        }
        #region Select All
        public List<Stage> GetServiceCatalogDetails()
        {

            ServiceCatalogDTO serviceCatalogDTO = new ServiceCatalogDTO();
            var connection = _context.Database.GetDbConnection();

            DynamicParameters parameter = new DynamicParameters();

            var data = connection.Query<ServiceCatalogDTO>("usp_stage_data", parameter, commandType: CommandType.StoredProcedure).ToList();
            var query = from bug in data
                        group bug by new { bug.StageID, bug.ModuleId, bug.SubModuleId } into grouped
                        where grouped.Any(x => x.ModuleId != x.SubModuleId)
                        select new
                        {
                            StageID = grouped.Key.StageID,
                            ModuleId = grouped.Key.ModuleId,
                            SubModuleId = grouped.Key.SubModuleId,
                            modukes = grouped.ToList(),
                            Count = grouped.Count()
                        };


            var ToLookupwithMultipleKeys = data
                                        .ToLookup(x => new { x.StageName, x.ModuleName })
                                        .OrderByDescending(g => g.Key.StageName).ThenBy(g => g.Key.ModuleName)
                                        .Select(g => new
                                        {
                                            Stage = g.Key.StageName,
                                            Module = g.Key.ModuleName,
                                            Submodule = g.OrderBy(x => new  {x.SubModuleName, x.Privilege_Name })
                                        });


            //var ToLookupwithMultipleKeys = query
            //                           .ToLookup(x => new { x.StageID, x.ModuleId, x.SubModuleId })
            //                           .OrderByDescending(g => g.Key.StageID).ThenBy(g => g.Key.ModuleId)
            //                           .Select(g => new
            //                           {
            //                               Branch = g.Key.StageID,
            //                               Gender = g.Key.ModuleId,
            //                               Students = g.OrderBy(x => x.SubModuleId)
            //                           });


            var groups = query
    .OrderBy(x => x.StageID).ThenBy(x => x.StageID )
    .ToLookup(x => new { x.StageID,x.ModuleId,x.SubModuleId });

           

            //        List<dynamic> finalList = new List<dynamic>();

            var queryNestedGroup = from stage in data
                                   group stage by stage.StageID into newGroup1
                                   from newGroup2 in
                                   (from module in newGroup1
                                    group module by module.ModuleId)
                                   group newGroup2 by newGroup1.Key;

            //into newGroup3
            //                       from submodule in newGroup3
            //                       group submodule by submodule.Key;


            List<Stage> stages = new List<Stage>();
            ArrayList myList = new ArrayList();

            foreach (var outerGroup in queryNestedGroup)
            {
                Stage stage = new Stage();
                stage.StageID = outerGroup.Key;

                stage.Modules = new List<Module>();
                foreach (var innerGroup in outerGroup)
                {
                    Module module = new Module();
                    module.ModuleId = innerGroup.Key;

                    module.Submodules = new List<Submodule>();
                    foreach (var innerGroupElement in innerGroup)
                    {
                        stage.StageName = innerGroupElement.StageName;
                        module.ModuleName = innerGroupElement.ModuleName;

                        Submodule submodule = new Submodule();                         
                        if (innerGroupElement.ModuleName != innerGroupElement.SubModuleName)
                        {
                            submodule.SubModuleId = innerGroupElement.SubModuleId;
                            //LocalDataStoreSlot localDataStoreSlot = new LocalDataStoreSlot();
                            submodule.SubModuleName = innerGroupElement.SubModuleName;
                           

                            submodule.Privileges = new List<Privilege>();
                            foreach (var Privileges in innerGroup)
                            {
                                if(submodule.SubModuleId == Privileges.SubModuleId)
                                {
                                    Privilege privilege = new Privilege();
                                    if (Privileges.ModuleName != Privileges.SubModuleName)
                                    {
                                        privilege.PrivilegeId = Privileges.PrivilegeId;
                                        privilege.Privilege_Name = Privileges.Privilege_Name;
                                        submodule.Privileges.Add(privilege);
                                    }
                                }
                               

                            }
                            var t = module.Submodules.Count;
                            

                            

                            if (module.Submodules.Count == 0)
                            {
                                module.Submodules.Add(submodule);
                                myList.Add(submodule.SubModuleId);
                            }
                            else
                            {
                                if (myList.Contains(submodule.SubModuleId))
                                {
                                    
                                }
                                else
                                {
                                    myList.Add(submodule.SubModuleId);
                                    module.Submodules.Add(submodule);

                                }
                            }
                           
                        }


                    }

                    stage.Modules.Add(module);
                }

                stages.Add(stage);
            }
            return stages;
        }
        #endregion

        #region Select By Id
        public IEnumerable<ModuleSubmoduleMasterDto>  GetServiceCatalogDetailsById(int id)
        {
            List<ModuleSubmoduleMasterDto> moduleSubmoduleMasterDto = null;
            try
            {
                moduleSubmoduleMasterDto = (from moduleSubmoduleDetails in _context.module_submodule_master
                                         where moduleSubmoduleDetails.submoduleid == id
                                         select new ModuleSubmoduleMasterDto
                                         { 
                                             ModuleId = moduleSubmoduleDetails.moduleid,
                                             SubModuleId = moduleSubmoduleDetails.submoduleid,
                                             ModuleName = moduleSubmoduleDetails.modulename,
                                             SubModuleName = moduleSubmoduleDetails.submodulename,
                                             Active = moduleSubmoduleDetails.active
                                         }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return moduleSubmoduleMasterDto;
        }

        public IEnumerable<ModuleSubmoduleMasterDto> GetPrivilegeServiceCatalogDetailsById(int id)
        {
            List<ModuleSubmoduleMasterDto> moduleSubmoduleMasterDto = null;
            try
            {
                moduleSubmoduleMasterDto = (from privilegeDetails in _context.privilege_master
                                            where privilegeDetails.id == id
                                            select new ModuleSubmoduleMasterDto
                                            {
                                                PrivilegeId = privilegeDetails.id,
                                                PrivilegeName = privilegeDetails.privilegename,
                                                Active = privilegeDetails.active
                                            }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return moduleSubmoduleMasterDto;
        }
        #endregion

        #region Insert
        public string InsertServiceCatalogDetails(List<ServiceCatalogDTO> serviceCatalogDTOs)
        {
            List<ServiceCatalog> listServiceCatalog;
            string Success;
            try
            {
                listServiceCatalog = new List<ServiceCatalog>();

                for(int i = 0; i <= serviceCatalogDTOs.Count - 1; i++)
                {
                    ServiceCatalog serviceCatalog = new ServiceCatalog()
                    {
                        stagename = serviceCatalogDTOs[i].StageName,
                        modulename = serviceCatalogDTOs[i].ModuleName,
                        submodulename = serviceCatalogDTOs[i].SubModuleName == null ? "" : serviceCatalogDTOs[i].SubModuleName,
                        Privilege = serviceCatalogDTOs[i].Privilege_Name
                    };

                    listServiceCatalog.Add(serviceCatalog);
                }
           
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var data = serializer.Serialize(listServiceCatalog);


                var connection = _context.Database.GetDbConnection();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@inputjson", data);

                var datas = connection.Query<dynamic>("usp_stage_master_insert", parameter, commandType: CommandType.StoredProcedure).ToList();

                if (datas[0].usp_stage_master_insert == true)
                {
                    Success = "Success";
                }
                else
                {
                    Success = "Failed";
                }
            }
            catch(Exception)
            {
                throw;
            }
            return Success;
        }
        #endregion

        #region Update
        public string UpdateServiceCatalogDetails(UpdateServicecatalogdetails updateServicecatalogdetails)
        {
            string Success;
            try
            {
                var connection = _context.Database.GetDbConnection();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@process_id", updateServicecatalogdetails.ProcessId);
                parameter.Add("@primaryid", updateServicecatalogdetails.Id);
                parameter.Add("@name", updateServicecatalogdetails.Name);
                parameter.Add("@active_flag", updateServicecatalogdetails.Active);

                //parameter.Add("@process_id", 1);
                //parameter.Add("@primaryid", 16);
                //parameter.Add("@name", "Testing");
                //parameter.Add("@active_flag", true);
                var data = connection.Query<dynamic>("usp_stage_module_process", parameter, commandType: CommandType.StoredProcedure).ToList();

                if (data[0].usp_stage_module_process == true)
                {
                    Success = "Success";
                }
                else
                {
                    Success = "Failed";
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Success;
        }
        #endregion
    }
}
