﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Repository
{
    public class AdminUserBoardRepository: IAdminUserBoardRepository
    {
        private readonly DbWebApiContext _context;

        public AdminUserBoardRepository(DbWebApiContext context)
        {
            _context = context;
        }

        #region Get
        public IEnumerable<AdminUserBoardDTO> GetAdminUserBoardList()
        {
            List<AdminUserBoardDTO> result = null;
            try
            {
                result = (from adminbasicinformation in _context.adminbasicinformation
                          orderby adminbasicinformation.adminid descending
                          join usermaster in _context.usermaster on adminbasicinformation.adminid equals usermaster.adminid
                          join clientbasicinformation in _context.clientbasicinformation on adminbasicinformation.clientid equals clientbasicinformation.clientid
                          join userlogin in _context.userlogin on usermaster.userid equals userlogin.userid
                          select new AdminUserBoardDTO
                          {
                              AdminId = adminbasicinformation.adminid,
                              ClientId = adminbasicinformation.clientid,
                              UserLoginId = userlogin.userloginid,
                              UserId = usermaster.userid,
                              OrganizationName = clientbasicinformation.organization_name,
                              AdminName = usermaster.firstname + ' ' + usermaster.lastname,
                              FirstName = usermaster.firstname,
                              LastName = usermaster.lastname,
                              Password = userlogin.password,
                              PointOfContact = adminbasicinformation.point_of_contact,
                              PhoneNumber = adminbasicinformation.phone_number,
                              Address = adminbasicinformation.address,
                              EmailId = adminbasicinformation.email_id,
                              Active = adminbasicinformation.active,
                              RecordStatus = adminbasicinformation.recordstatus,
                              InsertedBy = adminbasicinformation.insertedby,
                              UpdatedBy = adminbasicinformation.updatedby,
                              CreatedBy = usermaster.firstname + ' ' + usermaster.lastname,
                              InsertedDateTime = Convert.ToDateTime(adminbasicinformation.insertedtime).ToString("yyyy-MM-dd HH:mm:ss.fffff"),
                              UpdatedTime = adminbasicinformation.updatedtime,
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;

        }
        #endregion


        #region Get By Id
        public IEnumerable<AdminUserBoardDTO> GetAdminUserBoardListById(int clientId)
        {
            List<AdminUserBoardDTO> result = null;

            try
            {
                result = (from adminbasicinformation in _context.adminbasicinformation
                          orderby adminbasicinformation.clientid descending
                          join usermaster in _context.usermaster on adminbasicinformation.adminid equals usermaster.adminid
                          join clientbasicinformation in _context.clientbasicinformation on adminbasicinformation.clientid equals clientbasicinformation.clientid
                          join userlogin in _context.userlogin on usermaster.userid equals userlogin.userid
                          where adminbasicinformation.clientid.Equals(clientId)
                          select new AdminUserBoardDTO
                          {
                              AdminId = adminbasicinformation.adminid,
                              ClientId = adminbasicinformation.clientid,
                              AdminName = adminbasicinformation.admin_name,
                              PointOfContact = adminbasicinformation.point_of_contact,
                              PhoneNumber = adminbasicinformation.phone_number,
                              Address = adminbasicinformation.address,
                              EmailId = adminbasicinformation.email_id,
                              Active = adminbasicinformation.active,
                              RecordStatus = adminbasicinformation.recordstatus,
                              InsertedBy = adminbasicinformation.insertedby,
                              UpdatedBy = adminbasicinformation.updatedby,
                              CreatedBy = usermaster.firstname + ' ' + usermaster.lastname,
                              InsertedDateTime = Convert.ToDateTime(adminbasicinformation.insertedtime).ToString("yyyy-MM-dd HH:mm:ss.fffff"),
                              UpdatedTime = adminbasicinformation.updatedtime,
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        #endregion


        #region Insert
        public int InsertAdminUserBoard(AdminUserBoard adminUserBoard, UserManagement user, Login login)
        {
            try
            {
                //var existsRecord = _context.usermaster.Any(um => um.emailid == adminUserBoard.email_id);
                var adminResult = from adminbasicinformation in _context.adminbasicinformation
                             where adminbasicinformation.email_id.ToLower() == adminUserBoard.email_id.ToLower()
                             select new { adminId = adminbasicinformation.adminid, email = adminbasicinformation.email_id };

                var userMasterResult = from usermaster in _context.usermaster
                             where usermaster.emailid.ToLower() == adminUserBoard.email_id.ToLower()
                             select new { userId = usermaster.userid, email = usermaster.emailid };

                var userLoginResult = from userlogin in _context.userlogin
                             where userlogin.username.ToLower() == adminUserBoard.email_id.ToLower()
                             select new { userId = userlogin.userid, email = userlogin.username };

                if (!adminResult.Any() && !userMasterResult.Any() && !userLoginResult.Any())
                {
                    foreach (var entry in _context.ChangeTracker.Entries())
                        entry.State = EntityState.Detached;

                    var id = _context.adminbasicinformation.Add(adminUserBoard);
                    _context.SaveChanges();

                    user.adminid = adminUserBoard.adminid;
                    _context.usermaster.Add(user);
                    _context.SaveChanges();

                    login.userid = user.userid;
                    _context.userlogin.Add(login);
                    _context.SaveChanges();

                }
                else
                {
                    return 0;
                }


            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return login.userloginid;
        }
        #endregion


        #region Update
        public string UpdateAdminUserBoard(AdminUserBoard adminUserBoard, UserManagement user, Login login)
        {
            try
            {
                
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;


                var existClientNameAndEmail = _context.adminbasicinformation.Any(abi => (abi.clientid == adminUserBoard.clientid) && (abi.email_id.ToLower() == adminUserBoard.email_id.ToLower()));

                var existClientEmail = _context.adminbasicinformation.Any(abi => (abi.email_id.ToLower() == adminUserBoard.email_id.ToLower()));

                var existClientName = _context.adminbasicinformation.Any(abi => (abi.clientid == adminUserBoard.clientid));

                if (!existClientEmail && !existClientNameAndEmail)
                {
                    _context.userlogin.Update(login);
                    _context.SaveChanges();

                    _context.usermaster.Update(user);
                    _context.SaveChanges();

                    _context.adminbasicinformation.Update(adminUserBoard);
                    _context.SaveChanges();

                }
                else if (!existClientNameAndEmail && existClientEmail && existClientName == false || !existClientNameAndEmail && !existClientEmail && existClientName == true)
                {
                    _context.userlogin.Update(login);
                    _context.SaveChanges();

                    _context.usermaster.Update(user);
                    _context.SaveChanges();

                    _context.adminbasicinformation.Update(adminUserBoard);
                    _context.SaveChanges();
                }
                else if (existClientEmail == true && existClientNameAndEmail == false)
                {
                    return "Failed";
                }
                else
                {
                    _context.userlogin.Update(login);
                    _context.SaveChanges();

                    _context.usermaster.Update(user);
                    _context.SaveChanges();

                    _context.adminbasicinformation.Update(adminUserBoard);
                    _context.SaveChanges();
                }


                
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            return "Success";
        }
        #endregion


        #region Delete
        public string DeleteAdminUserBoard(AdminUserBoard adminUserBoard, UserManagement user, Login login)
        {
            foreach (var entry in _context.ChangeTracker.Entries())
                entry.State = EntityState.Detached;
            try
            {
                _context.adminbasicinformation.Remove(adminUserBoard);
                _context.SaveChanges();

                _context.userlogin.Remove(login);
                _context.SaveChanges();

                _context.usermaster.Remove(user);
                _context.SaveChanges();

               

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            return "Success";
        }
        #endregion

    }
}
