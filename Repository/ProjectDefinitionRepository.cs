﻿using Dapper;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using Nancy.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Repository
{
    public class ProjectDefinitionRepository: IProjectDefinitionRepository
    {
        private readonly DbWebApiContext _context;
        public ProjectDefinitionRepository(DbWebApiContext context)
        {
            _context = context;
        }


        #region Select All
        public IEnumerable<ProjectDefinitionDTO> GetProjectList()
        {
            List<ProjectDefinitionDTO> result = null;
            try
            {
                result = (from projectmaster in _context.projectmaster
                          join usermaster in _context.usermaster on projectmaster.insertedby equals usermaster.userid
                          join projecttype_master in _context.projecttype_master on projectmaster.projecttypeid equals projecttype_master.projecttypeid
                          join project_mapping in _context.project_mapping on projectmaster.id equals project_mapping.projectid
                          orderby projectmaster.id descending
                          select new ProjectDefinitionDTO
                          {
                              ProjectId = projectmaster.id,
                              ProjectName = projectmaster.projectname,
                              ProjectTypeId = projectmaster.projecttypeid,
                              ProjectTypeName = projecttype_master.projecttype,
                              ProjectDescription = projectmaster.projectdescription,
                              DisplayOrder = projectmaster.displayorder,
                              Active = projectmaster.active,
                              CreatedBy = usermaster.firstname + ' ' + usermaster.lastname,
                              InsertedBy = projectmaster.insertedby,
                              InsertedDateTime = Convert.ToDateTime(projectmaster.insertedtime).ToString("MM-dd-yyyy")

                          }).Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        #endregion


        #region Get Project Stage list
        public IEnumerable<ProjectStageList> GetProjectStageList(int projectTypeId)
        {
            List<ProjectStageList> result = null;
            try
            {

                result = (from projecttype_mapping in _context.projecttype_mapping
                          join projecttype_master in _context.projecttype_master on projecttype_mapping.projecttypeid equals projecttype_master.projecttypeid
                          join stage_master in _context.stage_master on projecttype_mapping.stageid equals stage_master.stageid
                          join module_submodule_master in _context.module_submodule_master on projecttype_mapping.submoduleid equals module_submodule_master.submoduleid
                          where projecttype_mapping.projecttypeid.Equals(projectTypeId)
                          select new ProjectStageList
                          {
                              ProjectTypeMapId = projecttype_mapping.id,
                              ProjectTypeid = projecttype_master.projecttypeid,
                              ProjectTypeName = projecttype_master.projecttype,
                              StageId = stage_master.stageid,
                              StageName = stage_master.stagename,
                              GeneralQuestions = stage_master.general_questions,
                              ModuleId = module_submodule_master.moduleid,
                              ModuleName = module_submodule_master.modulename,
                              SubModuleId = module_submodule_master.submoduleid,
                              SubModuleName = module_submodule_master.submodulename,

                          }).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }

            return result;
        }
        #endregion

        #region Insert 
        public string InsertProjectDefinition(List<ProjectMappingParams> projectMappingParams)
        {
            List<ProjectMappingList> listProjectMappings;
            string Success;
            try
            {

                var existProjectName = _context.projectmaster.Any(pm => (pm.projectname.ToLower() == projectMappingParams[0].ProjectName.ToLower()));
                if (existProjectName)
                {
                    return "Failed";
                }

                listProjectMappings = new List<ProjectMappingList>();

                for (int i = 0; i <= projectMappingParams.Count - 1; i++)
                {
                    ProjectMappingList projectMapping = new ProjectMappingList()
                    {
                        projectname = projectMappingParams[i].ProjectName,
                        projectdescription = projectMappingParams[i].ProjectDescription,
                        projecttypeid = projectMappingParams[i].ProjectTypeId,
                        userid = projectMappingParams[i].UserId,
                        stageid = projectMappingParams[i].StageId,
                        submoduleid = projectMappingParams[i].SubModuleId,
                        displayorder = projectMappingParams[i].DisplayOrder,
                        active = projectMappingParams[i].Active,
                        flag = projectMappingParams[i].Flag,
                    };

                    listProjectMappings.Add(projectMapping);
                }

                var questions = projectMappingParams[0].Questions.ToString();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var data = serializer.Serialize(listProjectMappings);

                var connection = _context.Database.GetDbConnection();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@project_general_qans", questions);
                parameter.Add("@inputjson", data);

                var datas = connection.Query<dynamic>("usp_project_insert", parameter, commandType: CommandType.StoredProcedure).ToList();

                if (datas[0].usp_project_insert == "true")
                {
                    Success = "Success";
                }
                else if (datas[0].usp_project_insert == "false")
                {
                    Success = "Failed";
                }
                else
                {
                    Success = datas[0].usp_project_insert;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Success;
        }
        #endregion


        #region Get By Id
        public IEnumerable<ProjectDefinitionDTO> GetProjectDefinitionListById(int projectId)
        {
            List<ProjectDefinitionDTO> result = null;

            try
            {
                result = (from projectmaster in _context.projectmaster
                          join project_mapping in _context.project_mapping on projectmaster.id equals project_mapping.projectid
                          join projecttype_mapping in _context.projecttype_mapping on project_mapping.projecttype_mapping_id equals projecttype_mapping.id
                          join projecttype_master in _context.projecttype_master on projecttype_mapping.projecttypeid equals projecttype_master.projecttypeid
                          join stage_master in _context.stage_master on projecttype_mapping.stageid equals stage_master.stageid
                          join module_submodule_master in _context.module_submodule_master on projecttype_mapping.submoduleid equals module_submodule_master.submoduleid
                          where project_mapping.projectid.Equals(
                              projectId)
                          select new ProjectDefinitionDTO
                          {
                              ProjectId = projectmaster.id,
                              ProjectName = projectmaster.projectname,
                              ProjectGeneralQA = projectmaster.project_general_qa,
                              ProjectTypeId = projectmaster.projecttypeid,
                              ProjectTypeName = projecttype_master.projecttype,
                              ProjectDescription = projectmaster.projectdescription,
                              UserId = projecttype_mapping.userid,
                              StageId = projecttype_mapping.stageid,
                              StageName = stage_master.stagename,
                              ModuleId = module_submodule_master.moduleid,
                              ModuleName = module_submodule_master.modulename,
                              SubModuleId = projecttype_mapping.submoduleid,
                              SubModuleName = module_submodule_master.submodulename,
                              Active = projectmaster.active,
                              RecordStatus = projectmaster.recordstatus

                          }).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        #endregion


        #region update
        public string UpdateProjectDefinition(List<ProjectMappingParams> projectMappingParams)
        {
            List<ProjectMappingList> listProjectMappings;
            string Success;
            try
            {

                var existProjectName = _context.projectmaster.Any(pm => (pm.projectname.ToLower() == projectMappingParams[0].ProjectName.ToLower()) && (pm.id != projectMappingParams[0].ProjectId));
                if (existProjectName)
                {
                    return "Failed";
                }


                listProjectMappings = new List<ProjectMappingList>();

                for (int i = 0; i <= projectMappingParams.Count - 1; i++)
                {
                    ProjectMappingList projectMapping = new ProjectMappingList()
                    {
                        projectid = projectMappingParams[i].ProjectId,
                        projectname = projectMappingParams[i].ProjectName,
                        projectdescription = projectMappingParams[i].ProjectDescription,
                        projecttypeid = projectMappingParams[i].ProjectTypeId,
                        userid = projectMappingParams[i].UserId,
                        stageid = projectMappingParams[i].StageId,
                        submoduleid = projectMappingParams[i].SubModuleId,
                        displayorder = projectMappingParams[i].DisplayOrder,
                        active = projectMappingParams[i].Active,
                        flag = projectMappingParams[i].Flag,
                    };

                    listProjectMappings.Add(projectMapping);
                }

                var questions = projectMappingParams[0].Questions.ToString();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var data = serializer.Serialize(listProjectMappings);

                

                var connection = _context.Database.GetDbConnection();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@project_general_qans", questions);
                parameter.Add("@inputjson", data);

                var datas = connection.Query<dynamic>("usp_project_insert", parameter, commandType: CommandType.StoredProcedure).ToList();

                if (datas[0].usp_project_insert == "true")
                {
                    Success = "Success";
                }
                else if (datas[0].usp_project_insert == "false")
                {
                    Success = "Failed";
                }
                else
                {
                    Success = datas[0].usp_project_insert;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Success;
        }
        #endregion

        #region delete
        public string DeleteProjectDefinition(int projectId)
        {

            string Success;
            try
            {
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;

                var projectMappings = _context.project_mapping.Where(m => m.projectid == projectId).ToList();
                foreach (var projectMapping in projectMappings)
                {
                    _context.project_mapping.Remove(projectMapping); 
                    _context.SaveChanges();
                }

                var projectmaster = _context.projectmaster.FirstOrDefault(s => s.id == projectId);
                if (projectmaster != null)
                {
                    _context.projectmaster.Remove(projectmaster);
                    _context.SaveChanges();
                }

           Success = "Success";

            }
            catch (Exception)
            {
                throw;
            }

          return Success;
        }
        #endregion
    }
}
