﻿using Dapper;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using Nancy.Json;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataPlatform.Repository
{
    public class UserManagementRepository : IUserManagementRepository
    {
        private readonly DbWebApiContext _context;

        public UserManagementRepository(DbWebApiContext context)
        {
            _context = context;
        }




        #region Get
        public IEnumerable<UserManagementDTO> GetUserManagementList()
        {
            List<UserManagementDTO> result = null;
            try
            {
                result = (from usermaster in _context.usermaster
                          orderby usermaster.userid descending
                          join userlogin in _context.userlogin on usermaster.userid equals userlogin.userid
                          select new UserManagementDTO
                          {
                              UserId = usermaster.userid,
                              RoleId = userlogin.roleid,
                              UserLoginId = userlogin.userloginid,
                              ClientId = usermaster.clientid == null ? 0 : usermaster.clientid,
                              AdminId = usermaster.adminid == null ? 0 : usermaster.adminid,
                              FirstName = usermaster.firstname,
                              LastName = usermaster.lastname,
                              EmailId = usermaster.emailid,
                              Phone = usermaster.phone,
                              WorkPhone = usermaster.workphone,
                              Active = usermaster.active,
                              RecordStatus = usermaster.recordstatus,
                              InsertedBy = usermaster.insertedby,
                              InsertedDateTime = Convert.ToDateTime(usermaster.insertedtime).ToString("yyyy-MM-dd HH:mm:ss.fffff")
                          }).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;

        }
        #endregion

        #region GetBy
        public IEnumerable<UserManagementDTO> GetUserManagementListById(int userId)
        {
            List<UserManagementDTO> result = null;
            try
            {

                result = (from usermaster in _context.usermaster
                          orderby usermaster.userid descending
                          where usermaster.userid == userId
                          select new UserManagementDTO
                          {
                              UserId = usermaster.userid,
                              FirstName = usermaster.firstname,
                              LastName = usermaster.lastname,
                              EmailId = usermaster.emailid,
                              Phone = usermaster.phone,
                              WorkPhone = usermaster.workphone,
                              Active = usermaster.active,
                              RecordStatus = usermaster.recordstatus,
                              InsertedBy = usermaster.insertedby,
                              InsertedDateTime = Convert.ToDateTime(usermaster.insertedtime).ToString("yyyy-MM-dd HH:mm:ss.fffff"),


                          }).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        #endregion


        #region Insert
        public int InsertUserManagement(UserManagement userManagement)
        {

            try
            {
                var existEmail = _context.usermaster.Any(um => (um.emailid == userManagement.emailid) && (um.clientid == userManagement.clientid));
                if (!existEmail)
                {
                    var id = _context.usermaster.Add(userManagement);
                    _context.SaveChanges();
                    foreach (var entry in _context.ChangeTracker.Entries())
                        entry.State = EntityState.Detached;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return userManagement.userid;
        }

        #endregion

        #region BulkUploadValidation
        public dynamic BulkUpload(List<UserManagementDTO> userManagementDTOs)
        {
            List<UserManagement> userManagement;
            string Success;
            object datas;
            try
            {

                userManagement = new List<UserManagement>();

                for (int i = 0; i <= userManagementDTOs.Count - 1; i++)
                {
                    UserManagement userManagementList = new UserManagement()
                    {
                        clientid = userManagementDTOs[i].ClientId,
                        firstname = userManagementDTOs[i].FirstName,
                        lastname = userManagementDTOs[i].LastName,
                        emailid = userManagementDTOs[i].EmailId,
                        phone = userManagementDTOs[i].Phone,
                        workphone = userManagementDTOs[i].WorkPhone,
                        recordstatus = userManagementDTOs[i].RecordStatus,
                        insertedby = userManagementDTOs[i].InsertedBy,
                        insertedtime = userManagementDTOs[i].InsertedTime
                    };

                    userManagement.Add(userManagementList);
                }


                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var data = serializer.Serialize(userManagement);

                var connection = _context.Database.GetDbConnection();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@inputjson", data);


                datas = connection.Query<dynamic>("usp_usermaster_insert", parameter, commandType: System.Data.CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                throw;
            }

            return datas;
        }
        #endregion

        #region Insert
        public int BulkInsertUserManagement(List<UserManagement> listofusers)
        {

            try
            {
                //var data = listofusers[0];
                for (int i = 0; i < listofusers.Count - 1; i++)
                {
                    var existEmail = _context.usermaster.Any(um => (um.emailid == listofusers[i].emailid) && (um.clientid == listofusers[i].clientid));
                    if (!existEmail)
                    {
                        var id = _context.usermaster.Add(listofusers[i]);
                        _context.SaveChanges();
                        foreach (var entry in _context.ChangeTracker.Entries())
                            entry.State = EntityState.Detached;
                    }
                    else
                    {
                        return 0;
                    }
                }

                //JavaScriptSerializer serializer = new JavaScriptSerializer();
                //var data = serializer.Serialize(listofusers);


                //var connection = _context.Database.GetDbConnection();

                //DynamicParameters parameter = new DynamicParameters();
                //parameter.Add("@inputjson", data);

                //var datas = connection.Query<dynamic>("usp_usermaster_validation", parameter, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 1;
        }

        #endregion

        #region Update
        public string UpdateUserManagement(UserManagement userManagement)
        {
            try
            {
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;

                if (_context.usermaster.Where(um => um.userid == userManagement.userid && um.emailid == userManagement.emailid).Any())
                {
                    _context.usermaster.Update(userManagement);
                    _context.SaveChanges();
                }
                else if (_context.usermaster.Where(um => um.userid != userManagement.userid && um.emailid == userManagement.emailid).Any())
                {
                    return "Failed";
                }
                else
                {
                    _context.usermaster.Update(userManagement);
                    _context.SaveChanges();
                }


            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            return "Success";
        }
        #endregion

        #region Delete
        public string DeleteUserManagement(UserManagement userManagement, Login login)
        {
            try
            {
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;

                _context.userlogin.Remove(login);
                _context.SaveChanges();

                _context.usermaster.Remove(userManagement);
                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            return "Success";
        }


        #endregion

    }

}
