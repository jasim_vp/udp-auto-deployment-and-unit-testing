﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataPlatform.Repository
{
    public class ClientDetailsRepository : IClientDetailsRepository
    {
        private readonly DbWebApiContext _context;
        public ClientDetailsRepository(DbWebApiContext context)
        {
            _context = context;
        }

        #region Select All
        public IEnumerable<ClientDetailsDTO> GetClientDetails()
        {
            List<ClientDetailsDTO> result = null;
            try
            {
                result = (from clientbasicinformation in _context.clientbasicinformation
                          orderby clientbasicinformation.clientid descending
                         // where clientbasicinformation.recordstatus.Equals(1)
                          join usermaster in _context.usermaster on clientbasicinformation.clientid equals usermaster.clientid
                          join userlogin in _context.userlogin on usermaster.userid equals userlogin.userid
                          select new ClientDetailsDTO
                          {
                              ClientId = usermaster.clientid,
                              UserId = userlogin.userid,
                              UserLoginId = userlogin.userloginid,
                              OrganizationName = clientbasicinformation.organization_name,
                              Address = clientbasicinformation.address,
                              EmailId = clientbasicinformation.email_id,
                              PointOfContact = clientbasicinformation.point_of_contact,
                              PhoneNumber = usermaster.phone,
                              WebUrl = clientbasicinformation.web_url,
                              FirstName = usermaster.firstname,
                              LastName = usermaster.lastname,
                              Password = userlogin.password,
                              Active = clientbasicinformation.active,
                              CreatedBy = usermaster.firstname + ' ' + usermaster.lastname,
                              InsertedBy = clientbasicinformation.insertedby,
                              InsertedDateTime = Convert.ToDateTime(clientbasicinformation.insertedtime).ToString("yyyy-MM-dd HH:mm:ss.fffff")
                              
                              
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        #endregion


        #region GetBy
        public IEnumerable<ClientDetailsDTO> GetClientDetailsListById(int clientId)
        {
            List<ClientDetailsDTO> result = null;
            try
            {

                result = (from clientbasicinformation in _context.clientbasicinformation
                          orderby clientbasicinformation.clientid descending
                          join usermaster in _context.usermaster on clientbasicinformation.clientid equals usermaster.clientid
                          join userlogin in _context.userlogin on usermaster.userid equals userlogin.userid
                          where clientbasicinformation.clientid == clientId
                          select new ClientDetailsDTO
                          {
                              ClientId = clientbasicinformation.clientid,
                              UserId = usermaster.userid,
                              UserLoginId = userlogin.userloginid,
                              OrganizationName = clientbasicinformation.organization_name,
                              Address = clientbasicinformation.address,
                              FirstName = usermaster.firstname,
                              LastName = usermaster.lastname,
                              Password = userlogin.password,
                              EmailId = usermaster.emailid,
                              PhoneNumber = usermaster.phone,
                              WebUrl = clientbasicinformation.web_url,
                              Active = clientbasicinformation.active,
                              CreatedBy = usermaster.firstname + ' ' + usermaster.lastname

                          }).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        #endregion

        #region Insert
        public string InsertClientDetails(ClientDetails clientDetails, UserManagement user, Login login)
        {
            try
            {
                var existsEmail = _context.usermaster.Any(um => um.emailid.ToLower() == clientDetails.email_id.ToLower());
                var existsOrganizationName = _context.clientbasicinformation.Any(cbi => cbi.organization_name.ToLower() == clientDetails.organization_name.ToLower());
                if (!existsEmail)
                {
                    if (!existsOrganizationName)
                    {
                        foreach (var entry in _context.ChangeTracker.Entries())
                            entry.State = EntityState.Detached;

                        _context.clientbasicinformation.Add(clientDetails);
                        _context.SaveChanges();

                        user.clientid = clientDetails.clientid;
                        _context.usermaster.Add(user);
                        _context.SaveChanges();

                        login.userid = user.userid;
                        _context.userlogin.Add(login);
                        _context.SaveChanges();
                    }
                    else
                    {
                        if (existsEmail == true && existsOrganizationName == true)
                        {
                            return "EmailName";
                        }
                        else
                        {
                            return "OrganizationName";
                        }
                       
                    }

                } 
                else
                {
                    if(existsEmail == true && existsOrganizationName == true)
                    {
                        return "EmailName";
                    }
                    else
                    {
                        return "Email";
                    }
                    
                }
            }
            catch (Exception)
            {
                throw;
            }
            return "Success";
        }
        #endregion

        #region Update
        public string UpdateClientDetails(ClientDetails clientDetails, UserManagement user, Login login)
        {
            try
            {
                Boolean OrgResult;
                Boolean EmailResult;

                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;

                var  emailObj = (from clientbasicinformation in _context.clientbasicinformation
                             where clientbasicinformation.email_id.ToLower() == clientDetails.email_id.ToLower() && clientbasicinformation.clientid == clientDetails.clientid 
                             select new { clientId = clientbasicinformation.clientid, email = clientbasicinformation.email_id }).ToList();

                var existsEmail = emailObj.Any(obj => (obj.email.ToLower() == clientDetails.email_id.ToLower()));


                var userMasterEmailObj = (from usermaster in _context.usermaster
                                where usermaster.emailid.ToLower() == clientDetails.email_id.ToLower()
                                select new { userId = usermaster.userid, email = usermaster.emailid }).ToList();

                var userMasterExistsEmail = userMasterEmailObj.Any(obj => (obj.email.ToLower() == clientDetails.email_id.ToLower()));



                var existsOrganizationNameObj = (from clientbasicinformation in _context.clientbasicinformation
                             where clientbasicinformation.organization_name.ToLower() == clientDetails.organization_name.ToLower()
                             select new { clientId = clientbasicinformation.clientid, clienttName = clientbasicinformation.organization_name }).ToList();

                var existsOrganizationName = existsOrganizationNameObj.Any(obj => (obj.clienttName.ToLower() == clientDetails.organization_name.ToLower()) &&  (obj.clientId == clientDetails.clientid));

                var existsOrganizationNameCheck = existsOrganizationNameObj.Any(obj => (obj.clienttName.ToLower() == clientDetails.organization_name.ToLower()));


                if (!existsOrganizationName && existsOrganizationNameCheck)
                {
                    return "OrganizationName";
                }
                else
                {
                    OrgResult = true;
                }


                if (!userMasterExistsEmail)
                {
                    EmailResult = true;
                }
                else
                {
                    if (!existsEmail)
                    {
                        return "EmailId";
                    }
                    else
                    {
                        EmailResult = true;
                    }
                }

               




                if (OrgResult == true && EmailResult == true)
                {
                    _context.clientbasicinformation.Update(clientDetails);
                    _context.SaveChanges();

                    _context.usermaster.Update(user);
                    _context.SaveChanges();

                    _context.userlogin.Update(login);
                    _context.SaveChanges();
                    
                }

            }
            catch (Exception)
            {
                throw;
            }

            return "Success";
        }
        #endregion


        #region Delete
        public string DeleteClientDetails(ClientDetails objClient, UserManagement user, Login userlogin)
        {
            foreach (var entry in _context.ChangeTracker.Entries())
                entry.State = EntityState.Detached;
            try
            {

                var clientAdministrations = _context.adminbasicinformation.Where(m => m.clientid == objClient.clientid).ToList();
                foreach (var clientAdministration in clientAdministrations)
                {
                    _context.adminbasicinformation.Remove(clientAdministration);
                    _context.SaveChanges();
                }


                _context.clientbasicinformation.Remove(objClient);
                _context.SaveChanges();

                _context.userlogin.Remove(userlogin);
                _context.SaveChanges();

                _context.usermaster.Remove(user);
                _context.SaveChanges();


            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            return "Success";
        }
        #endregion
    }
}
