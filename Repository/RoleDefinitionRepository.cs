﻿using Dapper;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using Nancy.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Repository
{
    public class RoleDefinitionRepository: IRoleDefinitionRepository
    {
        private readonly DbWebApiContext _context;

        public RoleDefinitionRepository(DbWebApiContext context)
        {
            _context = context;
        }

        #region Get
        public IEnumerable<RoleDefinitionDTO> GetRoleDefinitionList()
        {
            List<RoleDefinitionDTO> result = null;
            try
            {
                result = (from rolemaster in _context.rolemaster
                          orderby rolemaster.roleid descending
                          join usermasters in _context.usermaster on rolemaster.insertedby equals usermasters.userid
                          select new RoleDefinitionDTO
                          {
                              RoleId = rolemaster.roleid,
                              Role = rolemaster.role,
                              RoleDescription = rolemaster.roledescription,
                              Active = rolemaster.active,
                              RecordStatus = rolemaster.recordstatus,
                              InsertedBy = rolemaster.insertedby,
                              CreatedBy = usermasters.firstname +' '+ usermasters.lastname,
                              InsertedDateTime = Convert.ToDateTime(rolemaster.insertedtime).ToString("MM-dd-yyyy"),

                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;

        }
        #endregion


        #region Get
        public IEnumerable<StageDTO> GetStageList()
        {
            List<StageDTO> result = null;
            try
            {

                result = (from stage_master in _context.stage_master
                          orderby stage_master.stageid descending
                          where stage_master.active.Equals(true)
                          select new StageDTO
                          {
                              StageId = stage_master.stageid,
                              StageName = stage_master.stagename,

                          }).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;

        }
        #endregion

        #region  Insert
        public string InsertRoleDefinition(List<RoleDefinitionDTO> roleDefinitionDTOs)
        {
            List<RoleDefinitionList> roleDefinitionList;
            string Success;
            try
            {
                var existRoleName = _context.rolemaster.Any(rm => (rm.role.ToLower() == roleDefinitionDTOs[0].Role.ToLower()));
                if (existRoleName)
                {
                    return "Failed";
                }

                roleDefinitionList = new List<RoleDefinitionList>();

                for (int i = 0; i <= roleDefinitionDTOs.Count - 1; i++)
                {
                    RoleDefinitionList roleDefinition = new RoleDefinitionList()
                    {
                        projecttypeid = roleDefinitionDTOs[i].ProjectTypeId,
                        userid = roleDefinitionDTOs[i].UserId,
                        stageid = roleDefinitionDTOs[i].StageId,
                        submoduleid = roleDefinitionDTOs[i].SubModuleId,
                        privilegeid = roleDefinitionDTOs[i].PrivilegeId,
                        flag = roleDefinitionDTOs[i].Flag,
                        role = roleDefinitionDTOs[i].Role,
                        active = roleDefinitionDTOs[i].Active,
                        roledescription = roleDefinitionDTOs[i].RoleDescription
                    };

                    roleDefinitionList.Add(roleDefinition);
                }

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var data = serializer.Serialize(roleDefinitionList);

                var connection = _context.Database.GetDbConnection();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@inputjson", data);

                var datas = connection.Query<dynamic>("usp_role_insert", parameter, commandType: System.Data.CommandType.StoredProcedure).ToList();


                if (datas[0].usp_role_insert == "true")
                {
                    Success = "Success";
                }
                else if (datas[0].usp_role_insert == "false")
                {
                    Success = "Failed";
                }
                else
                {
                    Success = datas[0].usp_role_insert;
                }


            }
             catch (Exception)
            {
                throw;
            }
            return Success;
        }
        #endregion

        //#region BulkUploadData
        //public dynamic BulkUpload(List<RoleDefinitionDTO> roleDefinitionDTOs)
        //{
        //    List<RoleDefinitionList> roleDefinitionList;
        //    string Success;
        //    object datas;
        //    try
        //    {

        //        roleDefinitionList = new List<RoleDefinitionList>();

        //        for (int i = 0; i <= roleDefinitionDTOs.Count - 1; i++)
        //        {
        //            RoleDefinitionList roleDefinition = new RoleDefinitionList()
        //            {
        //                projecttypeid = roleDefinitionDTOs[i].ProjectTypeId,
        //                userid = roleDefinitionDTOs[i].UserId,
        //                stageid = roleDefinitionDTOs[i].StageId,
        //                submoduleid = roleDefinitionDTOs[i].SubModuleId,
        //                privilegeid = roleDefinitionDTOs[i].PrivilegeId,
        //                flag = roleDefinitionDTOs[i].Flag,
        //                role = roleDefinitionDTOs[i].Role,
        //                active = roleDefinitionDTOs[i].Active,
        //                roledescription = roleDefinitionDTOs[i].RoleDescription
        //            };

        //            roleDefinitionList.Add(roleDefinition);
        //        }

                
        //        JavaScriptSerializer serializer = new JavaScriptSerializer();
        //        var data = serializer.Serialize(roleDefinitionList);

        //        var connection = _context.Database.GetDbConnection();

        //        DynamicParameters parameter = new DynamicParameters();
        //        parameter.Add("@inputjson", data);

        //         datas = connection.Query<dynamic>("usp_role_insert", parameter, commandType: System.Data.CommandType.StoredProcedure).ToList();

                
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    return datas;
        //}
        //#endregion

        #region Insert
        public int BulkInsertUserManagement(List<UserManagement> listofusers)
        {

            try
            {
                //var data = listofusers[0];
                for (int i = 0; i < listofusers.Count - 1; i++)
                {
                    var existEmail = _context.usermaster.Any(um => (um.emailid == listofusers[i].emailid) && (um.clientid == listofusers[i].clientid));
                    if (!existEmail)
                    {
                        var id = _context.usermaster.Add(listofusers[i]);
                        _context.SaveChanges();
                        foreach (var entry in _context.ChangeTracker.Entries())
                            entry.State = EntityState.Detached;
                    }
                    else
                    {
                        return 0;
                    }
                }

                //JavaScriptSerializer serializer = new JavaScriptSerializer();
                //var data = serializer.Serialize(listofusers);


                //var connection = _context.Database.GetDbConnection();

                //DynamicParameters parameter = new DynamicParameters();
                //parameter.Add("@inputjson", data);

                //var datas = connection.Query<dynamic>("usp_usermaster_validation", parameter, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception)
            {
                throw;
            }

            return 1;
        }

        #endregion


        #region Get By Id
        public IEnumerable<RoleDefinitionDTO> GetRoleDefinitionListById(int roleId)
        {
            List<RoleDefinitionDTO> result = null;

            try
            {
                result = (from rolemaster in _context.rolemaster
                          join role_mapping in _context.role_mapping on rolemaster.roleid equals role_mapping.roleid
                          join projecttype_mapping in _context.projecttype_mapping on role_mapping.projecttype_mapping_id equals projecttype_mapping.id
                          join projecttype_master in _context.projecttype_master on projecttype_mapping.projecttypeid equals projecttype_master.projecttypeid
                          join stage_master in _context.stage_master on projecttype_mapping.stageid equals stage_master.stageid
                          join module_submodule_master in _context.module_submodule_master on projecttype_mapping.submoduleid equals module_submodule_master.submoduleid
                          where role_mapping.roleid.Equals(roleId)
                          select new RoleDefinitionDTO
                          {
                              RoleId = rolemaster.roleid,
                              Role = rolemaster.role,
                              ProjectTypeId = projecttype_master.projecttypeid,
                              ProjectTypeName = projecttype_master.projecttype,
                              RoleDescription = rolemaster.roledescription,
                              UserId = projecttype_mapping.userid,
                              StageId = projecttype_mapping.stageid,
                              StageName = stage_master.stagename,
                              ModuleId = module_submodule_master.moduleid,
                              ModuleName = module_submodule_master.modulename,
                              SubModuleId = projecttype_mapping.submoduleid,
                              SubModuleName = module_submodule_master.submodulename,
                              PrivilegeId = role_mapping.privilegeid,
                              Active = rolemaster.active,
                              RecordStatus = rolemaster.recordstatus

                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        #endregion



        #region Get Project Wise Id
        public IEnumerable<RoleDefinitionDTO> GetProjectTypeWiseArray(int projectTypeId)
        {
            List<RoleDefinitionDTO> result = null;

            try
            {
                result = (from projecttype_master in _context.projecttype_master
                          join projecttype_mapping in _context.projecttype_mapping on projecttype_master.projecttypeid equals projecttype_mapping.projecttypeid
                          join stage_master in _context.stage_master on projecttype_mapping.stageid equals stage_master.stageid
                          join module_submodule_master in _context.module_submodule_master on projecttype_mapping.submoduleid equals module_submodule_master.submoduleid
                          where projecttype_mapping.projecttypeid.Equals(projectTypeId) && projecttype_master.active.Equals(true)
                          select new RoleDefinitionDTO
                          {
                              ProjectTypeId = projecttype_master.projecttypeid,
                              UserId = projecttype_mapping.userid,
                              StageId = projecttype_mapping.stageid,
                              StageName = stage_master.stagename,
                              ModuleId = module_submodule_master.moduleid,
                              ModuleName = module_submodule_master.modulename,
                              SubModuleId = projecttype_mapping.submoduleid,
                              SubModuleName = module_submodule_master.submodulename,
                              Active = projecttype_mapping.active,
                              RecordStatus = projecttype_mapping.recordstatus

                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        #endregion



        #region  Update
        public string UpdateRoleDefinition(List<RoleDefinitionDTO> roleDefinitionDTOs)
        {
            List<RoleDefinitionList> roleDefinitionList;
            string Success;
            try
            {
                var existRoleName = _context.rolemaster.Any(rm => (rm.role.ToLower() == roleDefinitionDTOs[0].Role.ToLower()) && rm.roleid != roleDefinitionDTOs[0].RoleId);
                if (existRoleName)
                {
                    return "Failed";
                }

                roleDefinitionList = new List<RoleDefinitionList>();

                for (int i = 0; i <= roleDefinitionDTOs.Count - 1; i++)
                {
                    RoleDefinitionList roleDefinition = new RoleDefinitionList()
                    {
                        projecttypeid = roleDefinitionDTOs[i].ProjectTypeId,
                        roleid = roleDefinitionDTOs[i].RoleId,
                        userid = roleDefinitionDTOs[i].UserId,
                        stageid = roleDefinitionDTOs[i].StageId,
                        submoduleid = roleDefinitionDTOs[i].SubModuleId,
                        privilegeid = roleDefinitionDTOs[i].PrivilegeId,
                        flag = roleDefinitionDTOs[i].Flag,
                        role = roleDefinitionDTOs[i].Role,
                        active = roleDefinitionDTOs[i].Active,
                        roledescription = roleDefinitionDTOs[i].RoleDescription
                    };

                    roleDefinitionList.Add(roleDefinition);
                }

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var data = serializer.Serialize(roleDefinitionList);

                var connection = _context.Database.GetDbConnection();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@inputjson", data);

                var datas = connection.Query<dynamic>("usp_role_insert", parameter, commandType: System.Data.CommandType.StoredProcedure).ToList();

                if (datas[0].usp_role_insert == "true")
                {
                    Success = "Success";
                }
                else if (datas[0].usp_role_insert == "false")
                {
                    Success = "Failed";
                }
                else
                {
                    Success = datas[0].usp_role_insert;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return Success;
        }
        #endregion

        #region delete
        public string DeleteRoleDefinition(int roleId)
        {

            string Success;
            try
            {
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;

                var roleMappings = _context.role_mapping.Where(m => m.roleid == roleId).ToList();
                foreach (var roleMapping in roleMappings)
                {
                    _context.role_mapping.Remove(roleMapping);
                    _context.SaveChanges();
                }

                var rolemaster = _context.rolemaster.FirstOrDefault(s => s.roleid == roleId);
                if (rolemaster != null)
                {
                    _context.rolemaster.Remove(rolemaster);
                    _context.SaveChanges();
                }

                Success = "Success";

            }
            catch (Exception)
            {
                throw;
            }

            return Success;
        }
        #endregion
    }
}
