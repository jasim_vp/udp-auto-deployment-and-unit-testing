﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using Nancy.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataPlatform.Repository
{
    public class ProjectUserRoleMappingRepository: IProjectUserRoleMappingRepository
    {
        private readonly DbWebApiContext _context;

        public ProjectUserRoleMappingRepository(DbWebApiContext context)
        {
            _context = context;
        }


        public object GetProjectUserRoleMapList(GetProjectRoleParams getProjectRoleParams)
        {

            try
            {
                var projectUserRoleMapList = (from project_user_role_mapping in _context.project_user_role_mapping
                              join usermaster in _context.usermaster on project_user_role_mapping.userid equals usermaster.userid
                              join rolemaster in _context.rolemaster on project_user_role_mapping.roleid equals rolemaster.roleid
                              where project_user_role_mapping.roleid == getProjectRoleParams.RoleId && project_user_role_mapping.projectid == getProjectRoleParams.ProjectId
                              select new
                              {

                                  id = project_user_role_mapping.id,
                                  userId = usermaster.userid,
                                  userName = usermaster.firstname + ' ' + usermaster.lastname,
                                  emailId = usermaster.emailid,
                                  roleId = project_user_role_mapping.roleid,
                                  createdBy = usermaster.firstname + ' ' + usermaster.lastname,
                                  createdOn =  project_user_role_mapping.insertedtime

                              }).ToList();

                var result = (from data in projectUserRoleMapList
                              group data by new { data.id, data.userId, data.userName, data.roleId, data.emailId, data.createdBy, data.createdOn } into newGroup
                            select new
                            {
                                id = newGroup.Key.id,
                                userId = newGroup.Key.userId,
                                userName = newGroup.Key.userName,
                                roleId = newGroup.Key.roleId,
                                emailId = newGroup.Key.emailId,
                                createdBy = newGroup.Key.createdBy,
                                createdOn = newGroup.Key.createdOn


                            }).ToList();

                return result;
             
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string InsertProjectRoleUserMapping(List<ProjectUserRoleMappingDTO> projectUserRoleMappingDTOs)
        {
            List<ProjectUserRoleMapping> listProjectUserRoleMapping;
            string Success;
            try
            {
                listProjectUserRoleMapping = new List<ProjectUserRoleMapping>();
                for (int i = 0; i <= projectUserRoleMappingDTOs.Count - 1; i++)
                {

                    var existsProjectUserRoleMap = _context.project_user_role_mapping.Any(purm => purm.projectid == projectUserRoleMappingDTOs[i].ProjectId  && purm.userid == projectUserRoleMappingDTOs[i].UserId && purm.roleid == projectUserRoleMappingDTOs[i].RoleId);

                    if (!existsProjectUserRoleMap)
                    {
                        ProjectUserRoleMapping projectUserRoles = new ProjectUserRoleMapping()
                        {
                            projectid = projectUserRoleMappingDTOs[i].ProjectId,
                            userid = projectUserRoleMappingDTOs[i].UserId,
                            roleid = projectUserRoleMappingDTOs[i].RoleId,
                            active = projectUserRoleMappingDTOs[i].Active,
                            recordstatus = projectUserRoleMappingDTOs[i].RecordStatus,
                            insertedby = projectUserRoleMappingDTOs[i].InsertedBy,
                            insertedtime = DateTime.Now,
                        };

                                listProjectUserRoleMapping.Add(projectUserRoles);

                        _context.project_user_role_mapping.Add(projectUserRoles);
                        _context.SaveChanges();
                    }
                    else
                    {
                        return Success = "Failed";
                    }


                    
                }

                Success = "Success";
            }
            catch (Exception ex)
            {
                throw;
            }
            return Success;
        }




        #region Delete Project User Role Map
        public string DeleteProjectUserRoleMap(ProjectUserRoleMapping objProjectUserRoleMap)
        {
            foreach (var entry in _context.ChangeTracker.Entries())
                entry.State = EntityState.Detached;
            try
            {
                _context.project_user_role_mapping.Remove(objProjectUserRoleMap);
                _context.SaveChanges();


            }
            catch (Exception ex)
            {
                throw;
            }

            return "Success";
        }
        #endregion


        #region Delete Multiple Project User Role Map
        public string DeleteMultipleProjectUserRoleMap(List<ProjectUserRoleMapping> objProjectUserRoleMap)
        {
            foreach (var entry in _context.ChangeTracker.Entries())
                entry.State = EntityState.Detached;
            try
            {
                for (int i = 0; i <= objProjectUserRoleMap.Count - 1; i++)
                {
                    _context.project_user_role_mapping.Remove(objProjectUserRoleMap[i]);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return "Success";
        }
        #endregion
    }
}
