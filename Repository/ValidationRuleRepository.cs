﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Repository
{
    public class ValidationRuleRepository : IValidationRuleRepository
    {
        private readonly DbWebApiContext _context;
        public ValidationRuleRepository(DbWebApiContext context)
        {
            _context = context;
        }

        #region Select All
        public IEnumerable<ValidationRuleDTO> GetValidationRuleDetails()
        {
            IEnumerable<ValidationRuleDTO> result;
            try
            {
                //Date today = Date.Today;
                DateTime dt = DateTime.Now;
                result = (from validationruleinformation in _context.validation_rule
                          orderby validationruleinformation.id descending
                          select new ValidationRuleDTO
                          {
                              Id = validationruleinformation.id,
                              Min_length = validationruleinformation.min_length,
                              Max_length = validationruleinformation.max_length,
                              Min_range = validationruleinformation.min_range,
                              Max_range = validationruleinformation.max_range,
                              From_date = validationruleinformation.from_date,
                              To_date = validationruleinformation.to_date
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        #endregion

        #region Insert
        public int InsertValidationRulrDetails(ValidationRule validationRule)
        {
            try
            {
                //var existsRecord = _context.validation_rule.Any(rule => rule. == menuList.menuname);
                _context.validation_rule.Add(validationRule);
                _context.SaveChanges();

                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;
            }
            catch (Exception)
            {
                throw;
            }
            return validationRule.id;
        }
        #endregion

        #region Update
        public string UpdateValidationRule(ValidationRule validationRule)
        {
            try
            {
                _context.validation_rule.Update(validationRule);
                _context.SaveChanges();

                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;
            }
            catch (Exception)
            {
                throw;
            }
            return "Success";
        }
        #endregion
    }
}
