﻿using Dapper;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.EntityFrameworkCore;
using Nancy.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Repository
{
    public class ProjectTypeRegulationRepository: IProjectTypeRegulationRepository
    {
        private readonly DbWebApiContext _context;

        public ProjectTypeRegulationRepository(DbWebApiContext context)
        {
            _context = context;
        }

        #region Get
        public IEnumerable<ProjectTypeRegulationDTO> GetProjectTypeRegulationList()
        {
            List<ProjectTypeRegulationDTO> result = null;
            try
            {
                result = (from projecttype_master in _context.projecttype_master
                          join usermasters in _context.usermaster on projecttype_master.insertedby equals usermasters.userid
                          join projecttype_mapping in _context.projecttype_mapping on projecttype_master.projecttypeid equals projecttype_mapping.projecttypeid
                          where projecttype_master.active == true
                          select new ProjectTypeRegulationDTO
                          {
                              ProjectTypeId = projecttype_master.projecttypeid,
                              ProjectType = projecttype_master.projecttype,
                              ProjectTypeDescription = projecttype_master.projecttype_description,
                              Active = projecttype_master.active,
                              RecordStatus = projecttype_master.recordstatus,
                              InsertedBy = projecttype_master.insertedby,
                              CreatedBy = usermasters.firstname + ' ' + usermasters.lastname,
                              InsertedDateTime = Convert.ToDateTime(projecttype_master.insertedtime).ToString("MM-dd-yyyy"),

                          }).Distinct().ToList();


            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        #endregion

        #region Insert
        public string InsertProjectType(List<ProjectTypeRegulationDTO> projectTypeRegulationDTOs)
        {
            List<ProjectTypeRegulationList> projectTypeRegulationList;
            string Success;
            try
            {

                projectTypeRegulationList = new List<ProjectTypeRegulationList>();

                for (int i = 0; i <= projectTypeRegulationDTOs.Count - 1; i++)
                {
                    ProjectTypeRegulationList projectTypeRegulation = new ProjectTypeRegulationList()
                    {
                        userid = projectTypeRegulationDTOs[i].UserId,
                        stageid = projectTypeRegulationDTOs[i].StageId,
                        submoduleid = projectTypeRegulationDTOs[i].SubModuleId,
                        flag = projectTypeRegulationDTOs[i].Flag,
                        projecttype = projectTypeRegulationDTOs[i].ProjectType,
                        active = projectTypeRegulationDTOs[i].Active,
                        projecttype_description = projectTypeRegulationDTOs[i].ProjectTypeDescription
                    };

                    projectTypeRegulationList.Add(projectTypeRegulation);
                }

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var data = serializer.Serialize(projectTypeRegulationList);

                var connection = _context.Database.GetDbConnection();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@inputjson", data);

                var datas = connection.Query<dynamic>("usp_projecttype_insert", parameter, commandType: System.Data.CommandType.StoredProcedure).ToList();

                if (datas[0].usp_projecttype_insert == true)
                {
                    Success = "Success";
                }
                else
                {
                    Success = "Failed";
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Success;
        }
        #endregion

        #region Get By Id
        public IEnumerable<ProjectTypeRegulationDTO> GetProjectTypeListById(int projectTypeId)
        {
            List<ProjectTypeRegulationDTO> result = null;

            try
            {
                result = (from projecttype_master in _context.projecttype_master
                          join projecttype_mapping in _context.projecttype_mapping on projecttype_master.projecttypeid equals projecttype_mapping.projecttypeid
                          join stage_master in _context.stage_master on projecttype_mapping.stageid equals stage_master.stageid
                          join module_submodule_master in _context.module_submodule_master on projecttype_mapping.submoduleid equals module_submodule_master.submoduleid
                          where projecttype_mapping.projecttypeid.Equals(projectTypeId)
                          select new ProjectTypeRegulationDTO
                          {
                              ProjectTypeId = projecttype_master.projecttypeid,
                              ProjectType = projecttype_master.projecttype,
                              ProjectTypeDescription = projecttype_master.projecttype_description,
                              UserId = projecttype_mapping.userid,
                              StageId = projecttype_mapping.stageid,
                              StageName = stage_master.stagename,
                              ModuleId = module_submodule_master.moduleid,
                              ModuleName = module_submodule_master.modulename,
                              SubModuleId = projecttype_mapping.submoduleid,
                              SubModuleName = module_submodule_master.submodulename,
                              Active = projecttype_master.active,
                              RecordStatus = projecttype_master.recordstatus

                          }).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        #endregion



        #region  Update
        public string UpdateProjectType(List<ProjectTypeRegulationDTO> projectTypeRegulationDTOs)
        {
            List<ProjectTypeRegulationList> projectTypeRegulationList;
            string Success;
            try
            {

                var existProjectTypeName = _context.projecttype_master.Any(ptm => (ptm.projecttype.ToLower() == projectTypeRegulationDTOs[0].ProjectType.ToLower()) && (ptm.projecttypeid != projectTypeRegulationDTOs[0].ProjectTypeId) && ptm.active == true);
                if (existProjectTypeName)
                {
                    return "Failed";
                }

                projectTypeRegulationList = new List<ProjectTypeRegulationList>();

                for (int i = 0; i <= projectTypeRegulationDTOs.Count - 1; i++)
                {
                    ProjectTypeRegulationList projectTypeRegulation = new ProjectTypeRegulationList()
                    {
                        projecttypeid = projectTypeRegulationDTOs[i].ProjectTypeId,
                        userid = projectTypeRegulationDTOs[i].UserId,
                        stageid = projectTypeRegulationDTOs[i].StageId,
                        submoduleid = projectTypeRegulationDTOs[i].SubModuleId,
                        flag = projectTypeRegulationDTOs[i].Flag,
                        projecttype = projectTypeRegulationDTOs[i].ProjectType,
                        active = projectTypeRegulationDTOs[i].Active,
                        projecttype_description = projectTypeRegulationDTOs[i].ProjectTypeDescription
                    };

                    projectTypeRegulationList.Add(projectTypeRegulation);
                }

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var data = serializer.Serialize(projectTypeRegulationList);

                var connection = _context.Database.GetDbConnection();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@inputjson", data);

                var datas = connection.Query<dynamic>("usp_projecttype_insert", parameter, commandType: System.Data.CommandType.StoredProcedure).ToList();

                if (datas[0].usp_projecttype_insert == true)
                {
                    Success = "Success";
                }
                else
                {
                    Success = "Failed";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Success;
        }
        #endregion


        #region Delete Project Type Regulation
        public string DeleteProjectTypeRegulation(int projectTypeId)
        {
            string Success;
            try
            {
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;

                var projectTypeMappings = _context.projecttype_mapping.Where(m => m.projecttypeid == projectTypeId).ToList();
                foreach (var projectTypeMapping in projectTypeMappings)
                {
                    _context.projecttype_mapping.Remove(projectTypeMapping);
                    _context.SaveChanges();
                }


                var projectMasters = _context.projectmaster.Where(m => m.projecttypeid == projectTypeId).ToList();
                foreach (var projectMaster in projectMasters)
                {
                    _context.projectmaster.Remove(projectMaster);
                    _context.SaveChanges();
                }

                var projectTypeMaster = _context.projecttype_master.FirstOrDefault(s => s.projecttypeid == projectTypeId);
                if (projectTypeMaster != null)
                {
                    _context.projecttype_master.Remove(projectTypeMaster);
                    _context.SaveChanges();
                }

                Success = "Success";

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            return Success;
        }
        #endregion





    }
}
