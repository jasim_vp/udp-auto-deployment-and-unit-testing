﻿using DataPlatform.DTO;
using DataPlatform.Model;
using DataPlatform.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nancy.Json;
using Microsoft.EntityFrameworkCore;
using Dapper;
using System.Data;
using System.Collections;

namespace DataPlatform.Repository
{
    public class ProjectConfigurationInputRepository : IProjectConfigurationInputRepository
    {
        private readonly DbWebApiContext _context;
        public ProjectConfigurationInputRepository(DbWebApiContext context)
        {
            _context = context;
        }


        #region Project Configuration Select All
        public IEnumerable<ProjectConfigurationDTO> GetProjectConfigurationDetails()
        {
            List<ProjectConfigurationDTO> result = null;
            try
            {
                result = (from project_config_credential_master in _context.project_config_credential_master
                          orderby project_config_credential_master.projectid descending

                          select new ProjectConfigurationDTO
                          {
                              Type = project_config_credential_master.type,
                              Configuration = project_config_credential_master.configuration,
                              HostAddress = project_config_credential_master.host_address,
                              UserName = project_config_credential_master.username,
                              Password = project_config_credential_master.password,
                              Port = project_config_credential_master.port,
                              Path = project_config_credential_master.path,
                              File = project_config_credential_master.file,
                              DatabaseName = project_config_credential_master.database_name,
                              TableName = project_config_credential_master.table_name,
                              CloudName = project_config_credential_master.cloud_name,
                              S3Url = project_config_credential_master.s3_url,
                              AccessKey = project_config_credential_master.access_key,
                              SecretKey = project_config_credential_master.secret_key,
                              Active = project_config_credential_master.active,

                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        #endregion


        #region GetBy
        public IEnumerable<ProjectConfigurationDTO> GetProjectConfigurationDetailsListById(int id)
        {
            List<ProjectConfigurationDTO> result = null;
            try
            {

                result = (from project_config_credential_master in _context.project_config_credential_master
                          where project_config_credential_master.projectid.Equals(id)
                          orderby project_config_credential_master.id descending

                          select new ProjectConfigurationDTO
                          {
                              Type = project_config_credential_master.type,
                              Configuration = project_config_credential_master.configuration,
                              HostAddress = project_config_credential_master.host_address,
                              UserName = project_config_credential_master.username,
                              Password = project_config_credential_master.password,
                              Port = project_config_credential_master.port,
                              Path = project_config_credential_master.path,
                              File = project_config_credential_master.file,
                              DatabaseName = project_config_credential_master.database_name,
                              TableName = project_config_credential_master.table_name,
                              CloudName = project_config_credential_master.cloud_name,
                              S3Url = project_config_credential_master.s3_url,
                              AccessKey = project_config_credential_master.access_key,
                              SecretKey = project_config_credential_master.secret_key,
                              Active = project_config_credential_master.active

                          }).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        #endregion


        #region Select All

        public string GetProjectConfigurationAttributeList(int flag)
        {            
            try
            {

                var connection = _context.Database.GetDbConnection();
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@flag", flag);
                var result = connection.Query<dynamic>("usp_project_configuration_attribute_data", parameter, commandType: CommandType.StoredProcedure).ToList();
                var attributes = result[0].usp_project_configuration_attribute_data;
                connection.Close();
                return attributes;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<GetAllAttributeData> GetProjectConfigurationAttributeList()
        {
            List<GetAllAttributeData> datas = null;
            try
            {

                datas = new List<GetAllAttributeData>();

                var connection = _context.Database.GetDbConnection();
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@flag", 0);
                var mappedDatas = connection.Query<dynamic>("usp_project_configuration_attribute_data", parameter, commandType: CommandType.StoredProcedure).ToList();
                var mappedAttributes = mappedDatas[0].usp_project_configuration_attribute_data;
                
                var connection2 = _context.Database.GetDbConnection();
                DynamicParameters parameter2 = new DynamicParameters();
                parameter2.Add("@flag", 1);
                var defaultDatas = connection2.Query<dynamic>("usp_project_configuration_attribute_data", parameter2, commandType: CommandType.StoredProcedure).ToList();
                var defaultAttributes = defaultDatas[0].usp_project_configuration_attribute_data;

                GetAllAttributeData addDatas = new GetAllAttributeData()
                {
                    MappedAttributes = mappedAttributes,
                    DefaultAttributes = defaultAttributes
                };

                datas.Add(addDatas);
                

            }
            catch (Exception)
            {
                throw;
            }

            return datas;
        }

        public string InsertProjectConfigurationInput(List<ProjectInputParams> projectInputParams)
        {
            List<GroupJson> groupJson;
            List<InputJsonAry> inputJson;
            string Success;
            try
            {
                    groupJson = new List<GroupJson>();

                for (int i = 0; i <= projectInputParams.Count - 1; i++)
                {
                    GroupJson groupJsonMapping = new GroupJson()
                    {
                        groupname = projectInputParams[i].GroupName,
                        display_group_subgroup_name = projectInputParams[i].DisplayGroupSubGroupName,
                        parent_subgroup_name = projectInputParams[i].ParentSubGroupName,
                        subgroupname = projectInputParams[i].SubGroupName,
                        attributedisplayorder = projectInputParams[i].AttributeDisplayOrder,
                        displaygrouporder = projectInputParams[i].DisplayGroupOrder,
                        attributename = projectInputParams[i].AttributeName,
                        attributetype = projectInputParams[i].AttributeType,
                        displayname = projectInputParams[i].DisplayName,
                        outputname = projectInputParams[i].OutputName,
                        outputflag = projectInputParams[i].OutputFlag,
                        attributevalue = projectInputParams[i].AttributeValue != null ? projectInputParams[i].AttributeValue[0] : null,
                        grouporder = projectInputParams[i].GroupOrder,
                        levelid = projectInputParams[i].levelid
                    };

                    groupJson.Add(groupJsonMapping);
                }

                var groupedResult = from s in groupJson
                                    group s by s.groupname;


                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var data = serializer.Serialize(groupedResult);

                var connection = _context.Database.GetDbConnection();

                inputJson = new List<InputJsonAry>();

                for (int i = 0; i <= projectInputParams[0].InputJson.Count - 1; i++)
                {
                    InputJsonAry inputJsonAry = new InputJsonAry()
                    {
                        projectid = projectInputParams[0].InputJson[i].ProjectId,
                        moduleid = projectInputParams[0].InputJson[i].ModuleId,
                        attributename = projectInputParams[0].InputJson[i].AttributeName,
                        attributetype = projectInputParams[0].InputJson[i].AttributeType,
                        displayname = projectInputParams[0].InputJson[i].DisplayName,
                        outputname = projectInputParams[0].InputJson[i].OutputName,
                        outputflag = projectInputParams[0].InputJson[i].OutputFlag,
                        attributevalue = projectInputParams[0].InputJson[i].AttributeValue != null ? projectInputParams[0].InputJson[i].AttributeValue[0] : null,
                    };

                    inputJson.Add(inputJsonAry);
                }


                var inputJsonForm = serializer.Serialize(inputJson);
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@inputjson", inputJsonForm);
                parameter.Add("@groupjson", data);

                var datas = connection.Query<dynamic>("usp_project_configuration_input_insert", parameter, commandType: CommandType.StoredProcedure).ToList();

                if (datas[0].usp_project_configuration_input_insert == "true")
                {
                    Success = "Success";
                }
                else if (datas[0].usp_project_insert == "false")
                {
                    Success = "Failed";
                }
                else
                {
                    Success = datas[0].usp_project_configuration_input_insert;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Success;
        }
        #endregion



        #region Update Project Configuration 
        public string UpdateProjectConfigurationAttr(List<UpdateAttr> updateAttrs)
        {
            List<UpdateJsonAry> updateJson;
            string Success;
            try
            {
                updateJson = new List<UpdateJsonAry>();

                for (int i = 0; i <= updateAttrs.Count - 1; i++)
                {
                    UpdateJsonAry updateJsonAry = new UpdateJsonAry()
                    {
                        attributeid = updateAttrs[i].AttributeId,
                        attributetype = updateAttrs[i].AttributeType,
                        attributevalue = updateAttrs[i].AttributeValue != null ? updateAttrs[i].AttributeValue[0] : null,
                        displayname = updateAttrs[i].DisplayName,
                        outputname = updateAttrs[i].OutputName,
                        outputflag = updateAttrs[i].OutputFlag,
                        attributedisplayorder = updateAttrs[i].AttributeDisplayOrder,
                        groupid = updateAttrs[i].GroupId,
                        displaygroupname = updateAttrs[i].DisplayGroupName,
                        grouporder = updateAttrs[i].DisplayGroupOrder,
                        displaygrouporder = updateAttrs[i].DisplayGroupOrder,
                        updatedby = updateAttrs[i].UpdatedBy
                    };

                    updateJson.Add(updateJsonAry);
                }


                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var data = serializer.Serialize(updateJson);

                var connection = _context.Database.GetDbConnection();
               
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@inputjson", data);

                var datas = connection.Query<dynamic>("usp_project_configuration_input_update", parameter, commandType: CommandType.StoredProcedure).ToList();

                if (datas[0].usp_project_configuration_input_update == "true")
                {
                    Success = "Success";
                }
                else if (datas[0].usp_project_configuration_input_update == "false")
                {
                    Success = "Failed";
                }
                else
                {
                    Success = datas[0].usp_project_configuration_input_insert;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Success;
        }
         #endregion

        #region Insert
        public string InsertProjectConfigurationDetails(ProjectConfiguration projectConfiguration)
        {
            List<ConfigDetails> existsRecord = null;
            try
            {
                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;

                existsRecord = (from project_config_credential_master in _context.project_config_credential_master
                                where project_config_credential_master.projectid == projectConfiguration.projectid && project_config_credential_master.type == projectConfiguration.type && project_config_credential_master.configuration == projectConfiguration.configuration
                          select new ConfigDetails { id = project_config_credential_master.id, projectid = project_config_credential_master.projectid, type = project_config_credential_master.type, configuration = project_config_credential_master.configuration }).ToList();

                var existId = _context.project_config_credential_master.Where(pccm => pccm.type == projectConfiguration.type && pccm.configuration == projectConfiguration.configuration && pccm.projectid == projectConfiguration.projectid).Any();
                if (existId == false && existsRecord.Count == 0)
                {

                    var id = _context.project_config_credential_master.Add(projectConfiguration);
                    _context.SaveChanges();
                   
                }
                else
                {
                    projectConfiguration.id = existsRecord[0].id;
                    var id = _context.project_config_credential_master.Update(projectConfiguration);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return "Success";
        }

        #endregion

        #region DelAttr
        public Boolean DelAttr(DelAttr delAttr)
        {
            List<ProjectAttributeMofuleMapping> result = null;
            try
            {

                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;

                result =  (from pamm in _context.projectattributemodule_mapping
                          where pamm.attributeid == delAttr.AttributeId && pamm.projectid == delAttr.ProjectId && pamm.moduleid == delAttr.ModuleId
                          select new ProjectAttributeMofuleMapping
                          {
                              mapid = pamm.mapid,
                              attributeid = pamm.attributeid,
                              projectid = pamm.projectid,
                              moduleid = pamm.moduleid,
                              attributedisplayorder = pamm.attributedisplayorder,
                              displayname = pamm.displayname,
                              attributetype = pamm.attributetype,
                              outputname = pamm.outputname,
                              outputflag = pamm.outputflag

                 }).ToList();

                if(result.Count() == 1)
                {
                    ProjectAttributeMofuleMapping projectAttrModuleMapp = new ProjectAttributeMofuleMapping
                    {
                        mapid = result[0].mapid,
                        groupmapid = null,
                        attributeid = result[0].attributeid,
                        projectid = result[0].projectid,
                        moduleid = result[0].moduleid,
                        attributedisplayorder = result[0].attributedisplayorder,
                        displayname = result[0].displayname,
                        active = true,
                        updatedby = delAttr.UpdatedBy,
                        updatedtime = Convert.ToDateTime(DateTime.Now),
                        attributetype = result[0].attributetype,
                        outputname = result[0].outputname,
                        outputflag = result[0].outputflag
                    };

                    _context.projectattributemodule_mapping.Update(projectAttrModuleMapp);
                    _context.SaveChanges();

                    return true;
                }

            }
            catch (Exception)
            {
                throw;            
            }

            return false;
        }
        #endregion

        #region DelAttr
        public Boolean DeleteGroup(List<DelGroup> delGroup)
        {
            List<ProjectAttributeMofuleMapping> result = null;
            List<ProjectGroupMapping> delGroupMapping = null;
            try
            {

                foreach (var entry in _context.ChangeTracker.Entries())
                    entry.State = EntityState.Detached;



                for (int i = 0; i <= delGroup.Count - 1; i++)
                {
                    if (delGroup[i].AttributeList.Count() > 0)
                    {
                        for (int j = 0; j <= delGroup[i].AttributeList.Count - 1; j++)
                        {
                            result = (from pamm in _context.projectattributemodule_mapping
                                      where pamm.attributeid == delGroup[i].AttributeList[j].AttributeId && pamm.projectid == delGroup[i].ProjectId && pamm.moduleid == delGroup[i].ModuleId
                                      select new ProjectAttributeMofuleMapping
                                      {
                                          mapid = pamm.mapid,
                                          attributeid = pamm.attributeid,
                                          projectid = pamm.projectid,
                                          moduleid = pamm.moduleid,
                                          displayname = pamm.displayname,
                                          attributetype = pamm.attributetype,
                                          outputname = pamm.outputname,
                                          outputflag = pamm.outputflag

                                      }).ToList();

                            if (result.Count() == 1)
                            {
                                ProjectAttributeMofuleMapping projectAttrModuleMapp = new ProjectAttributeMofuleMapping
                                {
                                    mapid = result[0].mapid,
                                    groupmapid = null,
                                    attributeid = result[0].attributeid,
                                    projectid = result[0].projectid,
                                    moduleid = result[0].moduleid,
                                    attributedisplayorder = result[0].attributedisplayorder,
                                    displayname = result[0].displayname,
                                    active = true,
                                    updatedby = delGroup[i].UpdatedBy,
                                    updatedtime = Convert.ToDateTime(DateTime.Now),
                                    attributetype = result[0].attributetype,
                                    outputname = result[0].outputname,
                                    outputflag = result[0].outputflag
                                };

                                _context.projectattributemodule_mapping.Update(projectAttrModuleMapp);
                                _context.SaveChanges();
                            }

                        }
                    }

                    delGroupMapping = (from pgm in _context.projectgroup_mapping
                                       where pgm.groupid == delGroup[i].GroupId && pgm.projectid == delGroup[i].ProjectId
                                       select new ProjectGroupMapping
                                       {
                                           groupmapid = pgm.groupmapid,
                                           groupid = pgm.groupid,
                                           projectid = pgm.projectid,
                                           displaygroupname = pgm.displaygroupname,
                                           grouporder = pgm.grouporder,
                                           displaygrouporder = pgm.displaygrouporder

                                       }).ToList();

                    if (delGroupMapping.Count() == 1)
                    {
                        _context.projectgroup_mapping.Remove(delGroupMapping[0]);
                        _context.SaveChanges();
                    }
                }

                return true;

            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        #endregion
    }
}
