﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Repository
{
    public class LoginRepository : ILoginRepository
    {

        private readonly DbWebApiContext _context;

        public LoginRepository(DbWebApiContext context)
        {
            _context = context;
        }

        #region Auth Check


        public IEnumerable<AuthCheck> VerifyLogin(LoginDTO loginDTO)
        {
            List<AuthCheck> result = null;
            try
            {

                 result = (from userlogin in _context.userlogin
                             where userlogin.username == loginDTO.UserName && userlogin.password == loginDTO.Password
                             join usermasters in _context.usermaster on userlogin.userid equals usermasters.userid
                             select new AuthCheck{ userId = userlogin.userid, userName = usermasters.firstname + ' ' + usermasters.lastname, roleId = userlogin.roleid, email = usermasters.emailid, clientId = usermasters.clientid }).ToList();

              
              return result;

            }
            catch (Exception)
            {
                throw;
            }

        }

    }

        #endregion
    }

    
