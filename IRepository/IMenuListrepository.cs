﻿using DataPlatform.DTO;
using DataPlatform.Model;
using System.Collections.Generic;

namespace DataPlatform.IRepository
{
    public interface IMenuListrepository
    {
        IEnumerable<MenuListDTO> GetMenuList();

        int InsertMenuList(MenuList menuList);

        IEnumerable<MenuListDTO> GetMenuListById(int menuId);

        string UpdateMenuList(MenuList menuList);
    }
}
