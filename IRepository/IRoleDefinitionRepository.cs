﻿using DataPlatform.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface IRoleDefinitionRepository
    {
        IEnumerable<RoleDefinitionDTO> GetRoleDefinitionList();

        IEnumerable<StageDTO> GetStageList();

        //dynamic BulkUpload(List<RoleDefinitionDTO> roleDefinitionDTOs);

        string InsertRoleDefinition(List<RoleDefinitionDTO> roleDefinitionDTOs);

        IEnumerable<RoleDefinitionDTO> GetRoleDefinitionListById(int id);

        string UpdateRoleDefinition(List<RoleDefinitionDTO> roleDefinitionDTOs);

        IEnumerable<RoleDefinitionDTO> GetProjectTypeWiseArray(int id);

        string DeleteRoleDefinition(int roleId);
    }
}
