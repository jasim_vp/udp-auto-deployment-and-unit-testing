﻿using DataPlatform.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface IServicesCatalogRepository
    {
        List<Stage> GetServiceCatalogDetails();

        IEnumerable<ModuleSubmoduleMasterDto> GetServiceCatalogDetailsById(int id);

        IEnumerable<ModuleSubmoduleMasterDto> GetPrivilegeServiceCatalogDetailsById(int id);

        string InsertServiceCatalogDetails(List<ServiceCatalogDTO> serviceCatalogDTOs);

        string UpdateServiceCatalogDetails(UpdateServicecatalogdetails updateServicecatalogdetails);
    }
}
