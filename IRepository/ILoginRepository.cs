﻿using DataPlatform.DTO;
using DataPlatform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface ILoginRepository
    {
        IEnumerable<AuthCheck> VerifyLogin(LoginDTO loginDTO);
    }

}
