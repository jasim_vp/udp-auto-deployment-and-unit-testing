﻿using DataPlatform.Controllers;
using DataPlatform.DTO;
using DataPlatform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface IAdminUserBoardRepository
    {
        IEnumerable<AdminUserBoardDTO> GetAdminUserBoardList();

        IEnumerable<AdminUserBoardDTO> GetAdminUserBoardListById(int clientId);

        int InsertAdminUserBoard(AdminUserBoard adminUserBoard, UserManagement user, Login userlogin);

        string UpdateAdminUserBoard(AdminUserBoard adminUserBoard, UserManagement user, Login userlogin);

        string DeleteAdminUserBoard(AdminUserBoard adminUserBoard, UserManagement user, Login userlogin);
    }
}
