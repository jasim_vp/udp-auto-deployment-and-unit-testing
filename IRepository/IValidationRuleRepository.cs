﻿using DataPlatform.DTO;
using DataPlatform.Model;
using System.Collections.Generic;

namespace DataPlatform.IRepository
{
    public interface IValidationRuleRepository
    {
        IEnumerable<ValidationRuleDTO> GetValidationRuleDetails();

        int InsertValidationRulrDetails(ValidationRule validationRule);

        public string UpdateValidationRule(ValidationRule validationRule);
    }
}
