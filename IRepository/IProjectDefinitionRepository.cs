﻿using DataPlatform.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface IProjectDefinitionRepository
    {
        IEnumerable<ProjectDefinitionDTO> GetProjectList();

        IEnumerable<ProjectStageList> GetProjectStageList(int projectTypeId);

        string InsertProjectDefinition(List<ProjectMappingParams> projectMappingParams);

        IEnumerable<ProjectDefinitionDTO> GetProjectDefinitionListById(int projectId);

        string UpdateProjectDefinition(List<ProjectMappingParams> projectMappingParams);

        string DeleteProjectDefinition(int projectId);
    }
}
