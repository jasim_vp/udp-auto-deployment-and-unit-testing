﻿using DataPlatform.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface IStageRepository
    {
        IEnumerable<StageDTO> GetStageList();
    }
}
