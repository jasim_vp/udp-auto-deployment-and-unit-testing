﻿using DataPlatform.DTO;
using DataPlatform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface IUserGroupRepository
    {
        IEnumerable<UserGroupList> GetUserGroupList();
        
        string DeleteUserGroup(UserGroup objUserGroup);

        IEnumerable<UserRoleList> GetProjectUserRoleList(int projectId);

        string InsertUserGroup(UserGroup objUserGroup, List<UserGroupMapping> userGroupMappings);

        IEnumerable<UserGroupDTO> GetEditUserList(int userGroupId);

        string UpdateUserGroup(UserGroup objUserGroup, UserGroup objUpdateUserGroup, List<UserGroupMapping> userGroupMappings);
    }
}
