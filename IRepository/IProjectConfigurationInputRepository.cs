﻿using DataPlatform.DTO;
using DataPlatform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface IProjectConfigurationInputRepository
    {
        string GetProjectConfigurationAttributeList(int flag);

        IEnumerable<GetAllAttributeData> GetProjectConfigurationAttributeList();

        string InsertProjectConfigurationInput(List<ProjectInputParams> projectInputParams);

        IEnumerable<ProjectConfigurationDTO> GetProjectConfigurationDetails();

        IEnumerable<ProjectConfigurationDTO> GetProjectConfigurationDetailsListById(int id);

        string InsertProjectConfigurationDetails(ProjectConfiguration projectConfiguration);

        string UpdateProjectConfigurationAttr(List<UpdateAttr> updateAttrs);

        Boolean DelAttr(DelAttr delAttr);

        Boolean DeleteGroup(List<DelGroup> delGroup);
    }
}
