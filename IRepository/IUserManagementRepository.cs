﻿using DataPlatform.DTO;
using DataPlatform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface IUserManagementRepository
    {
        IEnumerable<UserManagementDTO> GetUserManagementList();

        IEnumerable<UserManagementDTO> GetUserManagementListById(int userId);


        dynamic BulkUpload(List<UserManagementDTO> userManagementDTOs);

        int InsertUserManagement(UserManagement objUserManagment);

        string UpdateUserManagement(UserManagement objUserManagment);

        string DeleteUserManagement(UserManagement objUserManagement, Login login);
    }
}
