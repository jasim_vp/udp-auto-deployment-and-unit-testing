﻿using DataPlatform.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface IProjectTypeRegulationRepository
    {
        IEnumerable<ProjectTypeRegulationDTO> GetProjectTypeRegulationList();

        string InsertProjectType(List<ProjectTypeRegulationDTO> projectTypeRegulationDTOs);

        IEnumerable<ProjectTypeRegulationDTO> GetProjectTypeListById(int id);

        string UpdateProjectType(List<ProjectTypeRegulationDTO> projectTypeRegulationDTOs);

        string DeleteProjectTypeRegulation(int projectTypeId);
    }
}
