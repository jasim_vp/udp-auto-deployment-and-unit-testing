﻿using DataPlatform.DTO;
using DataPlatform.Model;
using System.Collections.Generic;

namespace DataPlatform.IRepository
{
    public interface IClientDetailsRepository
    {
        IEnumerable<ClientDetailsDTO> GetClientDetails();

        IEnumerable<ClientDetailsDTO> GetClientDetailsListById(int clientId);

        string InsertClientDetails(ClientDetails client, UserManagement user, Login userlogin);

        string UpdateClientDetails(ClientDetails clientDetails, UserManagement user, Login userlogin);

        string DeleteClientDetails(ClientDetails objClient, UserManagement user, Login userlogin);
    }
}
