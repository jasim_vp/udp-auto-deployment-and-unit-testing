﻿using DataPlatform.DTO;
using DataPlatform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.IRepository
{
    public interface IProjectUserRoleMappingRepository
    {
        object GetProjectUserRoleMapList(GetProjectRoleParams getProjectRoleParams);

        string InsertProjectRoleUserMapping(List<ProjectUserRoleMappingDTO> projectUserRoleMappingDTOs);

        string DeleteProjectUserRoleMap(ProjectUserRoleMapping objProjectUserRoleMap);

        string DeleteMultipleProjectUserRoleMap(List<ProjectUserRoleMapping> objProjectUserRoleMap);
    }
}
