﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class ProjectUserRoleMappingDTO
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int UserId { get; set; }

        public int RoleId { get; set; }

        public Boolean Active { get; set; }

        public int RecordStatus { get; set; }

        public int InsertedBy { get; set; }

        public int UpdatedBy { get; set; }

        public string CreatedBy { get; set; }

        public DateTime InsertedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public string InsertedDateTime { get; set; }
    }

    public class GetProjectRoleParams
    {
        public int ProjectId { get; set; }

        public int RoleId { get; set; }

        public int UserId { get; set; }

        public int ProjectUserRoleMapId { get; set; }
    }


}
