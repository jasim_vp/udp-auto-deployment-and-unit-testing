﻿using System;
using System.Collections.Generic;

namespace DataPlatform.DTO
{

    public class ProjectConfigurationDTO
    {
        public int ProjectId { get; set; }
        public string Configuration { get; set; }
        public string Type { get; set; }
        public string HostAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Port { get; set; }
        public string Path { get; set; }
        public string File { get; set; }
        public string DatabaseName { get; set; }
        public string TableName { get; set; }
        public string CloudName { get; set; }
        public string S3Url { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public bool Active { get; set; }
    }


    public class ProjectConfigurationInputDTO
    {
        public int AttributeId { get; set; }

        public string AttributeName { get; set; }

        public Boolean Active { get; set; }

        public int RecordStatus { get; set; }

        public int InsertedBy { get; set; }

        public int UpdatedBy { get; set; }

        public string InsertedDateTime { get; set; }

        public DateTime InsertedTime { get; set; }

        public DateTime UpdatedTime { get; set; }
    }


    public class ProjectConfigParams
    { 
        public List<ProjectInputParams> ProjectInputParams { get; set; }
    }


    public class ProjectInputParams
    {
        public List<InputJson> InputJson { get; set; }

        public string GroupName { get; set; }

        public string DisplayGroupSubGroupName { get; set; }

        public string ParentSubGroupName { get; set; }

        public string SubGroupName { get; set; }

        public int AttributeDisplayOrder { get; set; }

        public int DisplayGroupOrder { get; set; }

        public string AttributeName { get; set; }

        public string AttributeType { get; set; }

        public string OutputName { get; set; }

        public Boolean OutputFlag { get; set; }

        public List<AttributeValue> AttributeValue { get; set; }

        public string DisplayName { get; set; }

        public int levelid { get; set; }

        public int GroupOrder { get; set; }

    }

    public class InputJson
    {
        public int ProjectId { get; set; }

        public int ModuleId { get; set; }

        public string AttributeName { get; set; }

        public string AttributeType { get; set; }

        public List<AttributeValue> AttributeValue { get; set; }

        public string DisplayName { get; set; }

        public string OutputName { get; set; }

        public Boolean OutputFlag { get; set; }
    }


    public class UpdateAttr
    {
        public int AttributeId { get; set; }

        public string AttributeType { get; set; }

        public List<AttributeValue> AttributeValue { get; set; }

        public string DisplayName { get; set; }

        public string OutputName { get; set; }

        public Boolean OutputFlag { get; set; }

        public int AttributeDisplayOrder { get; set; }

        public int GroupId { get; set; }

        public string DisplayGroupName { get; set; }

        public int GroupOrder { get; set; }

        public int DisplayGroupOrder { get; set; }

        public int UpdatedBy { get; set; }
    }


    public class GetAllAttributeData
    {
        public string MappedAttributes { get; set; }

        public string DefaultAttributes { get; set; }

    }


    public class DelAttr
    {
        public int ProjectId { get; set; }

        public int ModuleId { get; set; }

        public int AttributeId { get; set; }

        public int UpdatedBy { get; set; }

    }


    public class DelGroup
    {
        public int GroupId { get; set; }

        public int ProjectId { get; set; }

        public int ModuleId { get; set; }

        public List<AttributeList> AttributeList { get; set; }

        public int UpdatedBy { get; set; }

    }

    public class AttributeList
    {
        public int AttributeId { get; set; }

        public string AttributeName { get; set; }

    }


    public class AttributeValue
    {
        public string value { get; set; }
         
        public int min { get; set; }

        public int max { get; set; }

        public string format { get; set; }

        public string range { get; set; }


    }


    
}
