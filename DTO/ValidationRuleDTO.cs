﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class ValidationRuleDTO
    {
        public int Id { get; set; }

        public int Min_length { get; set; }

        public int Max_length { get; set; }

        public int Min_range { get; set; }

        public int Max_range { get; set; }

        public DateTime From_date { get; set; }

        public DateTime To_date { get; set; }
    }
}
