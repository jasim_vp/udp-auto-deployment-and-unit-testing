﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class ServiceCatalogDTO
    {
        public int StageID { get; set; }

        public string StageName { get; set; }

        public int ModuleId { get; set; }

        public string ModuleName { get; set; }

        public int SubModuleId { get; set; }

        public string SubModuleName { get; set; }

        public int PrivilegeId { get; set; }

        public string Privilege_Name { get; set; }

        public int ProcessId { get; set; }
    }

    public class UpdateServicecatalogdetails
    {
        public int Id { get; set; }

        public int ProcessId { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

    }
    public class Privilege
    {
        public string Privilege_Name { get; set; }
        public int PrivilegeId { get; set; }
    }

    public class Submodule
    {
        public int SubModuleId { get; set; }

        public string SubModuleName { get; set; }   
        
        public List<Privilege> Privileges { get; set; }

    }

    public class Module {
        public int ModuleId { get; set; }

        public string ModuleName { get; set; }

        public List<Submodule> Submodules { get; set; }
    }

    public class Stage {
        public int StageID { get; set; }

        public string StageName { get; set; }

        public List<Module> Modules { get; set; }
    }

    public class ModuleSubmoduleMasterDto {
        public int ModuleId { get; set; }

        public string ModuleName { get; set; }

        public int SubModuleId { get; set; }

        public string SubModuleName { get; set; }

        public string PrivilegeName { get; set; }

        public int PrivilegeId { get; set; }

        public bool Active { get; set; }
    }

}
