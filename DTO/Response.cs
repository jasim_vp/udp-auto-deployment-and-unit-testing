﻿namespace DataPlatform.DTO
{
    public class Response
    {
        public string Message { get; set; }

        public int status { get; set; }

        public string Error { get; set; }
    }
}
