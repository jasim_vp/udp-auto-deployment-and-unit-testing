﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class UserManagementDTO
    {

        public int UserId { get; set; }

        public int UserLoginId { get; set; }

        public int ClientId { get; set; }

        public int RoleId { get; set; }

        public int AdminId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailId { get; set; }

        public string Phone { get; set; }

        public string WorkPhone { get; set; }

        public int RecordStatus { get; set; }

        public int InsertedBy { get; set; }

        public int UpdatedBy { get; set; }

        public string InsertedDateTime { get; set; }

        public DateTime InsertedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public Boolean Active { get; set; }
    }

    public class ErrorUserManagementDTO
    {
        public int ClientId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailId { get; set; }

        public string Phone { get; set; }

        public string WorkPhone { get; set; }

        public int RecordStatus { get; set; }

        public int InsertedBy { get; set; }

        public string InsertedDateTime { get; set; }

        public Boolean Active { get; set; }
    }
}


