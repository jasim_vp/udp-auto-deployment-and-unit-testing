﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class StageDTO
    {
        public int StageId { get; set; }

        public string StageName { get; set; }

        public string Description { get; set; }

        public string Actions { get; set; }

        public Boolean Active { get; set; }

        public int RecordStatus { get; set; }

        public int InsertedBy { get; set; }

        public int UpdatedBy { get; set; }

        public string InsertedDateTime { get; set; }

        public DateTime InsertedTime { get; set; }

        public DateTime UpdatedTime { get; set; }
    }
}
