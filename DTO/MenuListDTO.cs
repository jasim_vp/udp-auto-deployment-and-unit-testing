﻿using System;

namespace DataPlatform.DTO
{
    public class MenuListDTO
    {
        public int MenuId { get; set; }

        public string Menuname { get; set; }

        public string SubMenuName { get; set; }

        public int InsertedBy { get; set; }

        public string CreatedBy { get; set; }

        public int UpdatedBy { get; set; }

        public string InsertedTime { get; set; }

        public DateTime UpdatedDateTime { get; set; }

        public DateTime InsertedDateTime { get; set; }

        public bool Status { get; set; }
        public bool Active { get; set; }
    }
}
