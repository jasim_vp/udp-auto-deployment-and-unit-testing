﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class Status
    {
        public enum OptionalField
        {
            Success = 1,
            Failure = 2,
            DBFailure = 3
}
    }
}
