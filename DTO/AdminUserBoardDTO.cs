﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class AdminUserBoardDTO
    {
        public int AdminId { get; set; }

        public int ClientId { get; set; }

        public int UserId { get; set; }

        public int UserLoginId { get; set; }

        public string OrganizationName { get; set; }

        public string AdminName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Password { get; set; }

        public string PointOfContact { get; set; }

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        public string EmailId { get; set; }

        public Boolean Active { get; set; }

        public int RecordStatus { get; set; }

        public int InsertedBy { get; set; }

        public int UpdatedBy { get; set; }

        public string InsertedDateTime { get; set; }

        public DateTime InsertedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public string CreatedBy { get;  set; }
    }
}
