﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class UserGroupDTO
    {
        public int UserGroupId { get; set; }

        public int UserId { get; set; }

        public int ProjectId { get; set; }

        public string UserName { get; set; }

        public string UserGroupName { get; set; }

        public Boolean Active { get; set; }

        public int RecordStatus { get; set; }

        public int InsertedBy { get; set; }

        public string CreatedBy { get; set; }

        public int UpdatedBy { get; set; }

        public string InsertedDateTime { get; set; }

        public string GroupMailId { get; set; }

        public string ProjectName { get; set; }

        public int NoOfUser { get; set; }

        public DateTime InsertedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

    }


    public class UserGroupList
    {
        public int UserGroupMapId { get; set; }

        public int UserGroupId { get; set; }

        public int UserId { get; set; }

        public int RoleId { get; set; }

        public string UserName { get; set; }

        public string RoleName { get; set; }

        public int NoOfUsers { get; set; }

        public string UserGroupName { get; set; }

        public string Project { get; set; }

        public string UserGroupMail { get; set; }

        public Boolean Active { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedOn { get; set; }
    }


    public class UserRoleList
    {
        public int UserId { get; set; }

        public int RoleId { get; set; }

        public string UserName { get; set; }

        public string RoleName { get; set; }

    }


    public class InsertUserGroup
    {
        public int UserId { get; set; }

        public int UserGroupId { get; set; }

        public int ProjectId { get; set; }

        public int RoleId { get; set; }

        public string UserGroupName { get; set; }

        public Boolean Active { get; set; }

        public string GroupMailId { get; set; }

        public string ProjectName { get; set; }

        public int NoOfUser { get; set; }

        public int InsertedBy { get; set; }

        public int RecordStatus { get; set; }
    }


}
