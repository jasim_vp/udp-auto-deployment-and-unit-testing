﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class RoleDefinitionDTO
    {
        public int RoleId { get; set; }

        public int ProjectTypeId { get; set; }

        public string ProjectTypeName { get; set; }

        public int UserId { get; set; }

        public int StageId { get; set; }

        public string StageName { get; set; }

        public int ModuleId { get; set; }

        public string ModuleName { get; set; }

        public int SubModuleId { get; set; }

        public string SubModuleName { get; set; }

        public int PrivilegeId { get; set; }

        public int Flag { get; set; }

        public string Role { get; set; }

        public string RoleDescription { get; set; }

        public Boolean Active { get; set; }

        public int RecordStatus { get; set; }

        public int InsertedBy { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime InsertedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public string CreatedBy { get; set; }

        public string InsertedDateTime { get; set; }
    }
}
