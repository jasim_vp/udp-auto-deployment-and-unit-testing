﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class LoginDTO
    {

        public int UserLoginId { get; set; }

        public int UserId { get; set; }

        public int RoleId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public DateTime ExpiryDate { get; set; }

        public int ExpirationFlag { get; set; }

        public string ResetKey { get; set; }

        public int PasswordReset { get; set; }

        public DateTime ResetTime { get; set; }

        public Boolean Active { get; set; }

        public int RecordStatus { get; set; }

        public int InsertedBy { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime InsertedTime { get; set; }

        public DateTime Updatedtime { get; set; }
    }
}
