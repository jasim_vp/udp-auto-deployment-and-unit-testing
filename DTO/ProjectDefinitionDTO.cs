﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.DTO
{
    public class ProjectDefinitionDTO
    {
        public int ProjectId { get; set; }

        public string ProjectName { get; set; }

        public int ProjectTypeId { get; set; }

        public int UserId { get; set; }

        public int StageId { get; set; }

        public string StageName { get; set; }

        public int ModuleId { get; set; }

        public string ModuleName { get; set; }

        public int SubModuleId { get; set; }

        public string SubModuleName { get; set; }

        public string ProjectTypeName { get; set; }

        public string ProjectDescription { get; set; }

        public Boolean Active { get; set; }

        public int RecordStatus { get; set; }

        public int InsertedBy { get; set; }

        public int UpdatedBy { get; set; }

        public double DisplayOrder { get; set; }

        public DateTime InsertedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public string InsertedDateTime { get; set; }

        public string CreatedBy { get; set; }

        public string ProjectGeneralQA { get; set; }
    }

    public class ProjectStageList
    {
        public int ProjectTypeMapId { get; set; }

        public int ProjectTypeid { get; set; }
       
        public string ProjectTypeName { get; set; }

        public int StageId { get; set; }

        public string StageName { get; set; }

        public int ModuleId { get; set; }

        public string ModuleName { get; set; }

        public int SubModuleId { get; set; }
        
        public string SubModuleName { get; set; }

        public string GeneralQuestions { get; set; }

    }

   


    public class ProjectQuestionParams
    {
        public int Id { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }
    }


    public class ProjectMappingParams
    {
        public int ProjectId { get; set; }

        public string ProjectName { get; set; }

        public string ProjectDescription { get; set; }

        public int ProjectTypeId { get; set; }

        public int UserId { get; set; }

        public int StageId { get; set; }

        public int SubModuleId { get; set; }

        public int DisplayOrder { get; set; }

        public string Questions { get; set; }

        public Boolean Active { get; set; }

        public int Flag { get; set; }

    }
}
