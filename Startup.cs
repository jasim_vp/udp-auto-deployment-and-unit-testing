using Autofac;
using Autofac.Extensions.DependencyInjection;
using DataPlatform.IRepository;
using DataPlatform.Model;
using DataPlatform.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace DataPlatform
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public ILifetimeScope ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DbWebApiContext>(ServiceLifetime.Scoped);
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Employee API", Version = "V1" });
            });

            //services.AddCors(c =>
            //{
            //    c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            //});
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
                //.AllowCredentials());
            });

            services.AddMvc();
            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
            //   .ConfigureApiBehaviorOptions(o => { o.SuppressModelStateInvalidFilter = true; });
            //services.AddControllers();

            /*var builder = new ContainerBuilder();
            services.AddEntityFrameworkNpgsql().AddDbContext<DbWebApiContext>(opt =>
                opt.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            builder.RegisterType<MenuListrepository>().As<IMenuListrepository>()
                .WithParameter("context", new DbContextOptionsBuilder<DbWebApiContext>().UseNpgsql(Configuration.GetConnectionString("DefaultConnection")).Options)
                .InstancePerDependency();
            ApplicationContainer = builder.Build();*/
            //return new AutofacServiceProvider(this.ApplicationContainer);
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<DbWebApiContext>()
               .UseNpgsql(Configuration.GetConnectionString("DefaultConnection")
               );
            builder.RegisterType<DbWebApiContext>().As<DbContext>()
                 .WithParameter("options", dbContextOptionsBuilder.Options)
                .InstancePerLifetimeScope();

            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            builder.RegisterType<MenuListrepository>().As<IMenuListrepository>()
                .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
                .InstancePerLifetimeScope();

            builder.RegisterType<ClientDetailsRepository>().As<IClientDetailsRepository>()
                .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
                .InstancePerLifetimeScope();
            //ApplicationContainer=builder.Build();

            // User Group 
            builder.RegisterType<UserGroupRepository>().As<IUserGroupRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();

            // Admin User Board
            builder.RegisterType<AdminUserBoardRepository>().As<IAdminUserBoardRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();

            // User Management
            builder.RegisterType<UserManagementRepository>().As<IUserManagementRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();

            // User Login
            builder.RegisterType<LoginRepository>().As<ILoginRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();

            builder.RegisterType<ServicesCatalogRepository>().As<IServicesCatalogRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();

            builder.RegisterType<ValidationRuleRepository>().As<IValidationRuleRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();
			   
			// Role Definition
            builder.RegisterType<RoleDefinitionRepository>().As<IRoleDefinitionRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();

            // Stage
            builder.RegisterType<StageRepository>().As<IStageRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();

            // Project Type Regulation
            builder.RegisterType<ProjectTypeRegulationRepository>().As<IProjectTypeRegulationRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();

            // Project User Role Mapping
            builder.RegisterType<ProjectUserRoleMappingRepository>().As<IProjectUserRoleMappingRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();


            // Project Definition
            builder.RegisterType<ProjectDefinitionRepository>().As<IProjectDefinitionRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();

            // Project Configuration Input
            builder.RegisterType<ProjectConfigurationInputRepository>().As<IProjectConfigurationInputRepository>()
               .WithParameter("context", new DbWebApiContext(dbContextOptionsBuilder.Options))
               .InstancePerLifetimeScope();

        }
        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "post API V1");
            });

            //app.UseCors(options => options.AllowAnyOrigin());
            app.UseCors("CorsPolicy");

            //app.UseMvc();

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
