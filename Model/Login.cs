﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class Login
    {
        [Key]
        public int userloginid { get; set; }

        public int userid { get; set; }

        public int roleid { get; set; }

        public string username { get; set; }

        public string password { get; set; }

        public DateTime expirydate { get; set; }

        public int expirationflag { get; set; }

        public string resetkey { get; set; }

        public int passwordreset { get; set; }

        public DateTime resettime { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public int updatedby { get; set; }

        public DateTime insertedtime { get; set; }

        public DateTime updatedtime { get; set; }

        
    }


    public class AuthCheck
    {
        public int userId { get; set; }

        public string userName { get; set; }

        public int roleId { get; set; }

        public string email { get; set; }

        public int clientId { get; set; }
    }
}
