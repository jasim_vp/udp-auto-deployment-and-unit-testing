﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class ValidationRule
    {
        public int id { get; set; }

        public int min_length { get; set; }

        public int max_length { get; set; }

        public int min_range { get; set; }

        public int max_range { get; set; }

        public DateTime from_date { get; set; }

        public DateTime to_date { get; set; }
    }
}
