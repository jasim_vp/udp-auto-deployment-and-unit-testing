﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class AdminUserBoard
    {
        [Key]
        public int adminid { get; set; }

        public int clientid { get; set; }

        public string admin_name { get; set; }

        public string point_of_contact { get; set; }

        public string phone_number { get; set; }

        public string address { get; set; }

        public string email_id { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public int updatedby { get; set; }

        public DateTime insertedtime { get; set; }

        public DateTime updatedtime { get; set; }
    }
}
