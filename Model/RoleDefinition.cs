﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class RoleDefinition
    {
        [Key]
        public int roleid { get; set; }

        public string role { get; set; }

        public string roledescription { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public DateTime insertedtime { get; set; }

    }


    public class RoleDefinitionList
    {
        public int projecttypeid { get; set; }

        public int roleid { get; set; }

        public int userid { get; set; }

        public int stageid { get; set; }

        public int submoduleid { get; set; }

        public int privilegeid { get; set; }

        public int flag { get; set; }

        public string role { get; set; }

        public string roledescription { get; set; }

        public Boolean active { get; set; }
    }


    public class RoleMapping
    {
        [Key]
        public int id { get; set; }

        public int roleid { get; set; }

        public int projecttype_mapping_id { get; set; }

        public int privilegeid { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public DateTime insertedtime { get; set; }

    }

}
