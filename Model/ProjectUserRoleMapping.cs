﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class ProjectUserRoleMapping
    {
        [Key]
        public int id { get; set; }

        public int projectid { get; set; }

        public int userid { get; set; }

        public int roleid { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public int updatedby { get; set; }

        public DateTime insertedtime { get; set; }

        public DateTime updatedtime { get; set; }
    }
}
