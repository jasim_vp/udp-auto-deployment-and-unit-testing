﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class ServiceCatalog
    {
        public string stagename { get; set; }

        public string modulename { get; set; }

        public string submodulename { get; set; }

        public string Privilege { get; set; }
    }

    public class ModuleSubmoduleMaster
    {
        [Key]
        public int submoduleid { get; set; }

        public int moduleid { get; set; }

        public string modulename { get; set; }

        public string submodulename { get; set; }

        public bool active { get; set; }      

      
    }

    public class PrivilegeMaster
    {
        [Key]
        public int id { get; set; }

        public string privilegename { get; set; }

        public bool active { get; set; }
    }
}
