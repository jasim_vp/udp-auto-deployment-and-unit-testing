﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class UserGroup
    {
        [Key]
        public int usergroupid { get; set; }

        public string usergroupname { get; set; }

        public string group_mailid { get; set; }

        public string projectname { get; set; }

        public int no_of_user { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public int updatedby { get; set; }

        public DateTime insertedtime { get; set; }

        public DateTime updatedtime { get; set; }

    }

    public class UserGroupMapping
    {
        public int id { get; set; }

        public int userid { get; set; }

        public int usergroupid { get; set; }

        public int projectid { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public int updatedby { get; set; }

        public DateTime insertedtime { get; set; }

        public DateTime updatedtime { get; set; }

    }


}
