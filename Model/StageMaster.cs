﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class StageMaster
    {
        [Key]
        public int stageid { get; set; }

        public string stagename { get; set; }

        public string description { get; set; }

        public string general_questions { get; set; }

        public string actions { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public int updatedby { get; set; }

        public DateTime insertedtime { get; set; }

        public DateTime updatedtime { get; set; }
    }
}
