﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataPlatform.Model
{
    public class ClientDetails
    {
        [Key]
        public int clientid { get; set; }

        public string organization_name { get; set; }

        public string address { get; set; }

        public string point_of_contact { get; set; }

        public string phone_number { get; set; }

        public string web_url { get; set; }

        public string email_id { get; set; }

        public bool active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public int updatedby { get; set; }

        public DateTime insertedtime { get; set; }

        public DateTime updatedtime { get; set; }
    }

   
}
