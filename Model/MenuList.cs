﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class MenuList
    {
        [Key]
        public int menuid { get; set; }

        public string menuname { get; set; }

        [NotMapped]
        public string submenuname { get; set; }

        public  int insertedby { get; set; }

        public int updatedby { get; set; }

        public DateTime insertedtime { get; set; }

        public DateTime updatedtime { get; set; }

        public bool active { get; set; }
    }

    public class SubmenuList
    {
        [Key]
        public int submenuid { get; set; }

        public int menuid { get; set; }
        public string submenuname { get; set; }
    }

  
}
