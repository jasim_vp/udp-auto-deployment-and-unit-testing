﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class ProjectDefinition
    {
        [Key]
        public int id { get; set; }

        public string projectname { get; set; }

        public string projectdescription { get; set; }

        public int projecttypeid { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public float displayorder { get; set; }

        public int insertedby { get; set; }

        //public int updatedby { get; set; }

        public string project_general_qa { get; set; }

        public DateTime insertedtime { get; set; }

        //public DateTime updatedtime { get; set; }
    }

    public class ProjectMapping
    {
        [Key]
        public int id { get; set; }

        public int projectid { get; set; }

        public int projecttype_mapping_id { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        //public int updatedby { get; set; }

        public DateTime insertedtime { get; set; }

//        public DateTime updatedtime { get; set; }
    }

    public class ProjectMappingList
    {
        public int projectid { get; set; }

        public string projectname { get; set; }

        public string projectdescription { get; set; }

        public int projecttypeid { get; set; }

        public int userid { get; set; }

        public int stageid { get; set; }

        public int submoduleid { get; set; }

        public int displayorder { get; set; }

        public int flag { get; set; }

        public Boolean active { get; set; }

    }

}
