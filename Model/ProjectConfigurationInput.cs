﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class GroupJson
    {
        public string groupname { get; set; }

        public string display_group_subgroup_name { get; set; }

        public string parent_subgroup_name { get; set; }

        public string subgroupname { get; set; }

        public int attributedisplayorder { get; set; }

        public int displaygrouporder { get; set; }

        public string attributename { get; set; }

        public string attributetype { get; set; }

        public object attributevalue { get; set; }

        public string displayname { get; set; }

        public int levelid { get; set; }

        public int grouporder { get; set; }

        public string outputname { get; set; }

        public Boolean outputflag { get; set; }
    }

    public class InputJsonAry
    {
        public int projectid { get; set; }

        public int moduleid { get; set; }

        public string attributename { get; set; }

        public string attributetype { get; set; }

        public object attributevalue { get; set; }

        public string displayname { get; set; }

        public string outputname { get; set; }

        public Boolean outputflag { get; set; }
    }



    public class UpdateJsonAry
    {
        public int attributeid { get; set; }

        public string attributetype { get; set; }

        public object attributevalue { get; set; }

        public string displayname { get; set; }

        public int attributedisplayorder { get; set; }

        public int groupid { get; set; }

        public string displaygroupname { get; set; }

        public int grouporder { get; set; }

        public int displaygrouporder { get; set; }

        public string outputname { get; set; }

        public Boolean outputflag { get; set; }

        public int updatedby { get; set; }
    }



    public class ProjectConfiguration
    {
        [Key]
        public int id { get; set; }
        public int projectid { get; set; }
        public string configuration { get; set; }
        public string type { get; set; }
        public string host_address { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string port { get; set; }
        public string path { get; set; }
        public string file { get; set; }
        public string database_name { get; set; }
        public string table_name { get; set; }
        public string cloud_name { get; set; }
        public string s3_url { get; set; }
        public string access_key { get; set; }
        public string secret_key { get; set; }
        public bool active { get; set; }
        public int recordstatus { get; set; }
        public int insertedby { get; set; }
        public int updatedby { get; set; }
        public DateTime insertedtime { get; set; }
        public DateTime updatedtime { get; set; }
    }


    public class ConfigDetails
    {
        public int id { get; set; }

        public int projectid { get; set; }

        public string type { get; set; }
        
        public string configuration { get; set; }

    }

    public class ProjectAttributeMofuleMapping
    {
        [Key]

        public int mapid { get; set; }

        public int projectid { get; set; }

        public int attributeid { get; set; }

        public int moduleid { get; set; }

        public int? groupmapid { get; set; }

        public int attributedisplayorder { get; set; }

        public string displayname { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public DateTime insertedtime { get; set; }

        public int updatedby { get; set; }

        public DateTime updatedtime { get; set; }

        public string attributetype { get; set; }

        public string outputname { get; set; }

        public Boolean outputflag { get; set; }


    }


    public class ProjectGroupMapping
    {
        [Key]

        public int groupmapid { get; set; }

        public int groupid { get; set; }

        public int projectid { get; set; }

        public string displaygroupname { get; set; }

        public int parentgroupid { get; set; }

        public int grouporder { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public DateTime insertedtime { get; set; }

        public int updatedby { get; set; }

        public DateTime updatedtime { get; set; }

        public int relationid { get; set; }

        public string parent_subgroup_name { get; set; }

        public int displaygrouporder { get; set; }


    }


}
