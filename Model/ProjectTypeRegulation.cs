﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class ProjectTypeRegulation
    {
        [Key]
        public int projecttypeid { get; set; }

        public string projecttype { get; set; }

        public string projecttype_description { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public DateTime insertedtime { get; set; }

    }


    public class ProjectTypeRegulationList
    {
        public int projecttypeid { get; set; }

        public int userid { get; set; }

        public int stageid { get; set; }

        public int submoduleid { get; set; }

        public int flag { get; set; }

        public string projecttype { get; set; }

        public string projecttype_description { get; set; }

        public Boolean active { get; set; }
    }


    public class ProjectTypeMapping
    {
        [Key]
        public int id { get; set; }

        public int projecttypeid { get; set; }

        public int userid { get; set; }

        public int stageid { get; set; }

        public int submoduleid { get; set; }

        public Boolean active { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public DateTime insertedtime { get; set; }

    }
}
