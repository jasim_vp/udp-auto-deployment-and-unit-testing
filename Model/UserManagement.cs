﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataPlatform.Model
{
    public class UserManagement
    {
        [Key]
        public int userid { get; set; }

        public int clientid { get; set; }

        public int adminid { get; set; }

        public string firstname { get; set; }

        public string lastname { get; set; }

        public string emailid { get; set; }

        public string phone { get; set; }

        public string workphone { get; set; }

        public int recordstatus { get; set; }

        public int insertedby { get; set; }

        public int updatedby { get; set; }

        public DateTime insertedtime { get; set; }

        public DateTime updatedtime { get; set; }

        public Boolean active { get; set; }

    }

    public class RoleMaster
    {
        [Key]
        public int roleid { get; set; }

        public string role { get; set; }
    }

    public class uploadModel
    {
        public IFormFile File { get; set; }
        public int ClientId { get; set; }
        public int RecordStatus { get; set; }
        public int InsertedBy { get; set; }
    }

    
}