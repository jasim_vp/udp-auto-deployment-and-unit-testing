﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("ClientDetails")]
    [ApiController]
    public class ClientDetailsController : ControllerBase
    {
        private readonly IClientDetailsRepository clientDetailsRepository;
        private readonly ILogger<ClientDetailsController> _logger;

        public ClientDetailsController(IClientDetailsRepository clientDetailsRepository, ILogger<ClientDetailsController> logger)
        {
            this.clientDetailsRepository = clientDetailsRepository;
            _logger = logger;
        }

        // Get all records
        [HttpGet]
        [Route("GetClientDetails")]
        public IEnumerable<ClientDetailsDTO> GetClientDetailList()
        {
            IEnumerable<ClientDetailsDTO> result;
            try
            {
                
                result = clientDetailsRepository.GetClientDetails();
                _logger.LogInformation("Client registration details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all client registration details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Client regstration details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                throw;
            }

            return result;
        }

        // Get records select by id
        [HttpGet]
        [Route("GetClientDetailListById/{id}")]
        public IEnumerable<ClientDetailsDTO> GetClientDetailsListById(int id)
        {
            IEnumerable<ClientDetailsDTO> result;
            try
            {
                result = clientDetailsRepository.GetClientDetailsListById(id);
                _logger.LogInformation("client registration list by id details response:: {Status Code:200, Data:{present:true}, Status:True, Message:Get the client registration list by id details }");
            }
            catch (Exception ex)
            {
                _logger.LogError("Client registration list by id details response::{Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                throw;
            }

            return result;
        }


        #region Insert
        [HttpPost]
        [Route("InsertClientDetails")]
        public IActionResult InsertClientDetails([FromBody] ClientDetailsDTO clientDetailsDTO)
        {
            try
            {
                if(clientDetailsDTO != null)
                {

                    clientDetailsDTO.InsertedTime = DateTime.Now;
                    ClientDetails client = new ClientDetails
                    {
                        organization_name = clientDetailsDTO.OrganizationName,
                        address = clientDetailsDTO.Address,
                        phone_number = clientDetailsDTO.PhoneNumber,
                        web_url = clientDetailsDTO.WebUrl,
                        email_id = clientDetailsDTO.EmailId,
                        recordstatus = clientDetailsDTO.RecordStatus,
                        active = clientDetailsDTO.Active,
                        insertedby = clientDetailsDTO.InsertedBy,
                        insertedtime = clientDetailsDTO.InsertedTime,
                    };


                    UserManagement user = new UserManagement
                    {
                        firstname = clientDetailsDTO.FirstName,
                        lastname = clientDetailsDTO.LastName,
                        emailid = clientDetailsDTO.EmailId,
                        phone = clientDetailsDTO.PhoneNumber,
                        recordstatus = clientDetailsDTO.RecordStatus,
                        insertedby = clientDetailsDTO.InsertedBy,
                        insertedtime = clientDetailsDTO.InsertedTime,
                        active = clientDetailsDTO.Active
                    };


                    Login userlogin = new Login
                    {
                        roleid = 3,
                        username = clientDetailsDTO.EmailId,
                        password = clientDetailsDTO.Password,
                        recordstatus = clientDetailsDTO.RecordStatus,
                        insertedby = clientDetailsDTO.InsertedBy,
                        insertedtime = clientDetailsDTO.InsertedTime,
                        active = clientDetailsDTO.Active
                    };



                    if (clientDetailsDTO.ClientId == 0)
                    {
                        var insertObj = clientDetailsRepository.InsertClientDetails(client, user, userlogin);
                        var result = Convert.ToString(insertObj);
                        

                        if (result == "Success")
                        {
                            _logger.LogInformation("Client registration insert detail response:: {Status Code:200, Data:{present:true}, Status:True, Message:Get the client registration inserted successfully}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = clientDetailsDTO.ClientId.ToString(), status = (int)OptionalField.Success, Error = "" });
                        }
                        else if (result == "Email")
                        {
                            _logger.LogError("Client registration insert details response:: { Status Code:400, Status:False, Message: Client email already exists}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Client email already exists", status = (int)OptionalField.Failure, Error = "" });
                        }
                        else if (result == "OrganizationName")
                        {
                            _logger.LogError("Client registration insert details response:: { Status Code:400, Status:False, Message: Client orgnaization name already exists}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Client orgnaization name already exists", status = (int)OptionalField.Failure, Error = "" });
                        }
                        else if (result == "EmailName")
                        {
                            _logger.LogError("Client registration insert details response:: { Status Code:400, Status:False, Message: Client orgnaization name and email already exists}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Client orgnaization name and email already exists", status = (int)OptionalField.Failure, Error = "" });
                        }
                        else
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = clientDetailsDTO.ClientId.ToString(), status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        _logger.LogError("Client registration insert details response::{Status Code:400,  Status:False, Message:'Client registration response data is empty'}");
                        return NotFound();
                    }
                }
                else
                {
                    _logger.LogError("Client registration insert details response::{Status Code:400, Status:False, Message:'Client registration data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "ClientDetails Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch(Exception ex)
            {
                _logger.LogError("client registration insert details response::{Status Code:400, Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }
        #endregion

        #region Update
        // Update Client Administration
        [HttpPost]
        [Route("UpdateClientDetails")]
        public IActionResult UpdateClientDetails([FromBody] ClientDetailsDTO clientDetailsDTO)
        {
            try
            {
                if (clientDetailsDTO != null)
                {

                    clientDetailsDTO.InsertedTime = DateTime.Now;
                    ClientDetails client = new ClientDetails
                    {
                        clientid = clientDetailsDTO.ClientId,
                        organization_name = clientDetailsDTO.OrganizationName,
                        address = clientDetailsDTO.Address,
                        phone_number = clientDetailsDTO.PhoneNumber,
                        email_id = clientDetailsDTO.EmailId,
                        web_url = clientDetailsDTO.WebUrl,
                        active = clientDetailsDTO.Active,
                        insertedtime = Convert.ToDateTime(clientDetailsDTO.InsertedDateTime),
                        updatedby = clientDetailsDTO.UpdatedBy,
                        updatedtime = clientDetailsDTO.UpdatedTime,
                    };


                    UserManagement user = new UserManagement
                    {
                        userid = clientDetailsDTO.UserId,
                        clientid = clientDetailsDTO.ClientId,
                        firstname = clientDetailsDTO.FirstName,
                        lastname = clientDetailsDTO.LastName,
                        emailid = clientDetailsDTO.EmailId,
                        phone = clientDetailsDTO.PhoneNumber,
                        recordstatus = clientDetailsDTO.RecordStatus,
                        insertedby = clientDetailsDTO.InsertedBy,
                        insertedtime = clientDetailsDTO.InsertedTime,
                        active = clientDetailsDTO.Active,
                        updatedby = clientDetailsDTO.UpdatedBy,
                        updatedtime = clientDetailsDTO.UpdatedTime,
                    };


                    Login userlogin = new Login
                    {
                        userloginid = clientDetailsDTO.UserLoginId,
                        userid = clientDetailsDTO.UserId,
                        roleid = 3,
                        username = clientDetailsDTO.EmailId,
                        password = clientDetailsDTO.Password,
                        recordstatus = clientDetailsDTO.RecordStatus,
                        insertedby = clientDetailsDTO.InsertedBy,
                        insertedtime = clientDetailsDTO.InsertedTime,
                        active = clientDetailsDTO.Active,
                        updatedby = clientDetailsDTO.UpdatedBy,
                        updatedtime = clientDetailsDTO.UpdatedTime,
                    };

                    if (clientDetailsDTO.ClientId != 0)
                    {
                        var result = clientDetailsRepository.UpdateClientDetails(client, user, userlogin);
                        if (result == "Success")
                        {
                            _logger.LogInformation("Client registration update detail response:: {Status Code:200, Data:{present:true}, Status:True, Message:Client registration updated successfully}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                        }
                        else if (result == "OrganizationName")
                        {
                            _logger.LogError("Client registration update details response:: { Status Code:400, Status:False, Message: Organization name already exists}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Organization name already exists", status = (int)OptionalField.Failure, Error = "" });
                        }
                        else if (result == "EmailId")
                        {
                            _logger.LogError("Client registration update details response:: { Status Code:400, Status:False, Message: Client email already exists}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Client email already exists", status = (int)OptionalField.Failure, Error = "" });
                        }
                        else
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        _logger.LogError("Client registration update details response::{Status Code:400, Status:False, Message:'Client id is empty'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Client Id is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                    }
                }
                else
                {
                    _logger.LogError("Client registration update details response::{Status Code:400, Status:False, Message:'Client registration data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Client registration Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("client registration update details response::{Status Code:400, Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.DBFailure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }

        #endregion


        //Delete User Group list
        [HttpPost]
        [Route("DeleteClientDetails")]
        public IActionResult DeleteClientDetails([FromBody] ClientDetailsDTO clientDetailsDTO)
        {
            try
            {

                if (clientDetailsDTO != null)
                {
                    ClientDetails objClient = new ClientDetails
                    {
                        clientid = clientDetailsDTO.ClientId,
                    };

                    UserManagement user = new UserManagement
                    {
                        userid = clientDetailsDTO.UserId,
                        clientid = clientDetailsDTO.ClientId
                    };


                    Login userlogin = new Login
                    {
                        userloginid = clientDetailsDTO.UserLoginId,
                        userid = clientDetailsDTO.UserId

                    };

                    if (clientDetailsDTO.ClientId != 0)
                    {
                        var result = clientDetailsRepository.DeleteClientDetails(objClient, user, userlogin);

                        if (result == "Success")
                        {
                            _logger.LogInformation("Client registration delete detail response:: {Status Code:200, Data:{present:true}, Status:True, Message:Get the client registration deleted successfully}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                        }
                        else
                        {
                            _logger.LogError("Client registration delete details response::{Status Code:400, Status:False, Message:'"+ result + "'}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        _logger.LogError("Client registration update details response::{Status Code:400, Status:False, Message:'Client registration data is empty'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Admin user id is null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                    }
                }
                else
                {
                    _logger.LogError("Client registration update details response::{Status Code:400, Status:False, Message:'Client registration data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Admin User Board Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Client registraion delete details response::{Status Code:400, Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }

        }
    }
}
