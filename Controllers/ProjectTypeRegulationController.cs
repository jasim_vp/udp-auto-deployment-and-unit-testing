﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("ProjectTypeRegulation")]
    [ApiController]
    public class ProjectTypeRegulationController : ControllerBase
    {

        private readonly IProjectTypeRegulationRepository projectTypeRegulationRepository;
        private readonly ILogger<ProjectTypeRegulationController> _logger;

        public ProjectTypeRegulationController(IProjectTypeRegulationRepository projectTypeRegulationRepository, ILogger<ProjectTypeRegulationController> logger)
        {
            this.projectTypeRegulationRepository = projectTypeRegulationRepository;
            _logger = logger;
        }

        // Get all records
        [HttpGet]
        [Route("GetProjectTypeRegulationList")]
        public IEnumerable<ProjectTypeRegulationDTO> GetProjectTypeRegulationList()
        {
            IEnumerable<ProjectTypeRegulationDTO> result;
            try
            {
                result = projectTypeRegulationRepository.GetProjectTypeRegulationList();
                _logger.LogInformation("Project type regulation details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all project type regulation details successfully}");

            }
            catch (Exception ex)
            {
                _logger.LogError("Project type regulation details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                throw;
            }

            return result;
        }


        #region Insert
        [HttpPost]
        [Route("InsertProjectType")]
        public IActionResult InsertRoleDefinition(List<ProjectTypeRegulationDTO> projectTypeRegulationDTOs)
        {
            try
            {
                if (projectTypeRegulationDTOs != null && projectTypeRegulationDTOs.Count != 0)
                {
                    var result = projectTypeRegulationRepository.InsertProjectType(projectTypeRegulationDTOs);

                    if (result == "Success")
                    {
                        _logger.LogInformation("project type regulation insert detail response:: {Status Code:200, Data:{present:true}, Status:True, Message:Get the project type regulation inserted successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == "Failed")
                    {
                        _logger.LogError("Project type regulation insert details response:: {Status Code:400, Data:'NULL', Status:False, Message:'Project type already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Project type regulation insert details response::{Status Code:400,  Status:False, Message:'Project type regulation response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project Type Data is Null", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project type regulation insert details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.Message, status = (int)OptionalField.DBFailure, Error = "" });
            }

        }
        #endregion


        #region Get select by Id
        // Get records select by id
        [HttpGet]
        [Route("GetProjectTypeListById/{id}")]
        public IEnumerable<ProjectTypeRegulationDTO> GetProjectTypeListById(int id)
        {
            IEnumerable<ProjectTypeRegulationDTO> result;
            try
            {
                result = projectTypeRegulationRepository.GetProjectTypeListById(id);
                _logger.LogInformation("Project type regulation list by id details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all project type regulation list by id details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Project type regulation details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                throw;
            }

            return result;
        }
        #endregion 



        #region Update
        [HttpPost]
        [Route("UpdateProjectType")]
        public IActionResult UpdateProjectType(List<ProjectTypeRegulationDTO> projectTypeRegulationDTOs)
        {
            try
            {
                if (projectTypeRegulationDTOs != null && projectTypeRegulationDTOs.Count != 0)
                {
                    var result = projectTypeRegulationRepository.UpdateProjectType(projectTypeRegulationDTOs);

                    if (result == "Success")
                    {
                        _logger.LogInformation("project type regulation insert detail response:: {Status Code:200, Data:{present:true}, Status:True, Message:Project type regulation updated successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == "Failed")
                    {
                        _logger.LogError("Project type regulation insert details response:: {Status Code:400, Data:'NULL', Status:False, Message:'Project type already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Project type regulation insert details response::{Status Code:400,  Status:False, Message:'Project type regulation response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project Type Data is Null", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project type regulation update details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.ToString(), status = (int)OptionalField.DBFailure, Error = "" });
            }

        }
        #endregion



        //Delete Project Definition
        [HttpGet]
        [Route("DeleteProjectTypeRegulation/{projectTypeId}")]
        public IActionResult DeleteProjectTypeRegulation(int projectTypeId)
        {
            try
            {
                if (projectTypeId != 0)
                {
                    var result = projectTypeRegulationRepository.DeleteProjectTypeRegulation(projectTypeId);

                    if (result == "Success")
                    {
                        _logger.LogInformation("project type regulation delete detail response:: {Status Code:200, Data:{present:true}, Status:True, Message:Project type regulation deleted successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else
                    {
                        _logger.LogError("Project type regulation delete details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + result + "'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }

                }
                else
                {
                    _logger.LogError("Project type regulation delete details response::{Status Code:400,  Status:False, Message:'Project type regulation response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Project Definition Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project type regulation delete details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }

        }
    }
}
