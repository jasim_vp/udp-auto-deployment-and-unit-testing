﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nancy.Json;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("RoleDefinition")]
    [ApiController]
    public class RoleDefinitionController : ControllerBase
    {
        private readonly IRoleDefinitionRepository roleDefinitionRepository;
        private readonly ILogger<RoleDefinitionController> _logger;

        public RoleDefinitionController(IRoleDefinitionRepository roleDefinitionRepository, ILogger<RoleDefinitionController> logger)
        {
            this.roleDefinitionRepository = roleDefinitionRepository;
            _logger = logger;
        }


        // Get all records
        [HttpGet]
        [Route("GetRoleDefinitionList")]
        public IEnumerable<RoleDefinitionDTO> GetRoleDefinitionList()
        {
            IEnumerable<RoleDefinitionDTO> result;
            try
            {
                result = roleDefinitionRepository.GetRoleDefinitionList();
                _logger.LogInformation("Role definition list details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all role definition details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Role definition list details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }


        #region Get select by Id
        // Get records select by id
        [HttpGet]
        [Route("GetProjectTypeWiseArray/{id}")]
        public IEnumerable<RoleDefinitionDTO> GetProjectTypeWiseArray(int id)
        {
            IEnumerable<RoleDefinitionDTO> result;
            try
            {
                result = roleDefinitionRepository.GetProjectTypeWiseArray(id);
                _logger.LogInformation("Role definition list by id details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all role definition list by id details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Role definition list by id details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }
        #endregion 


        // Get all records
        [HttpGet]
        [Route("GetStageList")]
        public IEnumerable<StageDTO> GetStageList()
        {
            IEnumerable<StageDTO> result;
            try
            {
                result = roleDefinitionRepository.GetStageList();
                _logger.LogInformation("Stage list details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all stage details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Stage list details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }


        // BulkInsert
        //[HttpPost]
        //[Route("Uploadfile")]
        //public IActionResult Post([FromForm] uploadModel uploadModel)
        //{
        //    List<RoleDefinitionDTO> roleDefinitionDTOs = new List<RoleDefinitionDTO>();
        //    using (var rd = new StreamReader(uploadModel.File.OpenReadStream()))
        //    {
        //        while (!rd.EndOfStream)
        //        {
        //            var splits = rd.ReadLine().Split(';');
        //            var res = splits[0].Split(',');
                    
        //            if (res[0] != "Role")
        //            {
        //                RoleDefinitionDTO users = new RoleDefinitionDTO()
        //                {

        //                    Role = res[0],
        //                    ProjectTypeName = res[1],
        //                    RoleDescription = res[2],
        //                    StageName = res[3],
        //                    ModuleName = res[4],
        //                    SubModuleName = res[5],
        //                    RecordStatus = uploadModel.RecordStatus,
        //                    InsertedBy = uploadModel.InsertedBy,
        //                    InsertedTime = DateTime.Now
        //                };
        //                roleDefinitionDTOs.Add(users);
        //            }
        //            else
        //            {

        //            }

        //        }
        //        try
        //        {
        //            if (uploadModel.File != null)
        //            {
        //                var result = roleDefinitionRepository.BulkUpload(roleDefinitionDTOs);

        //                JavaScriptSerializer serializer = new JavaScriptSerializer();
        //                var response = serializer.DeserializeObject(result[0].error_json);
        //                result[0].error_json = response;

        //                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, result);
        //            }
        //            else
        //            {
        //                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Bulk Upload Data is Null", status = (int)OptionalField.Failure, Error = "" });
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = "" });
        //        }

        //    }

        //}





        #region Insert
        [HttpPost]
        [Route("InsertRoleDefinition")]
        public IActionResult InsertRoleDefinition(List<RoleDefinitionDTO> roleDefinitionDTOs)
        {
            try
            {
                if (roleDefinitionDTOs != null && roleDefinitionDTOs.Count != 0)
                {
                    var result = roleDefinitionRepository.InsertRoleDefinition(roleDefinitionDTOs);

                    if (result == "Success")
                    {
                        _logger.LogInformation("Role Definition insert details response:: {Status Code:400,  Status:False, Message: Role definition saved successfully }");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == "Failed")
                    {
                        _logger.LogError("Role Definition insert details response:: {Status Code:400,  Status:False, Message: Role already exists }");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Role already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Role Definition insert details response:: {Status Code:400,  Status:False, Message: Role definition data is empty }");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Role Definition Data is Null", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Role definition insert details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.DBFailure, Error = "" });
            }

        }
        #endregion

        #region Get select by Id
        // Get records select by id
        [HttpGet]
        [Route("GetRoleDefinitionListById/{id}")]
        public IEnumerable<RoleDefinitionDTO> GetRoleDefinitionListById(int id)
        {
            IEnumerable<RoleDefinitionDTO> result;
            try
            {
                _logger.LogInformation("Role Definition list by id details response:: {Status Code:400,  Status:False, Message: Get all role definition list by id details successfully }");
                result = roleDefinitionRepository.GetRoleDefinitionListById(id);
            }
            catch (Exception ex)
            {

                _logger.LogError("Role definition list by id details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }
        #endregion 

        #region Update
        [HttpPost]
        [Route("UpdateRoleDefinition")]
        public IActionResult UpdateRoleDefinition(List<RoleDefinitionDTO> roleDefinitionDTOs)
        {
            try
            {
                if (roleDefinitionDTOs != null && roleDefinitionDTOs.Count != 0)
                {
                    var result = roleDefinitionRepository.UpdateRoleDefinition(roleDefinitionDTOs);

                    if (result == "Success")
                    {
                        _logger.LogInformation("Role Definition update details response:: {Status Code:400,  Status:False, Message: Role definition updated successfully }");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == "Failed")
                    {
                        _logger.LogError("Role Definition update details response:: {Status Code:400,  Status:False, Message: Role already exists }");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Role definition already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Role Definition update details response:: {Status Code:400,  Status:False, Message: Role definition data is empty }");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Role Defintion Data is Null", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Role definition update details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = "" });
            }

        }
        #endregion


        //Delete Project Definition
        [HttpGet]
        [Route("DeleteRoleDefinition/{roleId}")]
        public IActionResult DeleteRoleDefinition(int roleId)
        {
            try
            {
                if (roleId != null)
                {
                    var result = roleDefinitionRepository.DeleteRoleDefinition(roleId);

                    if (result == "Success")
                    {
                        _logger.LogInformation("Role Definition deleted details response:: {Status Code:400,  Status:True, Message: Role definition deleted successfully }");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else
                    {
                        _logger.LogError("Role Definition deleted details response:: {Status Code:400,  Status:False, Message: '"+ result  + "' }");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }

                }
                else
                {
                    _logger.LogError("Role Definition deleted details response:: {Status Code:400,  Status:False, Message: 'Role definition data is empty' }");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Role definition data is empty", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Role definition delete details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.DBFailure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }

        }



    }
}
