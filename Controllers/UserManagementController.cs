﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using DataPlatform.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("UserManagement")]
    [ApiController]
    public class UserManagementController : ControllerBase
    {

        private readonly ILogger<UserManagementController> _logger;
        private readonly IUserManagementRepository userManagementRepository;

        public UserManagementController(IUserManagementRepository userManagementRepository, ILogger<UserManagementController> logger)
        {
            this.userManagementRepository = userManagementRepository;
            _logger = logger;

        }


        // Get all records
        [HttpGet]
        [Route("GetUserManagementLists")]
        public IEnumerable<UserManagementDTO> GetUserManagementList()
        {
            IEnumerable<UserManagementDTO> result;
            try
            {

                result = userManagementRepository.GetUserManagementList();
                if (result != null)
                {
                    _logger.LogInformation("User management details response ::{Status Code:200, Data:{present:true}, Status:True, Message:Get the all user management details fetched successfully}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("User management details response ::{Status Code:400, Data:'NULL', Status:False,Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }
            return result;
        }


        // Get records select by id
        [HttpGet]
        [Route("GetUserManagementListById/{id}")]
        public IEnumerable<UserManagementDTO> GetUserManagementListById(int id)
        {
            IEnumerable<UserManagementDTO> result;

            try
            {

                result = userManagementRepository.GetUserManagementListById(id);
                if (result != null)
                {
                    _logger.LogInformation("User management list by id details response ::{Status Code:200, Data:{present:true}, Status:True, Message:Get the user management list by id details successfully}");
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("User management list by id details response ::{Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }

        // Create a new user
        [HttpPost]
        [Route("InsertUserManagement")]
        public IActionResult CreateUserManagementList([FromBody] UserManagementDTO userManagementDTO)
        {
            try
            {

                if (userManagementDTO != null)
                {
                    userManagementDTO.InsertedTime = DateTime.Now;
                    UserManagement objUserManagment = new UserManagement
                    {
                        clientid = userManagementDTO.ClientId,
                        firstname = userManagementDTO.FirstName,
                        lastname = userManagementDTO.LastName,
                        emailid = userManagementDTO.EmailId,
                        phone = userManagementDTO.Phone,
                        workphone = userManagementDTO.WorkPhone,
                        recordstatus = userManagementDTO.RecordStatus,
                        insertedby = userManagementDTO.InsertedBy,
                        insertedtime = userManagementDTO.InsertedTime,
                        active = userManagementDTO.Active,

                    };

                    var result = userManagementRepository.InsertUserManagement(objUserManagment);

                    if (result != 0)
                    {
                        _logger.LogInformation("User management insert details response ::{Status Code:200, Data:{present:true}, Status:True, Message:'" + userManagementDTO.UserId.ToString() + "'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = userManagementDTO.UserId.ToString(), status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == 0)
                    {
                        _logger.LogError("User management insert details response ::{Status Code:400, Data:'NULL', Status:False, Message:Email already exists}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Email already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        _logger.LogError("User management insert details response ::{Status Code:400, Data:'NULL', Status:Status:False, Message:'" + userManagementDTO.UserId.ToString() + "'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = userManagementDTO.UserId.ToString(), status = (int)OptionalField.Failure, Error = "" });
                    }

                }
                else
                {
                    _logger.LogWarning("User management insert details response ::{Status Code:400, Data:'NULL', Status:False, Message:User management data is empty}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "User management data is null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("User management insert details response ::{Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }


        
        // BulkInset
        [HttpPost]
        [Route("Uploadfile")]
        public IActionResult Post([FromForm] uploadModel uploadModel)
        {
            List<UserManagementDTO> userManagementDTOs = new List<UserManagementDTO>();
            using (var rd = new StreamReader(uploadModel.File.OpenReadStream()))
            {
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(';');
                    var res = splits[0].Split(',');
                    //column1.Add(splits[0]);
                    if (res[0] != "FirstName")
                    {
                        UserManagementDTO users = new UserManagementDTO()
                        {
                            ClientId = uploadModel.ClientId,
                            FirstName = res[0],
                            LastName = res[1],
                            EmailId = res[2],
                            Phone = res[3],
                            WorkPhone = res[4],
                            RecordStatus = uploadModel.RecordStatus,
                            InsertedBy = uploadModel.InsertedBy,
                            InsertedTime = DateTime.Now
                        };
                        userManagementDTOs.Add(users);
                    }
                    else
                    {

                    }

                }
                try
                {
                    if (uploadModel.File != null)
                    {
                        
                        var result = userManagementRepository.BulkUpload(userManagementDTOs);

                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var response = serializer.DeserializeObject(result[0].error_json);

                        result[0].error_json = response;
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, result);
                    }


                    else
                    {
                        _logger.LogWarning("User management bulk insert details response ::{Status Code:400, Data:'NULL', Status:False, Message:Bulk upload data is empty}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Bulk Upload Data is Null", status = (int)OptionalField.Failure, Error = "" });
                    }

                }
                catch (Exception ex)
                {
                    _logger.LogError("User management bulk insert details response ::{Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.Failure, Error = "" });
                }

            }

        }



        // Update user management
        [HttpPost]
        [Route("UpdateUserManagement")]
        public IActionResult UpdateUserManagement([FromBody] UserManagementDTO userManagementDTO)
        {
            try
            {
                if (userManagementDTO != null)
                {
                    userManagementDTO.UpdatedTime = DateTime.Now;
                    UserManagement objUserManagment = new UserManagement
                    {
                        userid = userManagementDTO.UserId,
                        clientid = userManagementDTO.ClientId,
                        firstname = userManagementDTO.FirstName,
                        lastname = userManagementDTO.LastName,
                        emailid = userManagementDTO.EmailId,
                        phone = userManagementDTO.Phone,
                        workphone = userManagementDTO.WorkPhone,
                        recordstatus = userManagementDTO.RecordStatus,
                        insertedby = userManagementDTO.InsertedBy,
                        insertedtime = Convert.ToDateTime(userManagementDTO.InsertedDateTime),
                        updatedby = userManagementDTO.UpdatedBy,
                        updatedtime = userManagementDTO.UpdatedTime,
                        active = userManagementDTO.Active
                    };


                    if (userManagementDTO.UserId != 0)
                    {
                        var result = userManagementRepository.UpdateUserManagement(objUserManagment);
                        if (result == "Success")
                        {
                            _logger.LogInformation("User management update details response ::{Status Code:200, Data:{present:true}, Status:True, Message:'" + result + "' }");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                        }
                        else if (result == "Failed")
                        {
                            _logger.LogError("User management update details response ::{Status Code:400, Data:'NULL', Status:False, Message:Email id already exists}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Email id already exists", status = (int)OptionalField.Failure, Error = "" });
                        }
                        else
                        {
                            _logger.LogError("User management update details response ::{Status Code:400, Data:'NULL', Status:False, Message:'" + result + "'}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        _logger.LogWarning("User management update details response ::{Status Code:400, Data:'NULL', Status:False, Message:Invalid ids}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "User Id is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                    }
                }
                else
                {
                    _logger.LogWarning("User management update details response ::{Status Code:400, Data:'NULL', Status:False, Message:User management data is empty}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "User Management Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("User management update details response ::{Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }

        // Delete user management
        [HttpPost]
        [Route("DeleteUserManagement")]
        public IActionResult DeleteUserManagement([FromBody] UserManagementDTO userManagementDTO)
        {
            try
            {

                if (userManagementDTO != null)
                {
                    userManagementDTO.UpdatedTime = DateTime.Now;
                    UserManagement objUserManagement = new UserManagement
                    {
                        userid = userManagementDTO.UserId,
                    };

                    Login login = new Login
                    {
                        userid = userManagementDTO.UserId,
                        userloginid = userManagementDTO.UserLoginId
                    };


                    if (userManagementDTO.UserId != 0)
                    {
                        var result = userManagementRepository.DeleteUserManagement(objUserManagement, login);

                        if (result == "Success")
                        {
                            _logger.LogInformation("User management delete details response ::{Code:200, Data:{present:true}, Status:True, Message:User deleted successfully}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "User management deleted successfully", status = (int)OptionalField.Success, Error = "" });
                        }
                        else
                        {
                            _logger.LogError("User management delete details response ::{Code:400, Data:'NULL', Status:False, Message:'" + result + "'}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        _logger.LogWarning("User management delete details response ::{Code:400, Data:'NULL', Status:False, Message:Invalid ids}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "User id is null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                    }
                }
                else
                {
                    _logger.LogWarning("User management delete details response ::{Code:400, Data:'NULL', Status:False, Message:User management data is empty}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "User Management Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("User management delete details response ::{Code:400, Data:'NULL', Status:False', Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }

        }
    }
}
