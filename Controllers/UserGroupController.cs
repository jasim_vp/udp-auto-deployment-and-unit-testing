﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("UserGroup")]
    [ApiController]
    public class UserGroupController : ControllerBase
    {

        private readonly IUserGroupRepository userGroupRepository;
        private readonly ILogger<UserGroupController> _logger;

        public UserGroupController(IUserGroupRepository userGroupRepository, ILogger<UserGroupController> logger)
        {
            this.userGroupRepository = userGroupRepository;
            _logger = logger;
        }

        // Get all records
        [HttpGet]
        [Route("GetUserGroupList")]
        public IEnumerable<UserGroupList> GetUserGroupList()
        {
            IEnumerable<UserGroupList> result;
            try
            {
                result = userGroupRepository.GetUserGroupList();
                _logger.LogInformation("User group management details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all user group details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("User group management details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
                
            }

            return result;
        }

        // Get records select by id
        [HttpGet]
        [Route("GetEditUserList/{userGroupId}")]
        public IEnumerable<UserGroupDTO> GetEditUserList(int userGroupId)
        {
            IEnumerable<UserGroupDTO> result;
            try
            {
                result = userGroupRepository.GetEditUserList(userGroupId);
                _logger.LogInformation("User group management list by id details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all user group list by id details successfully}");
            }
            catch (Exception ex)
            {

                _logger.LogError("User group management list by id details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }

        // Get project user role by id
        [HttpGet]
        [Route("GetProjectUserRoleList/{projectId}")]
        public IEnumerable<UserRoleList> GetProjectUserRoleList(int projectId)
        {
            IEnumerable<UserRoleList> result;
            try
            {
                _logger.LogInformation("Project user role details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Project user role details successfully}");
                result = userGroupRepository.GetProjectUserRoleList(projectId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Project user role details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }

        // Create a new user group
        [HttpPost]
        [Route("InsertUserGroupList")]
        public IActionResult CreateUserGroupList(List<InsertUserGroup> insertUserGroups)
        {
            try
            {
                List<UserGroupMapping> userGroupMappings;
                if (insertUserGroups != null && insertUserGroups.Count != 0)
                {


                    UserGroup objUserGroup = new UserGroup()
                    {
                        usergroupname = insertUserGroups[0].UserGroupName,
                        group_mailid = insertUserGroups[0].GroupMailId,
                        recordstatus = insertUserGroups[0].RecordStatus,
                        active = insertUserGroups[0].Active,
                        projectname = insertUserGroups[0].ProjectName,
                        no_of_user = insertUserGroups[0].NoOfUser,
                        insertedby = insertUserGroups[0].InsertedBy,
                        insertedtime = DateTime.Now
                    };


                    userGroupMappings = new List<UserGroupMapping>();

                    for (int i = 0; i <= insertUserGroups.Count - 1; i++)
                    {
                        UserGroupMapping objUserGroupMapping = new UserGroupMapping()
                        {
                            userid = insertUserGroups[i].UserId,
                            projectid = insertUserGroups[i].ProjectId,
                            recordstatus = insertUserGroups[i].RecordStatus,
                            active = insertUserGroups[i].Active,
                            insertedby = insertUserGroups[i].InsertedBy,
                            insertedtime = DateTime.Now
                        };

                        userGroupMappings.Add(objUserGroupMapping);
                    }

                  
                    var result = userGroupRepository.InsertUserGroup(objUserGroup, userGroupMappings);

                    if (result == "Failed_UsergroupName")
                    {
                        _logger.LogError("User group insert details response:: {Status Code:400, Data:'NULL', Status:False, Message:'User group name already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message ="User group name already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else if (result == "Failed_GroupMailId")
                    {
                        _logger.LogError("User group insert details response:: {Status Code:400, Data:'NULL', Status:False, Message:'User group mail id already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "User group mail id already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else if (result == "Success")
                    {
                        _logger.LogInformation("User group management list by id details response:: { Status Code:200, Data:{present:true}, Status:True, Message:'User group details inserted successfully'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "User group saved successfully", status = (int)OptionalField.Success, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "", status = (int)OptionalField.Failure, Error = "" });
                    }

                }
                else
                {
                    _logger.LogError("User group insert details response:: {Status Code:400,  Status:False, Message: User group list data is empty }");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "User group list data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("User group insert details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }



        // Create a new user group
        [HttpPost]
        [Route("UpdateUserGroupList")]
        public IActionResult UpdateUserGroupList(List<InsertUserGroup> insertUserGroups)
        {
            try
            {
                List<UserGroupMapping> userGroupMappings;
                if (insertUserGroups != null && insertUserGroups.Count != 0)
                {

                    UserGroup objUserGroup = new UserGroup
                    {
                        usergroupid = insertUserGroups[0].UserGroupId,
                    };


                    UserGroup objUpdateUserGroup = new UserGroup()
                    {
                        usergroupname = insertUserGroups[0].UserGroupName,
                        group_mailid = insertUserGroups[0].GroupMailId,
                        recordstatus = insertUserGroups[0].RecordStatus,
                        active = insertUserGroups[0].Active,
                        projectname = insertUserGroups[0].ProjectName,
                        no_of_user = insertUserGroups[0].NoOfUser,
                        insertedby = insertUserGroups[0].InsertedBy,
                        insertedtime = DateTime.Now
                    };


                    userGroupMappings = new List<UserGroupMapping>();

                    for (int i = 0; i <= insertUserGroups.Count - 1; i++)
                    {
                        UserGroupMapping objUserGroupMapping = new UserGroupMapping()
                        {
                            userid = insertUserGroups[i].UserId,
                            projectid = insertUserGroups[i].ProjectId,
                            recordstatus = insertUserGroups[i].RecordStatus,
                            active = insertUserGroups[i].Active,
                            insertedby = insertUserGroups[i].InsertedBy,
                            insertedtime = DateTime.Now
                        };

                        userGroupMappings.Add(objUserGroupMapping);
                    }


                    var result = userGroupRepository.UpdateUserGroup(objUserGroup, objUpdateUserGroup, userGroupMappings);

                    if (result == "Failed_UsergroupName")
                    {
                        _logger.LogError("User group update details response:: {Status Code:400, Data:'NULL', Status:False, Message:'User group name already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "User group name already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else if (result == "Failed_GroupMailId")
                    {
                        _logger.LogError("User group update details response:: {Status Code:400, Data:'NULL', Status:False, Message:'User group mail id already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "User group mail id already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else if (result == "Success")
                    {
                        _logger.LogInformation("User group update details response:: { Status Code:200, Data:{present:true}, Status:True, Message:'User group details updated successfully'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "User group updated successfully", status = (int)OptionalField.Success, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "", status = (int)OptionalField.Failure, Error = "" });
                    }

                }
                else
                {
                    _logger.LogError("User group update details response:: {Status Code:400,  Status:False, Message: User group list data is empty }");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "User group list data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("User group update details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }




        //Delete User Group list
        [HttpPost]
        [Route("DeleteUserGroup")]
        public IActionResult DeleteAdminUserBoard([FromBody] UserGroupList userGroupList)
        {
            try
            {

                if (userGroupList != null)
                {
                    UserGroup objUserGroup = new UserGroup
                    {
                        usergroupid = userGroupList.UserGroupId,
                    };

                    if (userGroupList.UserGroupId != 0 )
                    {
                        var result = userGroupRepository.DeleteUserGroup(objUserGroup);

                        if (result == "Success")
                        {
                            _logger.LogInformation("User group delete details response:: { Status Code:200, Data:{present:true}, Status:True, Message:'User group deleted successfully'}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                        }
                        else
                        {
                            _logger.LogError("User group delete details response:: {Status Code:400,  Status:False, Message: '"+  result +"' }");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        _logger.LogError("User group delete details response:: {Status Code:400,  Status:False, Message: User group id is empty }");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "User Group id is null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                    }
                }
                else
                {
                    _logger.LogError("User group delete details response:: {Status Code:400,  Status:False, Message: User group data is empty }");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "User group data is empty ", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("User group delete details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }

        }
    }
}
