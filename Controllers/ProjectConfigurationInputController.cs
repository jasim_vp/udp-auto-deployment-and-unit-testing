﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using DataPlatform.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("input")]
    [ApiController]
    public class ProjectConfigurationInputController : ControllerBase
    {
        private readonly IProjectConfigurationInputRepository projectConfigurationInputRepository;
        private readonly ILogger<ProjectConfigurationInputController> _logger;

        public ProjectConfigurationInputController(IProjectConfigurationInputRepository projectConfigurationInputRepository, ILogger<ProjectConfigurationInputController> logger)
        {
            this.projectConfigurationInputRepository = projectConfigurationInputRepository;
            _logger = logger;
        }


        // Get all records
        [HttpGet]
        [Route("GetProjectConfigurationDetails")]
        public IEnumerable<ProjectConfigurationDTO> GetProjectConfigurationDetails()
        {
            IEnumerable<ProjectConfigurationDTO> result;
            try
            {
                result = projectConfigurationInputRepository.GetProjectConfigurationDetails();
                _logger.LogInformation("Project configuration details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all project configuration details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Project configuration details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }

        // Get records select by id
        [HttpGet]
        [Route("GetProjectConfigurationListById/{id}")]
        public IEnumerable<ProjectConfigurationDTO> GetProjectConfigurationDetailsListById(int id)
        {
            IEnumerable<ProjectConfigurationDTO> result;
            try
            {
                result = projectConfigurationInputRepository.GetProjectConfigurationDetailsListById(id);
                _logger.LogInformation("project configuration list by id details response:: {Status Code:200, Data:{present:true}, Status:True, Message:Get the project configuration list by id details }");
            }
            catch (Exception ex)
            {
                _logger.LogError("project configuration list by id details response::{Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }

        // Get all records
        [HttpGet]
        [Route("GetProjectConfigurationAttributeList")]
        public IEnumerable<GetAllAttributeData> GetProjectConfigurationAttributeList()
        {
            IEnumerable<GetAllAttributeData> result;
            List<GetAllAttributeData>  datas = new List<GetAllAttributeData>();
            try
            {
                var mappedAttributes = projectConfigurationInputRepository.GetProjectConfigurationAttributeList(0);
                var defaultAttributes = projectConfigurationInputRepository.GetProjectConfigurationAttributeList(1);

                GetAllAttributeData addDatas = new GetAllAttributeData()
                {
                    MappedAttributes = mappedAttributes,
                    DefaultAttributes = defaultAttributes
                };

                datas.Add(addDatas);
                result = datas;

                //result = projectConfigurationInputRepository.GetProjectConfigurationAttributeList();
                _logger.LogInformation("Project Configuration details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all project configuration input details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Project Configuration details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                throw;
            }

            return result;
        }



        #region Insert
        [HttpPost]
        [Route("InsertProjectConfigurationInput")]
        public IActionResult InsertProjectConfigurationInput(List<ProjectInputParams> projectInputParams)
        {
            try
            {

                if (projectInputParams.Count != 0 && projectInputParams != null)
                {
                    var result = projectConfigurationInputRepository.InsertProjectConfigurationInput(projectInputParams);

                    if (result == "Success")
                    {
                        _logger.LogInformation("Project configuration insert details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Project configuration attributes inserted successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == "Failed")
                    {
                        _logger.LogError("Project configuration insert details response::{Status Code:400,  Status:False, Message:'Project attributes already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project attributes already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Project configuration insert details response::{Status Code:400,  Status:False, Message:'Project confiuration response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project configuration data is empty", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project configuration insert details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = "" });
            }

        }
        #endregion



        #region Insert
        [HttpPost]
        [Route("InsertProjectConfigurationDetails")]
        public IActionResult InsertProjectConfigurationDetails(ProjectConfigurationDTO projectConfigurationDTO)
        {
            try
            {
                if (projectConfigurationDTO != null)
                {
                    ProjectConfiguration projectConfiguration = new ProjectConfiguration
                    {
                        projectid = projectConfigurationDTO.ProjectId,
                        type = projectConfigurationDTO.Type,
                        configuration = projectConfigurationDTO.Configuration,
                        host_address = projectConfigurationDTO.HostAddress,
                        username = projectConfigurationDTO.UserName,
                        password = projectConfigurationDTO.Password,
                        port = projectConfigurationDTO.Port,
                        path = projectConfigurationDTO.Path,
                        file = projectConfigurationDTO.File,
                        database_name = projectConfigurationDTO.DatabaseName,
                        table_name = projectConfigurationDTO.TableName,
                        cloud_name = projectConfigurationDTO.CloudName,
                        s3_url = projectConfigurationDTO.S3Url,
                        access_key = projectConfigurationDTO.AccessKey,
                        secret_key = projectConfigurationDTO.SecretKey,
                        active = projectConfigurationDTO.Active
                    };

                    if (projectConfigurationDTO != null)
                    {
                        var result = projectConfigurationInputRepository.InsertProjectConfigurationDetails(projectConfiguration);

                        if (result == "Success")
                        {
                            _logger.LogInformation("Project configuration insert detail response:: {Status Code:200, Data:{present:true}, Status:True, Message:'" + result + "'}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                        }
                        else if (result == "Failed")
                        {
                            _logger.LogError("Project configuration insert detail response::{Status Code:400, Data:'NULL', Status:False, Message:'" + result + "'}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }
                        else
                        {
                            _logger.LogError("Project configuration insert detail response::{Status Code:400, Status:False, Message:'" + result + "'}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        _logger.LogError("Project configuration insert detail response::{Status Code:400,  Status:False, Message:'Project configuration response data is empty'}");
                        return NotFound();
                    }

                }
                else
                {
                    _logger.LogError("Project configuration insert detail response::{Status Code:400, Status:False, Message:'project configuration data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "project configuration data is Null", status = (int)OptionalField.Failure, Error = "" });

                }
            }


            catch (Exception ex)
            {
                _logger.LogError("Project configuration insert detail response::{Status Code:400, Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.Failure, Error = "" });
            }
        }
        #endregion


        #region Update
        [HttpPost]
        [Route("UpdateProjectConfigurationAttr")]
        public IActionResult UpdateProjectConfigurationAttr(List<UpdateAttr> updateAttrs)
        {
            try
            {

                if (updateAttrs.Count != 0 && updateAttrs != null)
                {
                    var result = projectConfigurationInputRepository.UpdateProjectConfigurationAttr(updateAttrs);

                    if (result == "Success")
                    {
                        _logger.LogInformation("Project configuration update details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Project configuration attributes updated successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == "Failed")
                    {
                        _logger.LogError("Project configuration update details response::{Status Code:400,  Status:False, Message:'Project attributes already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project attributes already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Project configuration update details response::{Status Code:400,  Status:False, Message:'Project confiuration response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project configuration data is empty", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project configuration update details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = "" });
            }

        }
        #endregion


        #region DeleteGroup
        [HttpPost]
        [Route("DeleteGroup")]
        public IActionResult DeleteGroup(List<DelGroup> delGroup)
        {
            try
            {

                if (delGroup != null)
                {

                    var result = projectConfigurationInputRepository.DeleteGroup(delGroup);

                    if (result)
                    {
                        _logger.LogInformation("Project configuration details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Attribute changed to default attributes}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Attribute deleted successfully", status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (!result)
                    {
                        _logger.LogError("Project configuration details response:: {Status Code:400, Data:'NULL', Status:False, Message: 'Invalid attribute id'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Invalid attribute id", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "", status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Project configuration update details response::{Status Code:400,  Status:False, Message:'Project confiuration response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project configuration data is empty", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project configuration update details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = "" });
            }

        }
        #endregion


        [HttpPost]
        [Route("DeleteMappedAttr")]
        public IActionResult DeleteMappedAttr([FromBody] DelAttr delAttr)
        {
            try
            {
                if (delAttr != null)
                {

                    var result = projectConfigurationInputRepository.DelAttr(delAttr);

                    if (result)
                    {
                        _logger.LogInformation("Project configuration details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Attribute changed to default attributes}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Attribute deleted successfully", status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (!result)
                    {
                        _logger.LogError("Project configuration details response:: {Status Code:400, Data:'NULL', Status:False, Message: 'Invalid attribute id'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Invalid attribute id", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "", status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Project configuration details response::{Status Code:400,  Status:False, Message:'Project attribute id is null'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Project attribute id is null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project Configuration details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }


    }
}
