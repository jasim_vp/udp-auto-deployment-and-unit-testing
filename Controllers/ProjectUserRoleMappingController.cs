﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("ProjectUserRoleMapping")]
    [ApiController]
    public class ProjectUserRoleMappingController : ControllerBase
    {
        private readonly IProjectUserRoleMappingRepository projectUserRoleMappingRepository;
        private readonly ILogger<ProjectUserRoleMappingController> _logger;

        public ProjectUserRoleMappingController(IProjectUserRoleMappingRepository projectUserRoleMappingRepository, ILogger<ProjectUserRoleMappingController> logger)
        {
            this.projectUserRoleMappingRepository = projectUserRoleMappingRepository;
            _logger = logger;
        }



        [HttpPost]
        [Route("GetProjectUserRoleMapList")]
        public IActionResult GetProjectTypeRegulationList([FromBody] GetProjectRoleParams getProjectRoleParams)
        {
            try
            {
                if (getProjectRoleParams != null)
                {
                    var result = projectUserRoleMappingRepository.GetProjectUserRoleMapList(getProjectRoleParams);

                    if (result != null)
                    {
                        _logger.LogInformation("Project user role mapping details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all project user role mapping details successfully}");
                        return StatusCode(200, result);
                    }
                    else if (result == null)
                    {
                        _logger.LogError("Project type regulation details response:: {Status Code:400, Data:'NULL', Status:False, Message: 'Invalid Project Type and Role'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Invalid Project Type and Role", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "", status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Project user role mapping details response::{Status Code:400,  Status:False, Message:'Project user role mapping response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Project User Role Mapping Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project user role mapping details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }


        #region Insert
        [HttpPost]
        [Route("InsertProjectRoleUserMapping")]
        public IActionResult InsertRoleDefinition(List<ProjectUserRoleMappingDTO> projectUserRoleMappingDTOs)
        {
            try
            {
                if (projectUserRoleMappingDTOs != null && projectUserRoleMappingDTOs.Count != 0)
                {
                    var result = projectUserRoleMappingRepository.InsertProjectRoleUserMapping(projectUserRoleMappingDTOs);

                    if (result == "Success")
                    {
                        _logger.LogInformation("Project user role mapping details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Project user role mapping details inserted successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == "Failed")
                    {
                        _logger.LogError("Project user role mapping details response::{Status Code:400,  Status:False, Message:'Project user role map already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project user role map already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Project user role mapping details response::{Status Code:400,  Status:False, Message:'Project user role mapping response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project User Role Mapping Data is Null", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project user role mapping insert details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.ToString(), status = (int)OptionalField.DBFailure, Error = "" });
            }

        }
        #endregion


        //Delete Project user role map
        [HttpPost]
        [Route("DeleteProjectUserRoleMap")]
        public IActionResult DeleteAdminUserBoard([FromBody] GetProjectRoleParams getProjectRoleParams)
        {
            try
            {

                if (getProjectRoleParams != null)
                {
                    ProjectUserRoleMapping objProjectUserRoleMap = new ProjectUserRoleMapping
                    {
                        id = getProjectRoleParams.ProjectUserRoleMapId,
                        projectid = getProjectRoleParams.ProjectId,
                        roleid = getProjectRoleParams.RoleId,
                        userid = getProjectRoleParams.UserId,
                    };

                    if (getProjectRoleParams.UserId != 0 && getProjectRoleParams.RoleId != 0 && getProjectRoleParams.ProjectId != 0)
                    {
                        var result = projectUserRoleMappingRepository.DeleteProjectUserRoleMap(objProjectUserRoleMap);

                        if (result == "Success")
                        {
                            _logger.LogInformation("Project user role mapping delete details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Project user role mapping details deleted successfully}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                        }
                        else
                        {
                            _logger.LogError("Project user role mapping delete details response::{Status Code:400,  Status:False, Message:'"+ result +"'}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        _logger.LogError("Project user role mapping delete details response::{Status Code:400,  Status:False, Message:'Project user role mapping id is empty'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Project user role mapping id is empty", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                    }
                }
                else
                {
                    _logger.LogError("Project user role mapping delete details response::{Status Code:400,  Status:False, Message:'Project user role mapping response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Admin User Board Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project user role mapping delete details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }

        }





        //Delete Project User Role Map
        [HttpPost]
        [Route("DeleteMultipleProjectUserRoleMap")]
        public IActionResult DeleteMultipleProjectUserRoleMap(List<GetProjectRoleParams> getProjectRoleParams)
        {
            try
            {
                List<ProjectUserRoleMapping> projectUserRoleMappings;
                if (getProjectRoleParams != null)
                {
                    projectUserRoleMappings = new List<ProjectUserRoleMapping>();

                    for (int i = 0; i <= getProjectRoleParams.Count - 1; i++)
                    {
                        ProjectUserRoleMapping objProjectUserRoleMap = new ProjectUserRoleMapping()
                        {
                            id = getProjectRoleParams[i].ProjectUserRoleMapId,
                            projectid = getProjectRoleParams[i].ProjectId,
                            roleid = getProjectRoleParams[i].RoleId,
                            userid = getProjectRoleParams[i].UserId,
                        };

                        projectUserRoleMappings.Add(objProjectUserRoleMap);
                    }

                    var result = projectUserRoleMappingRepository.DeleteMultipleProjectUserRoleMap(projectUserRoleMappings);

                    if (result == "Success")
                    {
                        _logger.LogInformation("Project user role mapping multiple delete details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Project user role mapping details deleted successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else
                    {
                        _logger.LogError("Project user role mapping delete details response::{Status Code:400,  Status:False, Message:'" + result + "'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }

                }
                else
                {
                    _logger.LogError("Project user role mapping multiple delete details response::{Status Code:400,  Status:False, Message:'Project user role mapping response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Admin User Board Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project user role mapping multiple delete details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }

        }






    }
}
