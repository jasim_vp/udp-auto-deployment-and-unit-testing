﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("ValidationRule")]
    [ApiController]
    public class ValidationRuleController : ControllerBase
    {
        public readonly IValidationRuleRepository validationRuleRepository;

        public ValidationRuleController(IValidationRuleRepository validationRuleRepository)
        {
            this.validationRuleRepository = validationRuleRepository;
        }

        #region Select All
        [HttpGet]
        [Route("GetValidationRuleDetails")]
        public IEnumerable<ValidationRuleDTO> GetValidationRuleDetails()
        {
            IEnumerable<ValidationRuleDTO> result;
            try
            {
                result = validationRuleRepository.GetValidationRuleDetails();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        #endregion

        #region Insert
        [HttpPost]
        [Route("InsertValidationRuleDetails")]
        public IActionResult CreateValidation(ValidationRuleDTO validationRuleDTO)
        {
            try
            {
                if(validationRuleDTO != null)
                {
                    ValidationRule objValidationRule = new ValidationRule
                    {
                        min_length = validationRuleDTO.Min_length,
                        max_length = validationRuleDTO.Max_length,
                        min_range = validationRuleDTO.Min_range,
                        max_range = validationRuleDTO.Max_range,
                        from_date = validationRuleDTO.From_date,
                        to_date = validationRuleDTO.To_date
                    };

                    if(validationRuleDTO.Id == 0)
                    {
                        var id = validationRuleRepository.InsertValidationRulrDetails(objValidationRule);
                        validationRuleDTO.Id = Convert.ToInt32(id);

                        if(validationRuleDTO.Id != 0)
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = validationRuleDTO.Id.ToString(), status = (int)OptionalField.Success, Error = "" });
                        }
                        //else if (validationRuleDTO.Id == 0)
                        //{
                        //    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "", status = (int)OptionalField.Failure, Error = "" });
                        //}
                        else if (validationRuleDTO.Id == 0)
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "", status = (int)OptionalField.Failure, Error = "" });
                        }
                    }
                    else
                    {
                        //update
                        return NotFound();//For no response
                    }
                }
                else
                {
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "ValidationRule Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = validationRuleDTO.Id.ToString(), status = (int)OptionalField.Success, Error = "" });
        }
        #endregion

        #region Update
        [HttpPost]
        [Route("UpdateValidationRuleDetails")]

        public IActionResult UpdatedValidationRuleDetails(ValidationRuleDTO validationRuleDTO)
        {
            try
            {
                if(validationRuleDTO != null)
                {
                    ValidationRule objValidationRule = new ValidationRule
                    {                        
                        min_length = validationRuleDTO.Min_length,
                        max_length = validationRuleDTO.Max_length,
                        min_range = validationRuleDTO.Min_range,
                        max_range = validationRuleDTO.Max_range,
                        from_date = validationRuleDTO.From_date,
                        to_date = validationRuleDTO.To_date                   
                    };

                    if (validationRuleDTO.Id != 0)
                    {
                        var response = validationRuleRepository.UpdateValidationRule(objValidationRule);
                        if (response == "Success")
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = response, status = (int)OptionalField.Success, Error = "" });
                        }
                        else if (response == "Fail")
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "", status = (int)OptionalField.Failure, Error = "" });
                        }
                        else
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = response, status = (int)OptionalField.Failure, Error = "" });
                        }
                    }
                    else
                    {
                        //update
                        return NotFound();//For no response
                    }

                }
                else
                {
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "ValidationRule Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = validationRuleDTO.Id.ToString(), status = (int)OptionalField.Success, Error = "" });
        }
        #endregion
    }
}
