﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("ServiceCatalog")]
    [ApiController]
    public class ServiceCatalogController : ControllerBase
    {
        private readonly IServicesCatalogRepository servicesCatalogRepository;

        public ServiceCatalogController(IServicesCatalogRepository servicesCatalogRepository)
        {
            this.servicesCatalogRepository = servicesCatalogRepository;
        }

        #region Select All
        [Route("GetServiceCatalogDetails")]
        [HttpGet]
        public IEnumerable<dynamic> GetServiceCatalogDetails()
        {
            IEnumerable<dynamic> result;
            try
            {
                result = servicesCatalogRepository.GetServiceCatalogDetails();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        #endregion

        #region SwelectByID
        [HttpGet]
        [Route("GetServiceCatalogDetailsById/{id}")]
        public IEnumerable<ModuleSubmoduleMasterDto> GetServiceCatalogDetailsById(int id)
        {
            IEnumerable<ModuleSubmoduleMasterDto> moduleSubmoduleMasterDtos = null;
            try
            {
                moduleSubmoduleMasterDtos = servicesCatalogRepository.GetServiceCatalogDetailsById(id);
                if (moduleSubmoduleMasterDtos != null)
                {
                    return moduleSubmoduleMasterDtos;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return moduleSubmoduleMasterDtos;

        }

        [HttpGet]
        [Route("GetPrivilegeServiceCatalogDetailsById/{id}")]
        public IEnumerable<ModuleSubmoduleMasterDto> GetPrivilegeServiceCatalogDetailsById(int id)
        {
            IEnumerable<ModuleSubmoduleMasterDto> moduleSubmoduleMasterDtos = null;
            try
            {
                moduleSubmoduleMasterDtos = servicesCatalogRepository.GetPrivilegeServiceCatalogDetailsById(id);
                if (moduleSubmoduleMasterDtos != null)
                {
                    return moduleSubmoduleMasterDtos;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return moduleSubmoduleMasterDtos;

        }
        #endregion

        #region Insert
        [HttpPost]
        [Route("InsertServiceCatalog")]
        public IActionResult InserteServiceCatalog(List<ServiceCatalogDTO> serviceCatalogDTOs)
        {
            try
            {
               if(serviceCatalogDTOs != null && serviceCatalogDTOs.Count != 0)
                {
                    var result = servicesCatalogRepository.InsertServiceCatalogDetails(serviceCatalogDTOs);

                    if (result == "Success")
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == "Failed")
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "ServiceCatalod Data is Null", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch(Exception ex)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = "" });
            }
       
        }
        #endregion

        #region Update
        [HttpPost]
        [Route("UpdateServiceCatalog")]
        public IActionResult UpdateServiceCatalog([FromBody] UpdateServicecatalogdetails updateServicecatalogdetails)
        {
            try
            {
                if (updateServicecatalogdetails != null)
                {
                    if(updateServicecatalogdetails.ProcessId != 0 && updateServicecatalogdetails.Id != 0)
                    {
                        var result = servicesCatalogRepository.UpdateServiceCatalogDetails(updateServicecatalogdetails);

                        if (result == "Success")
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                        }
                        else if (result == "Failed")
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }
                        else
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                    }
                }
                else
                {
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "UserGroupList Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }
        #endregion
        // GET: api/ServiceCatalog/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ServiceCatalog
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/ServiceCatalog/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
