﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DataPlatform.Controllers
{
    [Route("Stage")]
    [ApiController]
    public class StageController : ControllerBase
    {
        private readonly IStageRepository stageRepository;

        public StageController(IStageRepository stageRepository)
        {
            this.stageRepository = stageRepository;
        }


        // Get all records
        [HttpGet]
        [Route("GetStageList")]
        public IEnumerable<StageDTO> GetStageList()
        {
            IEnumerable<StageDTO> result;
            try
            {
                result = stageRepository.GetStageList();
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

       
    }
}
