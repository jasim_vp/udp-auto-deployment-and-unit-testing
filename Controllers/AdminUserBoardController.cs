﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("AdminUserBoard")]
    [ApiController]
    public class AdminUserBoardController : ControllerBase
    {
        private readonly IAdminUserBoardRepository adminUserBoardRepository;
        private readonly ILogger<AdminUserBoardController> _logger;

        public AdminUserBoardController(IAdminUserBoardRepository adminUserBoardRepository, ILogger<AdminUserBoardController> logger)
        {
            this.adminUserBoardRepository = adminUserBoardRepository;
            _logger = logger;
        }


        // Get all records
        [HttpGet]
        [Route("GetAdminUserBoardList")]
        public IEnumerable<AdminUserBoardDTO> GetAdminUserBoardList()
        {
            IEnumerable<AdminUserBoardDTO> result;
            try
            {
                result = adminUserBoardRepository.GetAdminUserBoardList();
                _logger.LogInformation("Admin user board details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all admin user board details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Admin user board details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }

        // Get records select by id
        [HttpGet]
        [Route("GetAdminUserBoardListById/{id}")]
        public IEnumerable<AdminUserBoardDTO> GetAdminUserBoardListById(int id)
        {
            IEnumerable<AdminUserBoardDTO> result;
            try
            {
                result = adminUserBoardRepository.GetAdminUserBoardListById(id);
                _logger.LogInformation("Admin user board list by id details response:: {Status Code:200, Data:{present:true}, Status:True, Message:Get the admin user board list by id details }");
            }
            catch (Exception ex)
            {
                _logger.LogError("Admin user board list by id details response::{Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                throw;
            }

            return result;
        }

        // Create a new admin user
        [HttpPost]
        [Route("InsertAdminUserBoard")]
        public IActionResult CreateAdminUserBoardList([FromBody] AdminUserBoardDTO adminUserBoardDTO)
        {
            try
            {
                if (adminUserBoardDTO != null)
                {

                    adminUserBoardDTO.InsertedTime = DateTime.Now;
                    AdminUserBoard objAdminUserBoard = new AdminUserBoard
                    {
                        clientid = adminUserBoardDTO.ClientId,
                        phone_number = adminUserBoardDTO.PhoneNumber,
                        email_id = adminUserBoardDTO.EmailId,
                        active = adminUserBoardDTO.Active,
                        recordstatus = adminUserBoardDTO.RecordStatus,
                        insertedby = adminUserBoardDTO.InsertedBy,
                        insertedtime = adminUserBoardDTO.InsertedTime,

                    };


                    UserManagement user = new UserManagement
                    {
                        firstname = adminUserBoardDTO.FirstName,
                        lastname = adminUserBoardDTO.LastName,
                        emailid = adminUserBoardDTO.EmailId,
                        phone = adminUserBoardDTO.PhoneNumber,
                        recordstatus = adminUserBoardDTO.RecordStatus,
                        insertedby = adminUserBoardDTO.InsertedBy,
                        insertedtime = adminUserBoardDTO.InsertedTime,
                        clientid = adminUserBoardDTO.ClientId,
                        active = adminUserBoardDTO.Active
                    };


                    Login userlogin = new Login
                    {
                        roleid = 3,
                        username = adminUserBoardDTO.EmailId,
                        password = adminUserBoardDTO.Password,
                        recordstatus = adminUserBoardDTO.RecordStatus,
                        insertedby = adminUserBoardDTO.InsertedBy,
                        insertedtime = adminUserBoardDTO.InsertedTime,
                        active = adminUserBoardDTO.Active
                    };


                    var result = adminUserBoardRepository.InsertAdminUserBoard(objAdminUserBoard, user, userlogin);

                    if (result != 0)
                    {
                        _logger.LogInformation("Admin user board insert detail response:: {Status Code:200, Data:{present:true}, Status:True, Message:Get the admin user board inserted successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = adminUserBoardDTO.ClientId.ToString(), status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == 0)
                    {
                        _logger.LogError("Admin user board insert details response:: { Status Code:400, Status:False, Message: Email id already exists}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Email id already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = adminUserBoardDTO.ClientId.ToString(), status = (int)OptionalField.Failure, Error = "" });
                    }

                }
                else
                {
                    _logger.LogError("Admin user board insert details response:: {Status Code:400,  Status:False, Message: Admin User list Data is empty}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "AdminUserList Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Admin user board insert details response::{Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.DBFailure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status200OK.ToString() });
            }
        }

        // Update admin user board list
        [HttpPost]
        [Route("UpdateAdminUserBoard")]
        public IActionResult UpdateAdminUserBoard([FromBody] AdminUserBoardDTO adminUserBoardDTO)
        {
            try
            {
                if (adminUserBoardDTO != null)
                {
                    adminUserBoardDTO.UpdatedTime = DateTime.Now;
                    AdminUserBoard objAdminUserBoard = new AdminUserBoard
                    {
                        adminid = adminUserBoardDTO.AdminId,
                        clientid = adminUserBoardDTO.ClientId,
                        phone_number = adminUserBoardDTO.PhoneNumber,
                        email_id = adminUserBoardDTO.EmailId,
                        active = adminUserBoardDTO.Active,
                        recordstatus = adminUserBoardDTO.RecordStatus,
                        insertedby = adminUserBoardDTO.InsertedBy,
                        updatedby = adminUserBoardDTO.UpdatedBy,
                        updatedtime = adminUserBoardDTO.UpdatedTime,
                    };


                    UserManagement user = new UserManagement
                    {
                        userid = adminUserBoardDTO.UserId,
                        adminid = adminUserBoardDTO.AdminId,
                        firstname = adminUserBoardDTO.FirstName,
                        lastname = adminUserBoardDTO.LastName,
                        emailid = adminUserBoardDTO.EmailId,
                        phone = adminUserBoardDTO.PhoneNumber,
                        recordstatus = adminUserBoardDTO.RecordStatus,
                        insertedby = adminUserBoardDTO.InsertedBy,
                        insertedtime = adminUserBoardDTO.InsertedTime,
                        active = adminUserBoardDTO.Active,
                        updatedby = adminUserBoardDTO.UpdatedBy,
                        updatedtime = adminUserBoardDTO.UpdatedTime,
                    };


                    Login userlogin = new Login
                    {
                        userloginid = adminUserBoardDTO.UserLoginId,
                        userid = adminUserBoardDTO.UserId,
                        roleid = 3,
                        username = adminUserBoardDTO.EmailId,
                        password = adminUserBoardDTO.Password,
                        recordstatus = adminUserBoardDTO.RecordStatus,
                        insertedby = adminUserBoardDTO.InsertedBy,
                        insertedtime = adminUserBoardDTO.InsertedTime,
                        active = adminUserBoardDTO.Active,
                        updatedby = adminUserBoardDTO.UpdatedBy,
                        updatedtime = adminUserBoardDTO.UpdatedTime,
                    };


                    if (adminUserBoardDTO.AdminId != 0)
                    {
                        var result = adminUserBoardRepository.UpdateAdminUserBoard(objAdminUserBoard, user, userlogin);

                        if (result == "Success")
                        {
                            _logger.LogInformation("Admin user board update detail response::{Status Code:200, Data:{present:true}, Status:Success, Message: Admin user board updated successfully}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                        }
                        else if (result == "Failed")
                        {
                            _logger.LogError("Admin user board update details response::{Code:200, Status:False, Message:'Email id already exists'}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Admin user email already exists", status = (int)OptionalField.Failure, Error = "" });
                        }
                        else
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Admin user email already exists", status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        _logger.LogError("Admin user board Update details response::{Status Code:400,  Status:False, Message:'AdminUserList data is empty'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "AdminUserList data is empty", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                    }
                }
                else
                {
                    _logger.LogError("Admin user board update details response::{Status Code:400,  Status:False, Message:'Admin user board response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Admin user board data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Admin user board update details response::{Status Code:400, Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status200OK.ToString() });
            }
        }

        //Delete User Group list
        [HttpPost]
        [Route("DeleteAdminUserBoard")]
        public IActionResult DeleteAdminUserBoard([FromBody] AdminUserBoardDTO adminUserBoardDTO)
        {
            try
            {

                if (adminUserBoardDTO != null)
                {
                    adminUserBoardDTO.UpdatedTime = DateTime.Now;
                    AdminUserBoard objAdminUserBoard = new AdminUserBoard
                    {
                        adminid = adminUserBoardDTO.AdminId,
                    };

                    UserManagement user = new UserManagement
                    {
                        userid = adminUserBoardDTO.UserId,
                        adminid = adminUserBoardDTO.AdminId
                    };


                    Login userlogin = new Login
                    {
                        userloginid = adminUserBoardDTO.UserLoginId,
                        userid = adminUserBoardDTO.UserId
                        
                    };

                    if (adminUserBoardDTO.AdminId != 0)
                    {
                        var result = adminUserBoardRepository.DeleteAdminUserBoard(objAdminUserBoard, user, userlogin);

                        if (result == "Success")
                        {
                            _logger.LogInformation("Admin user board delete detail response::{Status Code:200, Data:{present:true}, Status:True, Message:Get the admin user board deleted successfully}");
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                        }
                        else
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        _logger.LogError("Admin user board delete details response::{Status Code:400, Status:False, Message:'Admin user board response data is empty'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Admin user id is null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                    }
                }
                else
                {
                    _logger.LogError("Admin user board delete details response::{Status Code:400, Status:False, Message:'Admin User Board Response Data is Null'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "Admin User Board Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Admin user board delete details response::{Status Code:400, Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.DBFailure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }

        }
    }
}
