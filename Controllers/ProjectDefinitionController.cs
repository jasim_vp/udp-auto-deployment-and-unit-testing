﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("ProjectDefinition")]
    [ApiController]
    public class ProjectDefinitionController : ControllerBase
    {

        private readonly IProjectDefinitionRepository projectDefinitionRepository;
        private readonly ILogger<ProjectDefinitionController> _logger;

        public ProjectDefinitionController(IProjectDefinitionRepository projectDefinitionRepository, ILogger<ProjectDefinitionController> logger)
        {
            this.projectDefinitionRepository = projectDefinitionRepository;
            _logger = logger;
        }

        // Get all records
        [HttpGet]
        [Route("GetProjectList")]
        public IEnumerable<ProjectDefinitionDTO> GetProjectList()
        {
            IEnumerable<ProjectDefinitionDTO> result;
            try
            {
                result = projectDefinitionRepository.GetProjectList();
                _logger.LogInformation("Project definition details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all project definition details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Project definition details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                throw;
            }

            return result;
        }

        // Get project type stage, module, submodule by id
        [HttpGet]
        [Route("GetProjectStageList/{projectTypeId}")]
        public IEnumerable<ProjectStageList> GetProjectStageList(int projectTypeId)
        {
            IEnumerable<ProjectStageList> result;
            try
            {
                result = projectDefinitionRepository.GetProjectStageList(projectTypeId);
                _logger.LogInformation("Project definition list by id details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Project definition details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Project definition details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                throw;
            }

            return result;
        }

        #region Insert
        [HttpPost]
        [Route("InsertProjectDefinition")]
        public IActionResult InsertProjectDefinition(List<ProjectMappingParams> projectMappingParams)
        {
            try
            {
                
                if (projectMappingParams.Count != 0 && projectMappingParams != null)
                {
                    var result = projectDefinitionRepository.InsertProjectDefinition(projectMappingParams);

                    if (result == "Success")
                    {
                        _logger.LogInformation("Project definition insert details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Project definition details inserted successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == "Failed")
                    {
                        _logger.LogError("Project definition insert details response::{Status Code:400,  Status:False, Message:'Project already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Project definition insert details response::{Status Code:400,  Status:False, Message:'Projectdefinition response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project definition data is empty", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project definition insert details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = "" });
            }

        }
        #endregion


        #region Get select by Id
        [HttpGet]
        [Route("GetProjectDefinitionListById/{projectId}")]
        public IEnumerable<ProjectDefinitionDTO> GetProjectDefinitionListById(int projectId)
        {
            IEnumerable<ProjectDefinitionDTO> result;
            try
            {
                result = projectDefinitionRepository.GetProjectDefinitionListById(projectId);
                _logger.LogInformation("Project defintion list by id details response:: { Status Code:200, Data:{present:true}, Status:True, Message:Get all project definition list by id details successfully}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Project definition list by id details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                throw;
            }

            return result;
        }
        #endregion 


        #region Update
        [HttpPost]
        [Route("UpdateProjectDefinition")]
        public IActionResult UpdateProjectDefinition(List<ProjectMappingParams> projectMappingParams)
        {
            try
            {

                if (projectMappingParams.Count != 0 && projectMappingParams != null)
                {
                    var result = projectDefinitionRepository.UpdateProjectDefinition(projectMappingParams);

                    if (result == "Success")
                    {
                        _logger.LogInformation("Project definition update details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Project definition details inserted successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else if (result == "Failed")
                    {
                        _logger.LogError("Project definition update details response::{Status Code:400,  Status:False, Message:'Project already exists'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project already exists", status = (int)OptionalField.Failure, Error = "" });
                    }
                    else
                    {
                        _logger.LogError("Project definition update details response::{Status Code:400,  Status:False, Message:'Project definition response data is empty'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }
                }
                else
                {
                    _logger.LogError("Project definition update details response::{Status Code:400,  Status:False, Message:'Projectdefinition response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project Definition Data is Null", status = (int)OptionalField.Failure, Error = "" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project definition update details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = "" });
            }

        }
        #endregion




        //Delete Project Definition
        [HttpGet]
        [Route("DeleteProjectDefinition/{projectId}")]
        public IActionResult DeleteProjectDefinition(int projectId)
        {
            try
            {
                if (projectId != null)
                {
                    var result = projectDefinitionRepository.DeleteProjectDefinition(projectId);

                    if (result == "Success")
                    {
                        _logger.LogInformation("Project definition delete details response:: { Status Code:200, Data:{present:true}, Status:True, Message: Project definition deleted successfully}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Success, Error = "" });
                    }
                    else
                    {
                        _logger.LogError("Project definition delete details response:: { Status Code:200, Data:{present:true}, Status:False, Message:'"+ result +"'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = result, status = (int)OptionalField.Failure, Error = "" });
                    }

                }
                else
                {
                    _logger.LogError("Project definition delete details response::{Status Code:400,  Status:False, Message:'Project definition response data is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Project Definition Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Project definition delete details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.DBFailure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }

        }



    }
}
