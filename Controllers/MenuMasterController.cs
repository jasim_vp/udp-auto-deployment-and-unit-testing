﻿using DataPlatform.DTO;
using DataPlatform.IRepository;
using DataPlatform.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{
    [Route("MenuMaster")]
    [ApiController]
    public class MenuMasterController : ControllerBase
    {
        private readonly IMenuListrepository menuListrepository;


        public MenuMasterController(IMenuListrepository menuListrepository)
        {
            this.menuListrepository = menuListrepository;
        }

        #region Select All 
        [HttpGet]
        [Route("GetMenuList")]
        public IEnumerable<MenuListDTO> GetMenuList()
        {
            IEnumerable<MenuListDTO> result;
            try
            {
                result = menuListrepository.GetMenuList();
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }
        #endregion

        #region GetById
        [HttpGet]
        [Route("GetMenuListById/{id}")]
        public IEnumerable<MenuListDTO> GetMenuListById(int id)
        {
            IEnumerable<MenuListDTO> result;
            try
            {
                result = menuListrepository.GetMenuListById(id);
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }
        #endregion

        #region Insert
        [HttpPost]
        [Route("InsertMenuList")]
        public IActionResult CreateMenuList([FromBody] MenuListDTO menuListDto)
        {
            try
            {
                if (menuListDto != null)
                {
                    menuListDto.InsertedDateTime = DateTime.Now;
                    MenuList objMenuList = new MenuList
                    {
                        menuname = menuListDto.Menuname,
                        insertedby = menuListDto.InsertedBy,
                        insertedtime = menuListDto.InsertedDateTime,
                        active = menuListDto.Active
                    };
                    if (menuListDto.MenuId == 0)
                    {
                        var id = menuListrepository.InsertMenuList(objMenuList);
                        menuListDto.MenuId = Convert.ToInt32(id);
                        if (menuListDto.MenuId != 0)
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = menuListDto.MenuId.ToString(), status = (int)OptionalField.Success, Error = "" });
                        }
                        else if (menuListDto.MenuId == 0)
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Menu name already exists", status = (int)OptionalField.Failure, Error = "" });
                        }
                        else
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = menuListDto.MenuId.ToString(), status = (int)OptionalField.Failure, Error = "" });
                        }
                    }
                    else
                    {
                        //update
                        return NotFound();//For no response
                    }
                }
                else
                {
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "MenuList Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }
        #endregion

        #region Update
        [HttpPost]
        [Route("UpdatedMenuList")]
        public IActionResult UpdatedMenuList([FromBody] MenuListDTO menuListDto)
        {
            try
            {
                if (menuListDto != null)
                {
                    menuListDto.UpdatedDateTime = DateTime.Now;
                    MenuList objMenuList = new MenuList
                    {
                        menuid = menuListDto.MenuId,
                        menuname = menuListDto.Menuname,
                        insertedby = menuListDto.InsertedBy,
                        insertedtime = Convert.ToDateTime(menuListDto.InsertedTime),
                        updatedby = menuListDto.UpdatedBy,
                        updatedtime = menuListDto.UpdatedDateTime,
                        //active = menuListDto.Active
                    };
                    if (menuListDto.MenuId != 0)
                    {
                        var response = menuListrepository.UpdateMenuList(objMenuList);
                        if (response == "Success")
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = response, status = (int)OptionalField.Success, Error = "" });
                        }
                        else if (response == "Fail")
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Menu name already exists", status = (int)OptionalField.Failure, Error = "" });
                        }
                        else
                        {
                            return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = response, status = (int)OptionalField.Failure, Error = "" });
                        }

                    }
                    else
                    {
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "MenuId is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                    }
                }
                else
                {
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "MenuList Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.ToString(), status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }
        #endregion

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
