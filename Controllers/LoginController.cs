﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataPlatform.DTO;
using DataPlatform.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static DataPlatform.DTO.Status;

namespace DataPlatform.Controllers
{

    [Route("User")]
    [ApiController]
    public class LoginController : ControllerBase
    {

        private readonly ILoginRepository loginRepository;
        private readonly ILogger<LoginController> _logger;

        public LoginController(ILoginRepository loginRepository, ILogger<LoginController> logger)
        {
            this.loginRepository = loginRepository;
            _logger = logger;
        }

        // Get all records
        [HttpPost]
        [Route("Login")]
        public IActionResult VerifyLogin([FromBody] LoginDTO loginDTO)
        {

            try
            {
                if (loginDTO != null)
                {
                    var result = loginRepository.VerifyLogin(loginDTO);

                    if (result.Count() > 0)
                    {
                        _logger.LogInformation("Login details response:: {Status Code:200, Data:'NULL', Status:True, Message:'Logged in successfully'}");
                        return StatusCode(200, result);
                    }
                    else
                    {
                        _logger.LogError("Login details response:: {Status Code:400, Data:'NULL', Status:False, Message:'Invalid Username and Password'}");
                        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, new Response() { Message = "Invalid Username and Password", status = (int)OptionalField.Failure, Error = "" });
                    }
                    
                }
                else
                {
                    _logger.LogError("Login details response:: {Status Code:400, Data:'NULL', Status:False, Message:'User login data response is empty'}");
                    return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified, new Response() { Message = "User login Data is Null", status = (int)OptionalField.Failure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Login details response:: {Status Code:400, Data:'NULL', Status:False, Message:'" + ex.InnerException.Message.ToString() + "'}");
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest, new Response() { Message = ex.InnerException.Message.ToString(), status = (int)OptionalField.DBFailure, Error = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest.ToString() });
            }
        }


    }
}
