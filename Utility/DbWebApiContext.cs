﻿using Microsoft.EntityFrameworkCore;using System;using System.Collections.Generic;using System.Linq;using System.Threading.Tasks;namespace DataPlatform.Model{    public class DbWebApiContext : DbContext    {        public DbWebApiContext(DbContextOptions<DbWebApiContext> options) : base(options) { }        public DbSet<UserManagement> usermaster { get; set; }        public DbSet<MenuList> menumaster { get; set; }        public DbSet<SubmenuList> submenumaster { get; set; }        public DbSet<StageMaster> stage_master { get; set; }        public DbSet<RoleDefinition> rolemaster { get; set; }        public DbSet<ProjectTypeRegulation> projecttype_master { get; set; }        public DbSet<ProjectUserRoleMapping> project_user_role_mapping { get; set; }        public DbSet<RoleMapping> role_mapping { get; set; }

        public DbSet<ProjectDefinition> projectmaster { get; set; }

        public DbSet<ProjectMapping> project_mapping { get; set; }

        public DbSet<ProjectTypeMapping> projecttype_mapping { get; set; }        public DbSet<UserGroup> usergroupmaster { get; set; }        public DbSet<UserGroupMapping> usergroupmapping { get; set; }

        public DbSet<AdminUserBoard> adminbasicinformation { get; set; }

        public DbSet<ValidationRule> validation_rule { get; set; }

        public DbSet<ClientDetails> clientbasicinformation { get; set; }        public DbSet<Login> userlogin { get; set; }        public DbSet<ModuleSubmoduleMaster> module_submodule_master { get; set; }        public DbSet<PrivilegeMaster> privilege_master { get; set; }        public DbSet<ProjectConfiguration> project_config_credential_master { get; set; }

        public DbSet<ProjectAttributeMofuleMapping> projectattributemodule_mapping { get; set; }        public DbSet<ProjectGroupMapping> projectgroup_mapping { get; set; }    }}